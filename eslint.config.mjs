import path from 'node:path'
import { fileURLToPath } from 'node:url'
import { FlatCompat } from '@eslint/eslintrc'
import js from '@eslint/js'
import typescriptEslint from '@typescript-eslint/eslint-plugin'
import tsParser from '@typescript-eslint/parser'
import importX from 'eslint-plugin-import-x'
import solid from 'eslint-plugin-solid'
import sonarjs from 'eslint-plugin-sonarjs'
import globals from 'globals'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const compat = new FlatCompat({
  baseDirectory: __dirname,
  recommendedConfig: js.configs.recommended,
  allConfig: js.configs.all,
})

export default [
  {
    ignores: [
      '**/*.config.*',
      '**/node_modules/',
      '**/out/',
      '**/dist/',
      '**/build/',
    ],
  },
  ...compat.extends(
    'airbnb-base/legacy',
    'eslint:recommended',
    'plugin:import-x/recommended',
    'plugin:import-x/typescript',
    'plugin:solid/typescript',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'plugin:tailwindcss/recommended',
    'plugin:sonarjs/recommended-legacy',
    'prettier',
  ),
  {
    plugins: {
      '@typescript-eslint': typescriptEslint,
      sonarjs,
      importX,
    },
    settings: {
      'import-x/parser': {
        '@typescript-eslint/parser': ['.ts', '.tsx'],
      },
      'import-x/resolver': {
        typescript: true,
        node: true,
      },
    },

    languageOptions: {
      globals: {
        ...globals.browser,
      },

      parser: tsParser,
      ecmaVersion: 'latest',
      sourceType: 'module',

      parserOptions: {
        project: true,
        tsconfigRootDir: 'src/',
      },
    },

    rules: {
      'no-shadow': 'off',
      'no-underscore-dangle': 'off',
      'no-duplicate-imports': 'error',
      'no-useless-rename': 'error',
      'object-shorthand': 'error',
      'no-param-reassign': 'off',
      'default-case': 'off',
      'no-control-regex': 'off',
      'consistent-return': 'off',
      'no-use-before-define': 'off',
      'no-loop-func': 'off',
      'no-plusplus': [
        'error',
        {
          allowForLoopAfterthoughts: true,
        },
      ],
      'no-void': [
        'error',
        {
          allowAsStatement: true,
        },
      ],
      'no-unused-expressions': [
        'error',
        {
          allowShortCircuit: true,
          allowTernary: true,
        },
      ],

      'solid/jsx-no-undef': 'off',

      'sonarjs/no-small-switch': 'off',
      'sonarjs/no-nested-switch': 'off',
      'sonarjs/sonar-no-fallthrough': 'off',
      'sonarjs/pluginRules-of-hooks': 'off',
      'sonarjs/function-return-type': 'off',
      'sonarjs/no-misused-promises': 'off',
      'sonarjs/no-nested-functions': 'off',
      'sonarjs/void-use': 'off',
      'sonarjs/pseudo-random': 'off',
      'sonarjs/redundant-type-aliases': 'off',
      'sonarjs/no-duplicate-string': 'error',
      'sonarjs/no-unknown-property': 'off',
      'sonarjs/no-unstable-nested-components': 'off',
      'sonarjs/jsx-key': 'off',
      'sonarjs/no-base-to-string': 'off',
      'sonarjs/mouse-events-a11y': 'off',
      'sonarjs/sonar-max-params': 'off',

      '@typescript-eslint/no-explicit-any': 'error',
      '@typescript-eslint/no-empty-interface': 'off',
      '@typescript-eslint/no-shadow': 'error',
      '@typescript-eslint/consistent-type-definitions': 'error',
      '@typescript-eslint/no-unsafe-member-access': 'off',
      '@typescript-eslint/no-unsafe-call': 'off',
      '@typescript-eslint/no-unsafe-assignment': 'off',
      '@typescript-eslint/no-unsafe-argument': 'off',
      '@typescript-eslint/no-unsafe-return': 'off',
      '@typescript-eslint/no-empty-object-type': 'off',
      '@typescript-eslint/naming-convention': [
        'error',
        {
          selector: ['variable'],
          format: ['camelCase', 'UPPER_CASE', 'PascalCase'],
          leadingUnderscore: 'allow',
        },
        {
          selector: ['parameter'],
          format: ['camelCase'],
          leadingUnderscore: 'allow',
        },
        {
          selector: ['function'],
          format: ['camelCase'],
        },
        {
          selector: ['interface', 'typeAlias', 'enum'],
          format: ['PascalCase'],
        },
      ],
      '@typescript-eslint/no-floating-promises': [
        'error',
        {
          ignoreVoid: true,
        },
      ],
      '@typescript-eslint/no-misused-promises': [
        'error',
        {
          checksVoidReturn: {
            arguments: false,
          },
        },
      ],
    },
  },
]
