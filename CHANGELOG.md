## 2.3.3

🐛 Bugfix 🐛

- prevent overwriting screenshots #805
- restore screenshot in the right report #805
- fix reactivity problems when deleting zone #806
- make the reference path of all report visible at init #804
- stop displaying point when it is not visible at init #788

😎 Dev eyes only 😎

- change the screenshot structure in mpvz #805

## 2.3.2

✨ Feature ✨

- Add legacy export from Windows Acquisition software #725 #775 #774

🐛 Bugfix 🐛

- Manage project with empty first report #763
- Fix sortable list problem with the data table #777
- manage new error for when the project file is empty #764

😎 Dev eyes only 😎

- add UI on data table #777
- add test on legacy export #775
- add test on project with empty first PV #763
- add test for reference path #767 #757
- enhance some existing test #766
- add test on minidyn database V2 #765

# 2.3.1

We are pleased to announce the latest improvements made to Mapview, designed to offer you a unique user experience.

### Correcting distance with "Reference path"

Thanks to this new “Reference path” functionality, the points distance are corrected. From now on, you will be able to reliably display measurements based on distance.

All updates are done automatically when you connect to the website.

![reference path](./images/2.3.1.png)

✨ Feature ✨

- new interface for the "Reference path" #729 #667 #514 #689 #667
- Correcting distance with "Reference path" #744 #733

🐛 Bugfix 🐛

- enhance modal responsiveness #741
- disable "show album" button when there is no picture in album #738
- sensor coefficient translation fix #727

😎 Dev eyes only 😎

- enhance the test on the backward compatibility #743
- review the way the layers are managed #711
- create CI for reference path #661 #660 #633

## 2.2.6

✨ Feature ✨

- review the way the images is manage and add their name in the image modal #559 # 687 # 718
- update demo and logo #717
- display map in the user local #703
-

🐛 Bugfix 🐛

- update line color when point isn't shown #709
- change translation of "Utilisateur" to "Client" #688

😎 Dev eyes only 😎

- update node modules #712
- update unit name of "sensor coefficient" to respect our naming convention #698

## 2.2.5

🐛 Bugfix 🐛

- fix export when point over max #673
- fix dcar calculation #659
- Rework timezone management #658
- translation #657 #656

# 2.2.4

✨ Feature ✨

- show display error message when file can't be open for multiple reasons #342 #609
- display calibration info in info modal #507
- new project have min and max set for units #597
- creating new MathUnit (position and sensorCoefficients) for Heavydyn sensors #603 #626 #641
- update Channel name #604

🐛 Bugfix 🐛

- restrict template extension to `.xlsx` and `.xlsm` #592
- enforce the check of extension for the + button in menu project #611 #624
- deflection are sorted in export #613
- manage KO point for import, export and display #615 #621 646
- enhance backward compatibility #636
- rework the way default template are named #640

🤖 CI 🤖

- add test to check that the error import modal is displayed when needed #623
- add test on accepted extensions #631
- check that KO point is correctly displayed and managed #635 #637 #639
- review the way we manage rawdata file in test #644

# 2.2.3

:fire: Hotfix :fire:

- check only the points of the exported report instead of all #596

# 2.2.2

:fire: Hotfix :fire:

- fix key in the JSON for Excel export #591

# 2.2.1

The version 2.2.1 is a huge update of Mapview, we have change from vue.js to Solid.js.
With this change we have make some optimisation on the reactivity ad the performance go up.
Wa have also review the modal system to have something more flexible.

## 1 Interface update

### 1.1 User interface unification

The consistency of the interface is now unified thanks to the use of a new visual component management system (button, menu...).

### 1.2 Added window management system

A window management system has been implemented. Some windows (configurations, area,) are now movable and resizable.

![Window Management System](/uploads/112a53cd5bce5e14c0a336b03b78a545/1.2.png)

## 2 Menu Enhancements

Several menus have been redesigned to simplify the use of the most important functions.

### 2.1 Redesign Window Configuration

The Configuration window has been completely redesigned to clarify the load and temperature corrections for Heavydyn projects.

### 2.2 Conversion of Point menu into Color menu

The Point menu was not satisfactory in terms of user interaction.
The use of sub-menus with the need to click on "Returns" leads to superfluous manipulations on the part of the user.
The new Color menu allows you to manage the color of the points on the map with immediate and continuous access to the thresholds.

![Color menu](/uploads/53fe39bbbd32a0766ce6b7a9e84dd87e/2.2.png)

The configuration of the zones has been moved from the Report menu to the Color menu.

### 2.3 Updating the Data menu

The Data menu now has only one value column in its table in order to have a consistent display between the data table and the value displayed in the points on the map. \
The "See All Data" button has been added to display a window to view all data.\
The table can also be exported as a text file.

![Menu and data table](/uploads/996e93cecae803a4eb8789d63b79b9d2/2.3.png)

## 3 Performance Optimization

The data update management library has been changed to a more powerful version and many optimizations have been implemented to reduce the opening time of large projects by about a factor of 3.

![Performance improvements](/uploads/0c66d971202695a1df53c431b6e8e519/3.png)

## Issues

✨ Feature ✨

- window management system #396 #361 #360 #359 #358 #246 #84 #519
- adapte vue reactivity to solid reactivity #399 #400 #398 #505 #500
- rewrite component #395 #393 #392 #391 #390 #389 #388 #387 #386 #385 #384 #383 #382 #381 #380 #379 #378 #377 #376 #375 #374 #375 #374 #373 #372 #371 #370 369 #368 #367 #366 #365 #364 #363 #362 #361 #357 #356 #355 #354 #353 #352
- rework component structure #158 #121 #522 #521 #506
- create new text export #509
- reorder threshold list for UX purpose #494
- update demo files #492
- add button to download default templates in case people want to restore them #490
- add button to hide zone selector in the new all data modal #487
- order naturally data in data table #485
- Heavydyn now haven't point link as default #476
- add button to zoom on selected report/project #444
- improve data component #397
- add current project name as page title #333
- menu can be resize vertically on small screen #281
- add shapes name in the shape picker #280
- animate loading file overlay #277
- add color in correction menu #271
- add new standard for temperature correction for Belgium and Spain #533 #532
- Enhance CI

🐛 Bugfix 🐛

- Hide debug download button on mvrz export modal #495
- Make spacing int the table of all data consistant #487
- Enforce validity check of config input #479
- Change design of editable field for UX reason #477
- Fix link between range input and number input in thresholds #204
- Disable pinch zoom on ios #147
- Make the select menu visible after close #91
- Fix colorisation of the point when opening a project with hidden point #550
- Assure that report name take only one line #549
- Fix data computer slowness #327
- Fix access to average temp when using the average of the point instead of zone #590
- fix raw data reading error #248 #421

# 2.1.15

:bug: Buggix :bug:

- resolve translation problème on mvrz files #530
- resolve duplicate line on F25 file #504

# 2.1.14

:bug: BugFix :bug:

- Fix minor text error in template files

# 2.1.13

:bug: BugFix :bug:

- Fix PWA white screen
