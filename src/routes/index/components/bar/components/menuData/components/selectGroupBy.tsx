import { DialogSelect } from '/src/components'
import { useAppState } from '/src/index'
import { store } from '/src/store'

export const SelectGroupBy = (props: { button?: InternalButtonProps }) => {
  const { t } = useAppState()

  const groupBys = ['Number', 'Zone'] as ReportGroupBy[]

  return (
    <DialogSelect
      title={t('Select a group by')}
      id="select-group-by"
      button={{
        label: t('Group by'),
        testId: 'select-group-by-btn',
        full: true,
        ...props.button,
      }}
      attached
      values={{
        selected: t(store.selectedReport()?.settings.groupBy() ?? ''),
        list: groupBys.map((s) => ({
          value: s,
          text: t(s),
        })),
      }}
      onClose={(value) => {
        if (value) {
          if ((groupBys as string[]).includes(value)) {
            store.selectedReport()?.settings.groupBy.set(value as ReportGroupBy)
          }
        }
      }}
    />
  )
}
