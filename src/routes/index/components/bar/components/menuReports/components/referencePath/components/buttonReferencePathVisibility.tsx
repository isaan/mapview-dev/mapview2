import { Button } from '/src/components'
import { refPathEventClick } from '/src/scripts/utils/referencePath'
import { store } from '/src/store'

export const ButtonReferencePathVisibility = () => {
  const map = store.map()
  const report = store.selectedReport

  return (
    <Button
      id="button-reference-path-visibility"
      onClick={() => {
        if (report()?.referencePath) {
          report()
            ?.referencePath()
            ?.settings.isVisible.set((prev) => !prev)

          report()?.referencePath()?.line()?.refresh()

          if (!report()?.referencePath()?.settings.isVisible()) {
            report()?.settings.isRefpathEditing.set(false)
            map?.off('click', refPathEventClick)
          }
        }
      }}
      icon={
        report()?.referencePath()?.settings.isVisible()
          ? IconTablerEye
          : IconTablerEyeOff
      }
      disabled={report()?.referencePath()?.points().length === 0}
    />
  )
}
