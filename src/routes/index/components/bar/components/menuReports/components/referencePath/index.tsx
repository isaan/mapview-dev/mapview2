import dist from '/src/assets/svg/custom/dist.svg'
import { Dialog, Input, Interactive } from '/src/components'
import { DialogRemove } from '/src/components/generic/dialog/remove'
import { useAppState } from '/src/index'
import { recalculeChainage } from '/src/scripts/utils/referencePath'
import { store } from '/src/store'

import { ButtonReferencePathEdit } from './components/buttonReferencePathEdit'
import { ButtonReferencePathVisibility } from './components/buttonReferencePathVisibility'

export const ReferencePath = () => {
  const { t } = useAppState()
  const selectedReport = store.selectedReport
  const distUnit = selectedReport()?.project().units.distance?.currentUnit

  return (
    <Interactive
      full
      bgColor="base"
      label={t('Reference Path')}
      border={false}
      kind="static"
      leftIcon={IconTablerShare}
    >
      <div class="-my-1 -mr-3 w-full space-x-1 text-right">
        <Dialog
          closeable
          attached
          attachOptions={{ width: 300 }}
          button={{
            icon: dist,
            role: 'button',
          }}
          form={
            <div class="space-y-1.5">
              <Input
                label={t('Origin')}
                suffix={distUnit ? distUnit() : ''}
                value={selectedReport()?.referencePath()?.origin()}
                onInput={(value) => {
                  const selected = selectedReport()
                  selected?.referencePath()?.origin.set(Number(value))
                  if (selected) {
                    recalculeChainage(
                      selected.referencePath()!,
                      selectedReport()!,
                    )
                  }
                }}
              />
            </div>
          }
        />
        <ButtonReferencePathVisibility />
        <ButtonReferencePathEdit />
        <DialogRemove
          id="reference-path-remove"
          title={t('Delete reference path')}
          textContent={`${t('Are you sure that you want to delete the reference path')} ?`}
          onClose={(value) => {
            const referencePath = selectedReport()?.referencePath()
            if (
              value === 'delete' &&
              referencePath &&
              selectedReport &&
              selectedReport()?.settings.isVisible() &&
              referencePath.settings.isVisible()
            ) {
              referencePath.reset()
            }
          }}
          disabled={selectedReport()?.referencePath()?.points().length === 0}
        />
      </div>
    </Interactive>
  )
}
