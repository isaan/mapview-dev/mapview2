import { Button, Dialog } from '/src/components'
import { useAppState } from '/src/index'
import { downloadImage } from '/src/scripts'
import { store } from '/src/store'

import { Image } from './image'

export const DialogAlbum = () => {
  const { t } = useAppState()

  const emptyAlbum = createMemo(
    () => store.selectedReport()?.album().length === 0,
  )

  return (
    <Dialog
      closeable
      maximized
      button={{
        leftIcon: IconTablerSlideshow,
        text: !emptyAlbum()
          ? `${t('View the album')} - ${store.selectedReport()?.album().length}`
          : t('Empty album'),
        full: true,
        disabled: emptyAlbum(),
      }}
      title={t('Album')}
    >
      <div class="flex h-full items-center space-x-4 overflow-x-auto px-6">
        <For each={store.selectedReport()?.album()}>
          {(image, index) => (
            <div class="mx-auto flex-none space-y-8">
              <p style={{ 'text-align': 'center' }}>{image.name}</p>
              <Image image={image.file} />
              <div class="flex justify-center space-x-2">
                <div class="inline-block space-x-2">
                  <Button
                    leftIcon={IconTablerCameraDown}
                    onClick={() => {
                      void downloadImage(image.file, `${image.name}.png`)
                    }}
                  >
                    {t('Download')}
                  </Button>
                  <Button
                    color="red"
                    leftIcon={IconTablerCameraCancel}
                    onClick={() =>
                      store.selectedReport()?.album.set((l) => {
                        l.splice(index(), 1)
                        return l
                      })
                    }
                  >
                    {t('Delete')}
                  </Button>
                </div>
              </div>
            </div>
          )}
        </For>
      </div>
    </Dialog>
  )
}
