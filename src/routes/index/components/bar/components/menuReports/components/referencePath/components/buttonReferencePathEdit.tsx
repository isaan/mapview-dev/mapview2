import { Button } from '/src/components'
import { createASS } from '/src/scripts/utils/reactivity'
import { refPathEventClick } from '/src/scripts/utils/referencePath'
import { store } from '/src/store'

export const ButtonReferencePathEdit = () => {
  const map = store.map()
  const report = store.selectedReport
  const color: ASS<ColorProp | undefined> = createASS(undefined)

  createEffect(
    on(
      () => report()?.settings.isRefpathEditing(),
      () => {
        if (report?.()?.settings.isRefpathEditing()) {
          report()?.referencePath()?.settings.isVisible.set(true)
          color.set('green')
        } else {
          color.set(undefined)
        }
      },
    ),
  )

  createEffect(
    on(
      () => report()?.settings.isVisible(),
      () => {
        if (report && !report()?.settings.isVisible()) {
          color.set(undefined)
          map?.off('click', refPathEventClick)
        }
      },
    ),
  )

  return (
    <Button
      id="button-reference-path-edit"
      onClick={() => {
        if (!map) return
        report()?.settings.isRefpathEditing.set(
          !report()?.settings.isRefpathEditing(),
        )

        if (report?.()?.settings.isRefpathEditing()) {
          map.on('click', refPathEventClick)
          map.getCanvas().style.cursor = 'crosshair'
          const refPath = report()?.referencePath()
          const editing: boolean =
            report()?.settings.isRefpathEditing() || false
          if (refPath && refPath.points && refPath.points().length > 0) {
            refPath.points().forEach((p) => {
              p.marker?.setDraggable(editing)
              p.marker?.getElement().addEventListener('click', p.listener)
            })
          }
        } else {
          map.off('click', refPathEventClick)
          map.getCanvas().style.cursor = 'grab'
          if (report?.()?.referencePath) {
            const refPath = report()?.referencePath()
            if (refPath) {
              refPath.points().forEach((p) => {
                p.marker?.setDraggable(false)
                p.marker?.getElement().removeEventListener('click', p.listener)
              })
            }
          }
        }
      }}
      icon={IconTablerEdit}
      color={color()}
    />
  )
}
