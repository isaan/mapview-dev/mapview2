import { Button } from '/src/components'

interface Props {
  readonly isVisible: ASS<boolean>
  readonly 'data-testid'?: string
}

export const ButtonReportVisibility = (props: Props) => {
  return (
    <Button
      icon={props.isVisible() ? IconTablerEye : IconTablerEyeOff}
      onClick={() => props.isVisible.set((v) => !v)}
      data-testid={props['data-testid']}
    />
  )
}
