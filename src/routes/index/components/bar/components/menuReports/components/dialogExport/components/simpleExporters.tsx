import { Button } from '/src/components'
import { useAppState } from '/src/index'
import {
  downloadFile,
  getSimpleReportExports,
  hasRawData,
  mvrzExporter,
  run,
} from '/src/scripts'
import { store } from '/src/store'

export const SimpleExporters = (props: NavigatorComponentProps) => {
  const { t } = useAppState()

  const simpleExports = createMemo(() => {
    const selectedProject = store.selectedProject()
    return selectedProject ? getSimpleReportExports(selectedProject) : []
  })

  const haveExportablePoint = createMemo(
    () =>
      (store.selectedProject()?.reports.selected()?.exportablePoints().length ??
        0) > 0,
  )

  const haveRawData = createMemo(() =>
    hasRawData(store.selectedProject() as BaseProject),
  )

  return (
    <div class="space-y-2">
      <Button
        leftIcon={IconTablerFileSpreadsheet}
        rightIcon={IconTablerChevronRight}
        full
        disabled={!haveExportablePoint()}
        onClick={() => props.next('/mvrz')}
      >
        <span class="flex-1 text-left">{mvrzExporter.name}</span>
      </Button>
      <For each={simpleExports()}>
        {(exporter) => (
          <Show
            when={haveRawData() && exporter.name === '.txt'}
            fallback={
              <Button
                full
                leftIcon={IconTablerFileText}
                rightIcon={IconTablerDownload}
                disabled={!haveExportablePoint()}
                onClick={() => {
                  void run(async () => {
                    const selectedProject = store.selectedProject()
                    if (selectedProject) {
                      downloadFile(
                        await exporter.export(
                          // @ts-expect-error Types failing
                          selectedProject,
                          haveRawData(),
                        ),
                      )
                    }
                  })
                }}
              >
                <span class="flex-1 text-left">{exporter.name}</span>
              </Button>
            }
          >
            <>
              <Button
                full
                leftIcon={IconTablerFileText}
                rightIcon={IconTablerDownload}
                disabled={!haveExportablePoint()}
                onClick={() => {
                  void run(async () => {
                    const selectedProject = store.selectedProject()
                    if (selectedProject) {
                      downloadFile(
                        await exporter.export(
                          // @ts-expect-error Types failing
                          selectedProject,
                          haveRawData(),
                        ),
                      )
                    }
                  })
                }}
              >
                <span class="flex-1 text-left">{exporter.name}</span>
              </Button>
              <Button
                full
                leftIcon={IconTablerFileText}
                rightIcon={IconTablerDownload}
                disabled={!haveExportablePoint()}
                onClick={() => {
                  void run(async () => {
                    const selectedProject = store.selectedProject()
                    if (selectedProject) {
                      downloadFile(
                        await exporter.export(
                          // @ts-expect-error Types failing
                          selectedProject,
                          false,
                        ),
                      )
                    }
                  })
                }}
              >
                <span class="flex-1 text-left">
                  {exporter.name} {`(${t('Without time data')})`}
                </span>
              </Button>
            </>
          </Show>
        )}
      </For>
    </div>
  )
}
