import { ButtonFile } from '/src/components'
import {
  convertFileToDataURL,
  getExtensionFromPath,
  getFileNameFromList,
} from '/src/scripts'
import { store } from '/src/store'

import { ButtonFlyToReport } from './components/buttonFlyToReport'
import { DialogAlbum } from './components/dialogAlbum'
import { DialogExport } from './components/dialogExport'
import { DialogReportInformation } from './components/dialogInformation'
import { DialogReports } from './components/dialogReports'
import { DialogScreenshot } from './components/dialogScreenshot'
import { ReferencePath } from './components/referencePath'
import { SelectReportMarkerIcon } from './components/selectReportMarkerIcon'

export const MenuReports = () => {
  return (
    <>
      <Show when={store.selectedReport()}>
        {(report) => (
          <div class="flex space-x-2">
            <SelectReportMarkerIcon iconName={report().settings.iconName} />
            <DialogReports />
            <ButtonFlyToReport report={report()} />
          </div>
        )}
      </Show>
      <ReferencePath />
      <div class="flex space-x-2">
        <DialogAlbum />
        <DialogScreenshot />
        <ButtonFile
          onFiles={(files) =>
            Array.from(files || []).forEach(async (file: File) => {
              const dataURL = await convertFileToDataURL(file)
              store.selectedReport()?.album.set((l) => {
                const imageName = getFileNameFromList(l, file.name)
                const image = {
                  name: imageName,
                  file: dataURL,
                  extension: getExtensionFromPath(file.name),
                }

                l.push(image)
                return l
              })
            })
          }
          extensions={['png', 'jpg', 'jpeg', 'bmp', 'gif']}
        />
      </div>
      <DialogReportInformation />
      <DialogExport />
    </>
  )
}
