import { DialogRemove } from '/src/components/generic/dialog/remove'
import { useAppState } from '/src/index'
import { store } from '/src/store'

import { ButtonAddProject } from './components/buttonAddProject'
import { ButtonSave } from './components/buttonSave'
import { DialogConfig } from './components/dialogConfig'
import { DialogProjectInformation } from './components/dialogInformation'
import { SelectProject } from './components/selectProjects'

export const MenuProject = () => {
  const { t } = useAppState()
  return (
    <>
      <div class="flex space-x-2">
        <SelectProject />
        <ButtonAddProject />
        <DialogRemove
          id="test"
          title={t('Delete project')}
          textContent={
            <>
              {t('Are you sure that you want to delete the project')}{' '}
              <strong>
                {String(store.selectedProject()?.name.toString())}
              </strong>
              ?
            </>
          }
          onClose={(value) => {
            if (value === 'delete') {
              const index = store.projects
                .list()
                .findIndex((project) => project === store.selectedProject())

              const project = store.projects.removeIndex(index)

              if (project === store.selectedProject()) {
                const _project = store.projects
                  .list()
                  .at(Math.max(index - 1, 0))

                if (_project) {
                  store.selectProject(_project)
                  _project.addToMap()
                } else {
                  store.projects.resetSelected()
                }
              }
            }
          }}
          disabled={store.projects.list().length <= 1}
        />
      </div>
      <DialogProjectInformation />
      <DialogConfig />
      <ButtonSave />
    </>
  )
}
