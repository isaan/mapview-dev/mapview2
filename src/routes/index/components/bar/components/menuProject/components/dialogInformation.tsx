import { DialogInformation } from '/src/components'
import { useAppState } from '/src/index'
import { store } from '/src/store'

export const DialogProjectInformation = () => {
  const { t } = useAppState()

  const bulks = createMemo(() => {
    const selectedProject = store.selectedProject()
    return [
      {
        title: t('Information'),
        fields: selectedProject
          ? [selectedProject.name, ...selectedProject.information]
          : [],
      },
      {
        title: t('Hardware'),
        fields: selectedProject?.hardware || [],
      },
    ]
  })

  const calibrations = createMemo(() => {
    const selectedProject = store.selectedProject()
    if (selectedProject?.machine !== 'Heavydyn') return null
    return selectedProject ? selectedProject.calibrations : null
  })

  const units = createMemo(() => {
    const selectedProject = store.selectedProject()
    if (selectedProject?.machine !== 'Heavydyn') return null
    return selectedProject ? selectedProject.units : null
  })

  return (
    <DialogInformation
      id="dialog-project-informations"
      bulks={bulks()}
      calibrations={calibrations()}
      units={units()}
    />
  )
}
