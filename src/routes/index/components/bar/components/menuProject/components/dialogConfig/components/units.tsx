import { Details, DialogSelect, Input, Label } from '/src/components'
import { useAppState } from '/src/index'
import { roundValue } from '/src/scripts'
import { store } from '/src/store'

export const Units = () => {
  const { t } = useAppState()

  const getMathUnitIcon = (mathUnit: HeavydynUnits) => {
    switch (mathUnit.name) {
      case 'CumSum':
        return IconTablerSum
      case 'Deflection':
        return IconTablerArrowBarToDown
      case 'Distance':
        return IconTablerRuler2
      case 'Force':
        return IconTablerWeight
      case 'Modulus':
        return IconTablerEaseOut
      case 'Percentage':
        return IconTablerPercentage
      case 'Stiffness':
        return IconTablerCurlyLoop
      case 'Temperature':
        return IconTablerTemperature
      case 'Time':
        return IconTablerClock
    }
  }

  return (
    <Label size="lg" label={t('Units')} class="space-y-4">
      <For
        each={store
          .selectedProject()
          ?.units.list.filter((unit) => !unit.readOnly)}
      >
        {(mathUnit) => (
          <Details
            defaultOpen
            locked
            button={{
              leftIcon: getMathUnitIcon(mathUnit),
              label: t('Unit'),
              text: t(mathUnit.name),
            }}
          >
            <div class="flex space-x-2">
              <Show
                when={
                  mathUnit.possibleSettings.length > 0 &&
                  mathUnit.possibleSettings[0][0] !== ''
                }
              >
                <DialogSelect
                  attached
                  id={`${mathUnit.name.toLocaleLowerCase().replaceAll(' ', '-')}-unit-select`}
                  button={{
                    full: true,
                    label: t('Unit'),
                    color: 'tertiary',
                    border: true,
                    borderColor: 'base',
                    size: 'base',
                  }}
                  values={{
                    selected: mathUnit.currentUnit(),
                    list: mathUnit.possibleSettings.map(
                      (setting) => setting[0],
                    ),
                  }}
                  onClose={(value) => {
                    if (value) mathUnit.currentUnit.set(value)
                  }}
                />
              </Show>
              <DialogSelect
                attached
                id={`${mathUnit.name.toLocaleLowerCase().replaceAll(' ', '-')}-precision-select`}
                button={{
                  full: true,
                  leftIcon: IconTablerDecimal,
                  label: t('Precision'),
                  color: 'tertiary',
                  border: true,
                  borderColor: 'base',
                  size: 'base',
                }}
                values={{
                  selected: String(mathUnit.currentPrecision()),
                  list: mathUnit.possiblePrecisions.map((precision) =>
                    String(precision),
                  ),
                }}
                onClose={(value) => {
                  if (value) mathUnit.currentPrecision.set(Number(value))
                }}
              />
            </div>
            <div class="flex space-x-2">
              <Show when={'min' in mathUnit}>
                <Input
                  leftIcon={IconTablerArrowBarUp}
                  label={t('Min')}
                  full
                  value={roundValue(
                    mathUnit.baseToCurrent(
                      (mathUnit as MathUnit<string>).min(),
                    ),
                  )}
                  bind
                  onInput={(value) => {
                    ;(mathUnit as MathUnit<string>).min.set(
                      mathUnit.currentToBase(Number(value ?? 0)),
                    )
                  }}
                />
              </Show>

              <Show when={'max' in mathUnit}>
                <Input
                  leftIcon={IconTablerArrowBarToUp}
                  label={t('Max')}
                  full
                  bind
                  value={roundValue(
                    mathUnit.baseToCurrent(
                      (mathUnit as MathUnit<string>).max(),
                    ),
                  )}
                  onInput={(value) => {
                    ;(mathUnit as MathUnit<string>).max.set(
                      mathUnit.currentToBase(Number(value ?? 0)),
                    )
                  }}
                />
              </Show>
            </div>
          </Details>
        )}
      </For>
    </Label>
  )
}
