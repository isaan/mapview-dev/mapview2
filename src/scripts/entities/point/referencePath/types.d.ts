// ---
// JSON
// ---

type JSONReferencePathPointVAny = JSONReferencePathPoint

interface JSONReferencePathPoint extends JSONMapboxPoint {
  readonly version: 1
  readonly coordinates: mapboxgl.LngLatLike
}

// ---
// Object
// ---

interface ReferencePathPoint
  extends BaseObject<JSONReferencePathPoint>,
    MapboxPoint<JSONReferencePathPoint>,
    DisposableObject {
  readonly id: string
  readonly listener: ASS<((ev: MouseEvent) => void) | undefined>
}
