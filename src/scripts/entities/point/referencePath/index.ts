import { getOwner } from 'solid-js'

import { createIcon } from '/src/scripts/entities/icon'
import { createMapboxPointFromJSON } from '/src/scripts/entities/point/mapbox'
import { createASS } from '/src/scripts/utils'
import { recalculeChainage } from '/src/scripts/utils/referencePath'

export const createReferencePathPointFromJSON = (
  visibleOnMap: Accessor<boolean>,
  json: JSONMapboxPoint,
  map: mapboxgl.Map | null,
  index: number,
  referencePath: ReferencePath,
  refPathId: number,
  listenerFunction?: (ev: MouseEvent) => void,
) => {
  const owner = getOwner()

  return createRoot((dispose) => {
    json = upgradeJSON(json)

    const id = `${refPathId}-${Math.random().toString(36).substring(2, 15)}`

    const base = createMapboxPointFromJSON(visibleOnMap, json, map, id, {
      icon: createIcon('Circle', index, true, 'ref-path-icon')!,
      draggable: true,
    })

    const shouldBeOnMap = createMemo(() => visibleOnMap())

    const listener = createASS<((ev: MouseEvent) => void) | undefined>(
      listenerFunction,
    )

    const point: ReferencePathPoint = {
      ...base,
      listener,
      dispose,
      owner,
    }

    if (!listenerFunction) {
      point.listener.set(() => {
        return (ev: MouseEvent) => {
          ev.preventDefault()
          ev.stopPropagation()

          if (
            !referencePath.report().settings.isRefpathEditing() ||
            base.isDragged()
          ) {
            return
          }

          const report = referencePath.report()

          point.marker?.remove()

          point.dispose()

          referencePath.points.set(
            referencePath.points().filter((pt) => {
              return pt.id !== point.id
            }),
          )

          referencePath.line().removePoint(point.id)

          recalculeChainage(referencePath, report)
        }
      })
    }

    point.marker?.on('dragend', () => {
      const report = referencePath.report()
      recalculeChainage(referencePath, report)
    })

    point.marker?.getElement().addEventListener('click', point.listener()!)

    createEffect(() => {
      point.marker?.setDraggable(
        referencePath.report().settings.isRefpathEditing(),
      )
    })

    createEffect(() => {
      if (map && point.marker) {
        if (shouldBeOnMap()) {
          point.marker.addTo(map)
        } else {
          point.marker.remove()
        }
      }
    })

    return point
  })
}

const upgradeJSON = (json: JSONMapboxPointVAny): JSONMapboxPoint => {
  switch (json.version) {
    case 1:
    // upgrade
  }

  return json
}
