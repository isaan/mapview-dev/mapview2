import { Marker } from 'mapbox-gl'

import { createASS } from '/src/scripts'

export const createMapboxPointFromJSON = (
  shouldBeOnMap: Accessor<boolean>,
  json: JSONMapboxPoint,
  map: mapboxgl.Map | null,
  id: string,
  markerOptions?: {
    icon: Icon
    draggable: boolean
  },
) => {
  json = upgradeJSON(json)

  const settings = createPointSettingsFromJSON(json.settings)

  const marker = markerOptions?.icon
    ? new Marker({
        element: markerOptions.icon.element,
        draggable: markerOptions.draggable,
      }).setLngLat(json.coordinates)
    : null

  marker?.on('drag', () => {
    coordinates.set(marker.getLngLat())
  })

  const markerDragged: ASS<boolean> = createASS(false)

  marker?.on('dragend', () => {
    markerDragged.set(true)
    setTimeout(() => {
      markerDragged.set(false)
    }, 1)
  })

  const coordinates = createASS(marker?.getLngLat())

  const point: MapboxPoint = {
    id,
    marker,
    isDragged: markerDragged,
    coordinates,
    settings,
    toBaseJSON() {
      return {
        version: 1,
        coordinates: this.marker?.getLngLat() || json.coordinates,
        settings: this.settings.toJSON(),
      }
    },
  }

  createEffect(() => {
    if (map && point.marker) {
      if (shouldBeOnMap() && point.settings.isVisible()) {
        point.marker.addTo(map)
      } else {
        point.marker.remove()
      }
    }
  })

  return point
}

const upgradeJSON = (json: JSONMapboxPointVAny): JSONMapboxPoint => {
  switch (json.version) {
    case 1:
    // upgrade
  }

  return json
}

const createPointSettingsFromJSON = (
  json: JSONPointSettings | undefined,
): PointSettings => ({
  isVisible: createASS(json?.isVisible ?? true),
  toJSON() {
    return {
      version: 1,
      isVisible: this.isVisible(),
    }
  },
})
