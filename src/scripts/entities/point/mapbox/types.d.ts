// ---
// JSON
// ---

type JSONMapboxPointVAny = JSONMapboxPoint

interface JSONMapboxPoint {
  readonly version: 1
  readonly coordinates: mapboxgl.LngLatLike
  readonly settings?: JSONPointSettings
}

interface JSONPointSettings {
  readonly version: 1
  readonly isVisible: boolean
}

// ---
// Object
// ---

interface MapboxPoint<JSONBase extends JSONMapboxPoint = JSONMapboxPoint>
  extends BaseObject<JSONBase> {
  readonly id: string
  readonly marker: mapboxgl.Marker | null
  readonly isDragged: ASS<boolean>
  readonly coordinates: ASS<mapboxgl.LngLat | undefined>
  readonly settings: PointSettings
}
