import { ReactiveMap } from '@solid-primitives/map'
import { Popup } from 'mapbox-gl'

import { env } from '/src/env'
import { translate } from '/src/locales'
import {
  colors,
  createASS,
  createDataValueFromJSON,
  createFieldFromJSON,
  createIcon,
  formatToUTCDate,
} from '/src/scripts'
import { createMapboxPointFromJSON } from '/src/scripts/entities/point/mapbox'
import { recalculeChainage } from '/src/scripts/utils/referencePath'
import { store } from '/src/store'

export const createBasePointFromJSON = <
  Zone extends MachineZone,
  Drop extends MachineDrop,
>(
  json: JSONBasePointVAny,
  map: mapboxgl.Map | null,
  parameters: {
    zone: Zone
    information: JSONField[]
    drops: Drop[]
  },
) => {
  json = upgradeJSON(json)

  const zone = createASS(parameters.zone)

  const shouldBeOnMap = createMemo(() => {
    const _zone = zone()
    const report = _zone.report()
    const project = report.project()

    return (
      _zone.settings.isVisible() &&
      report.settings.isVisible() &&
      project.settings.arePointsVisible() &&
      (env.isTest || store.selectedProject() === project)
    )
  })

  const icon = createIcon(
    parameters.zone.report().settings.iconName(),
    json.index,
  )

  const base = createMapboxPointFromJSON(shouldBeOnMap, json, map, json.id, {
    icon: icon!,
    draggable: !parameters.zone.report().project().settings.arePointsLocked(),
  })

  const dataset = new ReactiveMap<DataLabel, DataValue<string>>()
  json.data.forEach((jsonDataValue) => {
    const dataValue = createDataValueFromJSON(
      jsonDataValue,
      parameters.zone.report().dataLabels.groups.list()[1].choices.list(),
    )

    dataset.set(dataValue.label, dataValue)
  })

  const point: BasePoint<Drop, Zone> = {
    ...base,
    date: new Date(json.date),
    dataset,
    drops: [],
    index: createASS(json.index),
    icon,
    information: parameters.information.map((field: JSONField) =>
      createFieldFromJSON(
        field,
        zone().report().project().database().timeZoneOffsetMinutes,
      ),
    ),
    number: createASS(json.number),
    rawDataFile: createASS(null),
    zone,
    onMapMathNumber: createMemo(() => {
      const group = zone().report().dataLabels.groups.selected()
      const selected = group?.choices.selected()

      if (!group || !selected) return

      return point.getMathNumber(
        group.from,
        selected,
        group.from === 'Drop' ? group.indexes.selected() : undefined,
      )
    }),
    getMathNumber(
      groupFrom: DataLabelsFrom,
      dataLabel: DataLabel<string>,
      index?: BaseDropIndex | null,
    ) {
      let source

      switch (groupFrom) {
        case 'Drop':
          source = this.drops.find((drop) => drop.index === index)
          break
        case 'Point':
          // eslint-disable-next-line @typescript-eslint/no-this-alias
          source = this
          break

        default:
          break
      }

      return source?.dataset.get(dataLabel)?.value
    },
    getDisplayedString(
      groupFrom: DataLabelsFrom,
      dataLabel: DataLabel<string>,
      index?: BaseDropIndex | null,
    ) {
      const value = this.getMathNumber(groupFrom, dataLabel, index)

      return value ? value.displayedString() : ''
    },
    shouldBeOnMap,
    toBaseJSON() {
      return {
        ...base.toBaseJSON(),
        id: this.id,
        settings: this.settings.toJSON(),
        version: 1,
        index: this.index(),
        number: this.number(),
        date: formatToUTCDate(
          this.date.toString(),
          zone().report().project().database().timeZoneOffsetMinutes || 0,
        ),
        data: Array.from<DataValue<string>>(this.dataset.values())
          .filter((data) =>
            this.zone()
              .report()
              .dataLabels.groups.list()[1]
              .saveableChoices.includes(data.label),
          )
          .map((data) => data.toJSON()),
        information: this.information.map((field) => field.toJSON()),
        drops: this.drops.map((drop) => drop.toJSON()),
      }
    },
  }

  point.marker?.on('dragend', () => {
    const report = zone().report()
    const referencePath = report.referencePath()

    if (referencePath) {
      recalculeChainage(referencePath, report)
    }
  })

  createColorEffect(point)

  createLineEffect(point)

  createEffect(() => {
    if (point.shouldBeOnMap()) {
      createIconEffect(point)

      createTextEffect(point)

      createPopupEffect(point)

      createLockEffect(point)
    }
  })

  return point
}

const upgradeJSON = (json: JSONBasePointVAny): JSONBasePoint => {
  switch (json.version) {
    case 1:
    // upgrade
  }

  return json
}

const createIconEffect = <Zone extends BaseZone, Drop extends MachineDrop>(
  point: BasePoint<Drop, Zone>,
) => {
  createEffect(() => {
    point.icon?.setIcon(point.zone().report().settings.iconName())
  })
}

const createTextEffect = <Zone extends MachineZone, Drop extends MachineDrop>(
  point: BasePoint<Drop, Zone>,
) => {
  const text = createMemo(() => {
    switch (point.zone().report().project().settings.pointsState()) {
      case 'number':
        return String(point.number())

      case 'value':
        return point.onMapMathNumber()?.displayedString() ?? ''

      case 'nothing':
        return ''
    }
  })

  createEffect(() => {
    point.icon?.setText(text())
  })
}

const createColorEffect = <Zone extends MachineZone, Drop extends MachineDrop>(
  point: BasePoint<Drop, Zone>,
) => {
  const currentThreshold = createMemo(() => {
    const mathNumber = point.onMapMathNumber()

    const unit = mathNumber?.unit

    if (!unit) return

    return (
      Object.values(point.zone().report().thresholds.groups).find(
        (_group) => _group.unit === unit,
      ) as ThresholdsGroup<string>
    )?.choices.selected()
  })

  const color = createMemo(() => {
    if (point.zone().report().settings.colorization() === 'Zone') {
      return colors[point.zone().settings.color()]
    }

    const mathNumber = point.onMapMathNumber()

    if (!mathNumber) return

    return currentThreshold()?.getColor(
      mathNumber,
      point.zone().report().thresholds.colors,
    )
  })

  createEffect(() => {
    point.icon?.setColor(color())
  })
}

const createPopupEffect = <Zone extends MachineZone, Drop extends MachineDrop>(
  point: BasePoint<Drop, Zone>,
) => {
  const popup = createMemo(() => {
    let html = ``

    const appendToPopup = (label: string, value: string) => {
      html += `<p ><strong>${translate(label)}:</strong> <span id="point-${point.index()}-${label}">${value}</span></p>`
    }

    const timeZoneOffsetMinutes = store
      .selectedProject()
      ?.database()?.timeZoneOffsetMinutes

    appendToPopup(
      translate('Date'),
      formatToUTCDate(
        point.date,
        timeZoneOffsetMinutes ?? 0,
        'DD/MM/YYYY HH:mm:ss (UTC Z)',
      ),
    )
    appendToPopup(
      'Longitude',
      point.coordinates()?.lng.toLocaleString(undefined, {
        maximumFractionDigits: 6,
      }) ?? '',
    )
    appendToPopup(
      'Latitude',
      point.coordinates()?.lat.toLocaleString(undefined, {
        maximumFractionDigits: 6,
      }) ?? '',
    )

    point.dataset.forEach((dataValue) => {
      appendToPopup(
        dataValue.label.name,
        dataValue.value.displayedStringWithUnit(),
      )
    })

    point.information.forEach((field) => {
      appendToPopup(field.label, String(field.getValue()))
    })

    return html
  })

  if (!point.zone().report().settings.isRefpathEditing()) {
    createEffect(() => {
      point.marker?.setPopup(new Popup().setHTML(popup()))
    })
  } else {
    createEffect(() => {
      point.marker?.setPopup()
    })
  }
}

const createLockEffect = <Zone extends MachineZone, Drop extends MachineDrop>(
  point: BasePoint<Drop, Zone>,
) => {
  createEffect(() =>
    point.marker?.setDraggable(
      !point.zone().report().project().settings.arePointsLocked(),
    ),
  )
}

const createLineEffect = <Zone extends MachineZone, Drop extends MachineDrop>(
  point: BasePoint<Drop, Zone>,
) => {
  createEffect(() => {
    if (!point.settings.isVisible()) {
      point.zone().report().line.removePoint(point.id)
    }
  })
}
