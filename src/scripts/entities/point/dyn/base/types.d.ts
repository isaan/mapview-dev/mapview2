// ---
// JSON
// ---

type JSONBasePointVAny = JSONBasePoint

interface JSONBasePoint extends JSONMapboxPoint {
  readonly version: 1
  readonly number: number
  readonly id: string
  readonly index: number
  readonly date: string
  readonly data: JSONDataValue[]
  readonly information: JSONField[]
  readonly drops: JSONMachineDrop[]
  readonly settings: JSONPointSettings
}

// ---
// Object
// ---

interface BasePoint<
  Drop extends BaseDrop = MachineDrop,
  Zone extends BaseZone = BaseZone,
> extends MapboxPoint<JSONBasePoint> {
  readonly dataset: ReactiveMap<DataLabel, DataValue<string>>
  readonly id: string
  readonly date: Date
  readonly drops: Drop[]
  readonly icon: Icon | null
  readonly index: ASS<number>
  readonly information: Field[]
  readonly number: ASS<number>
  readonly onMapMathNumber: Accessor<MathNumber | undefined>
  readonly rawDataFile: ASS<ArrayBufferLike | null>
  readonly zone: ASS<Zone>
  readonly getMathNumber: (
    groupFrom: DataLabelsFrom,
    dataLabel: DataLabel<string>,
    index?: BaseDropIndex | null,
  ) => MathNumber | undefined
  readonly getDisplayedString: (
    groupFrom: DataLabelsFrom,
    dataLabel: DataLabel<string>,
    index?: BaseDropIndex | null,
  ) => string
  readonly shouldBeOnMap: () => boolean
}

interface PointSettings extends SerializableObject<JSONPointSettings> {
  readonly isVisible: ASS<boolean>
}
