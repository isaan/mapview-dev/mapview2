import { createMathNumber } from '/src/scripts'

export const createDataValue = (
  _value: Accessor<number> | JSONDataValueValue,
  label: DataLabel<string>,
) => {
  const value = createMathNumber(_value, label.unit)

  const dataValue: DataValue<string> = {
    label,
    value,
    rawValue: value.value,
    toJSON() {
      return {
        version: 1,
        label: this.label.name,
        value: Number.isNaN(this.rawValue()) ? 'NaN' : this.rawValue(),
      }
    },
    toExcel() {
      return Number.isNaN(this.rawValue()) ? null : this.value.toExcel(true)
    },
  }

  return dataValue
}

export const createDataValueFromJSON = (
  json: JSONDataValueVAny,
  list: DataLabel<string>[],
): DataValue<string> => {
  json = upgradeJSON(json)

  const label = list.find(
    (dataLabel) => dataLabel.name === json.label,
  ) as DataLabel<string>

  return createDataValue(
    typeof json.value === 'string' ? json.value : () => json.value as number,
    label,
  )
}

const upgradeJSON = (json: JSONDataValueVAny): JSONDataValue => {
  switch (json.version) {
    case 1:
    // upgrade
  }

  return json
}
