import { LngLat } from 'mapbox-gl'
import { beforeEach, describe, expect, Mock, test, vitest } from 'vitest'

import { createReferencePathFromJSON } from '/src/scripts/entities/referencePath'
import { importFile } from '/src/scripts/io/importer/file'
import { recalculeChainage } from '/src/scripts/utils/referencePath'
import { store } from '/src/store'
import { getFileFromPath } from '/src/tests/utils/files'

describe('Tests referencePath', async () => {
  const path = `${__dirname}/files/refpath.mpvz`
  const testData = getFileFromPath(path)
  const project = await importFile(testData)

  let map: mapboxgl.Map | null
  let report: Mock | null
  let json: JSONReferencePath | null

  beforeEach(() => {
    map = store.map()
    report = vitest.fn(() => ({
      settings: {
        isVisible: vitest.fn(() => true),
        isRefpathEditing: vitest.fn(() => true),
      },
      project: vitest.fn(() => ({
        units: {
          distance: { valueToLocaleString: vitest.fn((arg) => `${arg} m`) },
        },
      })),
    }))

    json = {
      version: 1,
      settings: {
        isVisible: true,
        version: 1,
      },
      origin: 0,
      points: [
        { version: 1, coordinates: { lng: 0, lat: 0 } },
        { version: 1, coordinates: { lng: 1, lat: 1 } },
      ],
    }
  })

  test('test createReferencePathFromJSON:', () => {
    expect(json).not.toBeNull()

    const referencePath = createReferencePathFromJSON(
      json as JSONReferencePath,
      map!,
      report!,
    )

    expect(referencePath).not.toBeNull()
    expect(referencePath.settings.isVisible()).toEqual(true)
    expect(referencePath.points).toBeDefined()
    expect(referencePath.points().length).toEqual(2)
    expect(referencePath.origin).toBeDefined()
    expect(referencePath.settings).toBeDefined()
    expect(referencePath.settings.isVisible()).toEqual(true)
    expect(referencePath.report).toBeDefined()
    expect(referencePath.report).toBe(report)
    expect(referencePath.line).toBeDefined()
  })

  test('test reset:', () => {
    const referencePath = createReferencePathFromJSON(
      json as JSONReferencePath,
      map!,
      report!,
    )
    referencePath.reset()
    expect(referencePath.points().length).toEqual(0)
  })

  test('should add a point correctly', () => {
    if (!json) return

    const refPath: ReferencePath = createReferencePathFromJSON(
      json,
      map!,
      report!,
    )
    const newCoordinates = new LngLat(2, 2)

    refPath.addPoint(newCoordinates)

    expect(refPath.points().length).toBe(json.points.length + 1)
    expect(
      refPath.points()[json.points.length].toBaseJSON().coordinates,
    ).toEqual(newCoordinates)
  })

  test('should convert to JSON correctly', () => {
    if (!json) return

    const refPath: ReferencePath = createReferencePathFromJSON(
      json,
      map!,
      report!,
    )
    const jsonOutput = refPath.toJSON()

    expect(jsonOutput.version).toBe(json.version)
    expect(jsonOutput.points.length).toBe(json.points.length)
    expect(jsonOutput.settings).toEqual(json.settings)
    expect(jsonOutput.origin).toBe(refPath.origin())
  })

  test('test chainage:', () => {
    if (project) {
      store.pushAndSelectProject(project)
      const rep = store.selectedReport()
      const referencePath = createReferencePathFromJSON(
        {
          version: 1,
          origin: 0,
          points: [
            {
              version: 1,
              coordinates: [2.195429500664517, 48.67044007160726],
            },
            {
              version: 1,
              coordinates: [2.196146267044867, 48.67113830146957],
            },
            {
              version: 1,
              coordinates: [2.1968573220702865, 48.671751125892314],
            },
            {
              version: 1,
              coordinates: [2.197587432528252, 48.6729287789679],
            },
            {
              version: 1,
              coordinates: [2.1990585427987526, 48.67378801549245],
            },
            {
              version: 1,
              coordinates: [2.201021793248003, 48.674940693849095],
            },
          ],
          settings: {
            version: 1,
            isVisible: true,
          },
        },
        map!,
        () => store.selectedReport() as unknown as BaseReport,
      )

      const chainage = [
        657, 628, 597, 564, 527, 509, 464, 441, 422, 414, 409, 403, 398, 387,
        378, 361, 302, 272, 242, 216, 184, 162, 123, 81, 40, 7,
      ]

      if (rep && referencePath) {
        const chainageDL = rep.dataLabels.table
          .list()[1]
          .group.choices.list()[3]

        recalculeChainage(referencePath, rep)
        rep.sortedPoints().forEach((point, index) => {
          expect(
            point.dataset.get(chainageDL)?.value.getLocaleString(),
          ).toEqual(chainage[index].toString())
        })
      }
    }
  })
})
