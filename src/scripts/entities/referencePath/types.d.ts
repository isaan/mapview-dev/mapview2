// ---
// JSON
// ---

interface JSONReferencePath {
  readonly version: 1
  readonly points: JSONMapboxPoint[]
  readonly settings: JSONReferencePathSettings
  readonly origin: number
}

interface JSONReferencePathSettings {
  readonly version: 1
  readonly isVisible: boolean
}

// ---
// Object
// ---

interface ReferencePath {
  readonly points: ASS<ReferencePathPoint[]>
  readonly line: ASS<ReferencePathLine>
  readonly settings: ReferencePathSettings
  readonly origin: ASS<number>
  readonly report: Accessor<BaseReport>
  readonly addPoint: (coordinates: mapboxgl.LngLat) => void
  readonly reset: () => void
  readonly toJSON: () => T
}

interface ReferencePathSettings {
  readonly isVisible: ASS<boolean>
  readonly toJSON: () => T
}
