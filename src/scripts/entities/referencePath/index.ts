import { createASS, createReferencePathLine } from '/src/scripts'
import { createReferencePathPointFromJSON } from '/src/scripts/entities/point/referencePath'
import { store } from '/src/store'

export const createReferencePathFromJSON = (
  json: JSONReferencePath,
  map: mapboxgl.Map,
  report: Accessor<BaseReport>,
): ReferencePath => {
  const seed = Math.random()
  const points = createASS<ReferencePathPoint[]>([], {
    equals: false,
  })

  if (typeof navigator !== 'undefined' && navigator.webdriver) {
    // @ts-expect-error expose the points for the tests
    window.getPoints = () => points()
  }

  const index = createASS(0)

  const settings: ReferencePathSettings = {
    isVisible: createASS(json.settings.isVisible),
    toJSON() {
      return {
        version: json.settings.version,
        isVisible: this.isVisible(),
      }
    },
  }

  const shouldBeOnMap = createMemo(
    () =>
      settings.isVisible() &&
      report().settings.isVisible() &&
      // eslint-disable-next-line sonarjs/different-types-comparison
      report().project() === store.selectedProject(),
  )
  const origin = createASS(0)

  const line = createReferencePathLine(
    shouldBeOnMap,
    points,
    map,
    seed.toString(),
    origin,
    report().project()?.units.distance,
  )

  const refPath: ReferencePath = {
    points,
    line: createASS(line),
    origin,
    settings,
    report,
    reset() {
      this.points().forEach((p) => {
        p.marker?.remove()
        p.dispose()
      })
      this.points.set([])
    },
    addPoint(coordinates: mapboxgl.LngLat) {
      index.set(index() + 1)
      const point = createReferencePathPointFromJSON(
        shouldBeOnMap,
        {
          version: 1,
          coordinates,
        },
        map,
        index(),
        refPath,
        seed,
      )

      this.points.set((current) => {
        current.push(point)
        return current
      })
    },
    toJSON() {
      return {
        version: json.version,
        points: this.points().map((point) => point.toBaseJSON()),
        settings: this.settings.toJSON(),
        origin: this.origin(),
      }
    },
  }

  refPath.points.set(
    json.points.map((p, i) => {
      return createReferencePathPointFromJSON(
        shouldBeOnMap,
        p,
        map,
        i,
        refPath,
        seed,
      )
    }),
  )

  return refPath
}
