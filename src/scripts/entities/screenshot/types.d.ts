interface Documents {
  readonly name: string
  readonly file: string
  readonly extension: string
}
