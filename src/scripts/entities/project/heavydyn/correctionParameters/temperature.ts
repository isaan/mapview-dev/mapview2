import { createASS, createSL, createWritableMathNumber } from '/src/scripts'

export const countryData: TemperatureCountrySourceList = [
  { code: 'fr', defaultReference: 15 },
  { code: 'be', defaultReference: 20 },
  { code: 'es', defaultReference: 20 },
]

export const defaultCountry = countryData[0]

export const createHeavydynProjectTemperatureCorrectionParametersFromJSON = (
  json: JSONHeavydynTemperatureCorrectionParametersVAny,
  units: HeavydynMathUnits,
) => {
  const upgradedJson = upgradeJSON(json)

  const temperatureCorrectionParameters: HeavydynTemperatureCorrectionParameters =
    {
      active: createASS(upgradedJson.active),
      country: createSL(countryData, {
        selected:
          countryData.find((data) => data.code === upgradedJson.country) ||
          defaultCountry,
      }),
      source: createSL(
        ['Tair', 'Tsurf', 'Tman', 'Custom'] as TemperatureSourceList,
        {
          selected: upgradedJson.source,
        },
      ),
      average: createSL(['Point', 'Zone', 'Report'] as TemperatureAverageList, {
        selected: upgradedJson.average,
      }),
      customValue: createWritableMathNumber(
        upgradedJson.customValue,
        units.temperature,
      ),
      reference: createWritableMathNumber(
        upgradedJson.reference,
        units.temperature,
      ),
      structureType: createSL(
        [
          {
            name: 'Flexible',
            k: 0.15,
          },
          {
            name: 'Thick flexible',
            k: 0.2,
          },
          {
            name: 'Mixed',
            k: 0.08,
          },
          {
            name: 'Semi-flexible',
            k: 0.04,
          },
        ] as TemperatureStructureTypeList,
        {
          selectedIndex: upgradedJson.structureType,
        },
      ),
      toJSON() {
        return {
          version: 3,
          country: this.country.selected()?.code ?? 'fr',
          active: this.active(),
          source: this.source.selected() ?? 'Tair',
          average: this.average.selected() ?? 'Zone',
          customValue: this.customValue.value(),
          reference: this.reference.value(),
          structureType: this.structureType.selectedIndex() ?? 0,
        }
      },
    }

  return temperatureCorrectionParameters
}

const upgradeJSON = (
  json: JSONHeavydynTemperatureCorrectionParametersVAny,
): JSONHeavydynTemperatureCorrectionParameters => {
  switch (json.version) {
    case 1:
      json = {
        ...json,
        version: 2,
        reference: json.temperatureTo,
        source: json.temperatureFromSource,
      }
    // eslint-disable-next-line no-fallthrough
    case 2:
      json = {
        ...json,
        version: 3,
        country: 'fr',
      }
  }

  return json
}
