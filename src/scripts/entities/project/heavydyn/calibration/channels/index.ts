export const createHeavydynCalibrationsChannelsFromJSONArray = (
  jsonArray: JSONChannelVAny[],
): JSONChannel[] => upgradeJSONArray(jsonArray)

const upgradeJSONArray = (jsonArray: JSONChannelVAny[]): JSONChannel[] =>
  jsonArray.map((c) => {
    let cV: JSONChannelVAny = c
    switch (c.version) {
      case 1: {
        const channelNumber = c.acquisition
        const cV2 = {
          version: 2,
          name: c.name,
          position: c.position,
          gain: c.gain,
          channelNumber,
          type: c.type,
          v0: c.v0,
        } as JSONChannelV2
        cV = cV2
      }
      // eslint-disable-next-line no-fallthrough
      case 2: {
        if ('channelNumber' in cV) {
          const cV3 = {
            version: 3,
            name: cV.name,
            position: cV.position,
            gain: cV.gain,
            channelNumber: cV.channelNumber,
            type: cV.type,
            v0: cV.v0,
          } as JSONChannel

          cV = cV3
        }
      }
    }
    return cV as JSONChannel
  })
