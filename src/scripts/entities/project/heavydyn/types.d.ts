// ---
// JSON
// ---

type JSONHeavydynProjectVAny = JSONHeavydynProject

interface JSONHeavydynProject {
  readonly version: 1
  readonly machine: 'Heavydyn'
  readonly base: JSONBaseProject
  readonly distinct: JSONHeavydynProjectDistinct
}

interface JSONHeavydynProjectDistinct {
  readonly version: 1
  readonly units: JSONHeavydynUnits
  readonly calibrations: JSONHeavydynCalibrationsVAny
  readonly correctionParameters: JSONHeavydynCorrectionParameters
}

type JSONHeavydynCalibrationsVAny = JSONHeavydynCalibrations

interface JSONHeavydynCalibrations {
  readonly version: 1
  readonly date: string
  readonly dPlate: number
  readonly channels: JSONChannelVAny[]
  readonly sensors: JSONSensor[]
}

type JSONChannelVAny = JSONChannel | JSONChannelV1 | JSONChannelV2

interface JSONChannel {
  readonly version: 3
  readonly name: string
  readonly position: string
  readonly gain: number
  readonly channelNumber: number
  readonly type: 'LoadCell' | 'Geophone'
  readonly v0: number
  readonly adc?: number
  [key: string]: number | string
}

interface JSONChannelV2 {
  readonly version: 2
  readonly name: string
  readonly position: string
  readonly gain: number
  readonly channelNumber: number
  readonly type: 'LoadCell' | 'Geophone'
  readonly v0: number
  [key: string]: number | string
}

interface JSONChannelV1 {
  readonly version: 1
  readonly name: string
  readonly position: string
  readonly gain: number
  readonly acquisition: number
  readonly type: 'LoadCell' | 'Geophone'
  readonly v0: number
}

interface JSONSensor {
  readonly version: 1
  readonly name: string
  readonly gain: number
  readonly type: 'AirTemp' | 'SurfTemp' | 'Dmi'
  readonly v0: number
  [key: string]: number | string
}

// ---
// Object
// ---

interface HeavydynProject
  extends HeavydynObject<JSONHeavydynProject>,
    BaseProject<HeavydynReport, HeavydynMathUnits>,
    DisposableObject {
  readonly calibrations: HeavydynCalibrations
  readonly correctionParameters: HeavydynCorrectionParameters
}

interface HeavydynCalibrations {
  readonly date: Date
  readonly dPlate: number
  readonly channels: JSONChannel[]
  readonly sensors: JSONSensor[]
}
