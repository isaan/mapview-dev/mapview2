// ---
// JSON
// ---

interface JSONInfoDatabase {
  readonly version: 2
  readonly software: string
  readonly softwareVersion: string
  readonly local: string
  readonly timeZone: string
  readonly timeZoneOffsetMinutes: number
}

interface JSONInfoDatabaseV1 {
  readonly version: 1 | 3
  readonly software: string
  readonly softwareVersion: string
  readonly local: string
  readonly timeZone: string
}

type JSONInfoDatabaseVAny = JSONInfoDatabaseV1 | JSONInfoDatabase

// ---
// Object
// ---

interface InfoDatabase extends SerializableObject<JSONInfoDatabase> {
  readonly version: 2
  readonly software: string
  readonly softwareVersion: string
  readonly local: string
  readonly timeZone: string
  readonly timeZoneOffsetMinutes: number
}
