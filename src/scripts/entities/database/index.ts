export const createInfoDatabaseFromJSON = (json: JSONInfoDatabaseVAny) => {
  json = upgradeJSON(json)

  const database: InfoDatabase = {
    version: 2,
    software: json.software,
    softwareVersion: json.softwareVersion,
    local: json.local,
    timeZone: json.timeZone,
    timeZoneOffsetMinutes: json.timeZoneOffsetMinutes,
    toJSON() {
      return {
        version: 2,
        software: this.software,
        softwareVersion: this.softwareVersion,
        local: this.local,
        timeZone: this.timeZone,
        timeZoneOffsetMinutes: this.timeZoneOffsetMinutes,
      }
    },
  }

  return database
}
const upgradeJSON = (json: JSONInfoDatabaseVAny): JSONInfoDatabase => {
  let timeZoneOffsetMinutes = 0

  switch (json.timeZone) {
    case 'Paris, Madrid':
    case 'Midden-Europese standaardtijd':
      timeZoneOffsetMinutes = 120
      break
    case 'Europe de l’Ouest':
    case 'heure normale d’Europe centrale':
      timeZoneOffsetMinutes = 60
      break
  }

  if (json.software === 'Minidyn' && json.version === 3) {
    return {
      ...json,
      version: 2,
      timeZoneOffsetMinutes,
    }
  }

  switch (json.version) {
    case 1: {
      const jsonv2: JSONInfoDatabase = {
        ...json,
        version: 2,
        timeZoneOffsetMinutes,
      }

      json = jsonv2
    }
  }

  return json as JSONInfoDatabase
}
