import { createMapboxLine } from '/src/scripts/entities/line/base'
import { lineLayerId } from '/src/scripts/entities/map'
import { store } from '/src/store'

export const createLine = (
  reportSettings: Accessor<BaseReportSettings>,
  projectSettings: Accessor<BaseProjectSettings>,
  points: Accessor<BasePoint[]>,
  map: mapboxgl.Map | null,
): Line => {
  const shouldBeOnMap = createMemo(
    () =>
      reportSettings().isVisible() &&
      projectSettings().arePointsLinked() &&
      store.selectedProject()?.settings === projectSettings(),
  )

  const base = createMapboxLine(shouldBeOnMap, lineLayerId, points, map, {
    lineColor: (point) => point.icon?.color() ?? 'gray',
    lineWidth: 6,
  })

  const line: Line = {
    ...base,
  }

  return line
}
