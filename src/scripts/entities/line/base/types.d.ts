interface MapboxLine {
  readonly features: Accessor<Feature<Geometry, GeoJsonProperties>[]>
  readonly removePoint: (id: string) => void
  readonly refresh: VoidFunction
  readonly reset: VoidFunction
}
