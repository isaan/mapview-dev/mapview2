import { createLazyMemo } from '@solid-primitives/memo'

export const createMapboxLine = <Point extends MapboxPoint>(
  visibleOnMap: Accessor<boolean>,
  layerId: string,
  points: Accessor<Point[]>,
  map: mapboxgl.Map | null,
  options?: {
    lineColor?: string | ((point: Point) => string)
    lineWidth?: number
    id?: string
  },
): MapboxLine => {
  const shouldBeOnMap = createMemo(() => visibleOnMap())

  const features = createLazyMemo<
    GeoJSON.Feature<GeoJSON.Geometry, GeoJSON.GeoJsonProperties>[][]
  >(() => {
    const hiddenPoints = points().filter((point) => !point.settings.isVisible())
    const visiblePoints = points().filter((point) => point.settings.isVisible())

    const mapFeatures = map?.queryRenderedFeatures({
      layers: [layerId],
    })

    if (!mapFeatures) {
      return []
    }

    const uniqueFeatures = getUniqueFeatures(mapFeatures, 'id')

    // @ts-expect-error mapbox types are not correct see: https://github.com/mapbox/mapbox-gl-js/issues/13203
    const toDelete: GeoJSON.Feature<
      GeoJSON.Geometry,
      GeoJSON.GeoJsonProperties
    >[] = uniqueFeatures
      .filter((f) =>
        hiddenPoints.find((p) => f.properties?.id.toString().includes(p.id)),
      )
      .map((f) => ({
        id: f.properties?.id,
        properties: {
          id: f.properties?.id,
        },
        geometry: null,
      }))

    const lines: GeoJSON.Feature<
      GeoJSON.Geometry,
      GeoJSON.GeoJsonProperties
    >[] = visiblePoints.slice(1).map((point, index) => {
      const previousPoint = visiblePoints[index]

      const featureID = `${point.id}-${previousPoint.id}`

      uniqueFeatures
        .filter((f) => f.properties?.id.toString().includes(point.id))
        .forEach((f) => {
          if (
            f.properties?.id !== featureID &&
            f.properties?.id !== `${visiblePoints[index + 2]?.id}-${point.id}`
          ) {
            toDelete.push({
              id: f.properties?.id,
              // @ts-expect-error mapbox types are not correct see: https://github.com/mapbox/mapbox-gl-js/issues/13203
              geometry: null,
              properties: { id: f.properties?.id },
            })
          }
        })

      return {
        id: featureID,
        type: 'Feature',
        properties: {
          color:
            typeof options?.lineColor === 'function'
              ? options.lineColor(point)
              : options?.lineColor,
          id: featureID,
        },
        geometry: {
          type: 'LineString',
          coordinates: [
            [
              previousPoint.coordinates()?.lng ?? 0,
              previousPoint.coordinates()?.lat ?? 0,
            ],
            [point.coordinates()?.lng ?? 0, point.coordinates()?.lat ?? 0],
          ],
        },
      }
    })

    return [lines, getUniqueFeatures(toDelete, 'id')]
  })

  const addFeaturesToMap = () => {
    const lineSource = map?.getSource<mapboxgl.GeoJSONSource>(layerId)
    if (lineSource) {
      if (features()[0].length > 0) {
        lineSource.updateData({
          type: 'FeatureCollection',
          features: features()[0],
        })
      }
      // look likes mapbox doesn't like to add and delete line in the same call
      // so we need to do it in two steps
      if (features()[1].length > 0) {
        setTimeout(() => {
          lineSource.updateData({
            type: 'FeatureCollection',
            features: features()[1],
          })
        }, 10)
      }
    }
  }

  createEffect(() => {
    if (map) {
      if (shouldBeOnMap()) {
        addFeaturesToMap()
      } else {
        line.reset()
      }
    }
  })

  const line: MapboxLine = {
    features: () => features()[0] || [],
    removePoint(pointId: string) {
      const mapFeatures = map?.queryRenderedFeatures({ layers: [layerId] })
      const lineSource = map?.getSource<mapboxgl.GeoJSONSource>(layerId)

      const deletedFeatures: GeoJSON.Feature<
        GeoJSON.Geometry,
        GeoJSON.GeoJsonProperties
      >[] = []

      if (mapFeatures && lineSource) {
        getUniqueFeatures(mapFeatures, 'id').forEach((feature) => {
          const featureId = feature.properties?.id
          if (featureId?.includes(pointId)) {
            deletedFeatures.push({
              id: featureId,
              // @ts-expect-error mapbox types are not correct see: https://github.com/mapbox/mapbox-gl-js/issues/13203
              geometry: null,
            })
          }
        })

        lineSource.updateData({
          type: 'FeatureCollection',
          features: deletedFeatures,
        })
      }
    },
    refresh() {
      if (shouldBeOnMap()) {
        addFeaturesToMap()
      }
    },
    reset() {
      const lineSource = map?.getSource<mapboxgl.GeoJSONSource>(layerId)

      const mapFeatures = map?.queryRenderedFeatures({ layers: [layerId] })

      if (lineSource && mapFeatures && mapFeatures?.length > 0) {
        lineSource.updateData({
          type: 'FeatureCollection',
          // @ts-expect-error mapbox types are not correct see: https://github.com/mapbox/mapbox-gl-js/issues/13203
          features: getUniqueFeatures(mapFeatures, 'id')
            .filter((f) =>
              options?.id
                ? f.properties?.id.includes(options.id.toString())
                : true,
            )
            .map((f) => ({
              id: f.properties?.id,
              geometry: null,
            })),
        })
      }
    },
  }

  return line
}

export const sortPoints = <Points extends BasePoint[] = BasePoint[]>(
  points: Points,
) => points.sort((pointA, pointB) => pointA.index() - pointB.index())

const getUniqueFeatures = (
  features: GeoJSON.Feature<GeoJSON.Geometry, GeoJSON.GeoJsonProperties>[],
  comparatorProperty: string,
) => {
  const uniqueIds = new Set()
  const uniqueFeatures: GeoJSON.Feature<
    GeoJSON.Geometry,
    GeoJSON.GeoJsonProperties
  >[] = []
  features.forEach((feature) => {
    const id = feature.properties?.[comparatorProperty]
    if (!uniqueIds.has(id)) {
      uniqueIds.add(id)
      uniqueFeatures.push(feature)
    }
  })
  return uniqueFeatures
}

if (typeof navigator !== 'undefined' && navigator.webdriver) {
  // @ts-expect-error expose the map for the tests
  window.getUniqueFeatures = getUniqueFeatures
}
