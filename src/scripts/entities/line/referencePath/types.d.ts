interface ReferencePathLine extends MapboxLine {
  readonly addPoint: (point: ReferencePathPoint) => void
}
