import { debounce } from '@solid-primitives/scheduled'
import { length } from '@turf/turf'

import {
  referencePathDistanceLayerId,
  referencePathLayerId,
} from '/src/scripts/entities/map'
import { convertValueFromUnitAToUnitB } from '/src/scripts/entities/math'

import { createMapboxLine } from '../base'

export const createReferencePathLine = (
  visibleOnMap: Accessor<boolean>,
  points: ASS<ReferencePathPoint[]>,
  map: mapboxgl.Map | null,
  id: string,
  origin: Accessor<number>,
  unit: MathUnit<string>,
): ReferencePathLine => {
  const idDistance = `refpath-line-distance-${id}`

  const base = createMapboxLine(
    visibleOnMap,
    referencePathLayerId,
    points,
    map,
    { id },
  )

  const distance = createMemo(() => {
    let d = origin()

    base.features().forEach((feature) => {
      d += convertValueFromUnitAToUnitB(length(feature), 'km', unit.baseUnit)
    })

    return d
  })

  const distanceToLocaleString = createMemo(() =>
    unit.valueToLocaleString(distance(), { appendUnitToString: true }),
  )

  const addDistanceToMap = () => {
    const distanceSource = map?.getSource<mapboxgl.GeoJSONSource>(
      referencePathDistanceLayerId,
    )

    if (distanceSource) {
      distanceSource.updateData({
        type: 'FeatureCollection',
        features:
          base.features().length === 0
            ? []
            : [
                {
                  id: idDistance,
                  type: 'Feature',
                  properties: {
                    description: distanceToLocaleString(),
                    id: idDistance,
                  },
                  geometry: {
                    type: 'Point',
                    coordinates: (
                      base.features()[base.features().length - 1]
                        ?.geometry as GeoJSON.LineString
                    ).coordinates[1],
                  },
                },
              ],
      })
    }
  }

  const removeDistanceFromMap = debounce(() => {
    const refPathDistanceSources = map?.getSource<mapboxgl.GeoJSONSource>(
      referencePathDistanceLayerId,
    )

    const mapFeatures = map
      ?.queryRenderedFeatures({
        layers: [referencePathDistanceLayerId],
      })
      .filter((feature) => feature.properties?.id === idDistance)

    if (
      refPathDistanceSources &&
      mapFeatures &&
      mapFeatures?.length > 0 &&
      refPathDistanceSources._loaded
    ) {
      refPathDistanceSources.updateData({
        type: 'FeatureCollection',
        features: [
          {
            id: idDistance,
            // @ts-expect-error mapbox types are not correct see: https://github.com/mapbox/mapbox-gl-js/issues/13203
            geometry: null,
          },
        ],
      })
    }
  }, 200)

  const shouldBeOnMap = createMemo(() => visibleOnMap())

  createEffect(() => {
    if (map) {
      if (shouldBeOnMap()) {
        addDistanceToMap()
      } else {
        removeDistanceFromMap()
      }
    }
  })

  const line: ReferencePathLine = {
    ...base,
    addPoint(point: ReferencePathPoint) {
      points().push(point)
    },
    refresh() {
      if (shouldBeOnMap()) {
        addDistanceToMap()
      }

      setTimeout(() => {
        base.refresh()
      }, 10)
    },
    reset() {
      removeDistanceFromMap()

      setTimeout(() => {
        base.reset()
      }, 10)
    },
  }

  createEffect(() => {
    if (base.features().length <= 0) {
      line.reset()
    }
  })

  return line
}
