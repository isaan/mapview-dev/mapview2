// ---
// JSON
// ---

type JSONBaseReportVAny =
  | JSONBaseReport
  | JSONBaseReportV1
  | JSONBaseReportV2
  | JSONBaseReportV3

interface JSONBaseReport {
  readonly version: 4
  readonly id: string
  readonly dataLabels: JSONBaseDataLabels
  readonly information: JSONField[]
  readonly name: string
  readonly platform: JSONField[]
  readonly referencePath: JSONReferencePath
  readonly settings: JSONReportSettings
  readonly thresholds: JSONBaseThresholdsSettings
  readonly zones: JSONMachineZone[]
}

interface JSONBaseReportV3 {
  readonly version: 3
  readonly dataLabels: JSONBaseDataLabels
  readonly information: JSONField[]
  readonly name: string
  readonly platform: JSONField[]
  readonly referencePath: JSONReferencePath
  readonly settings: JSONReportSettings
  readonly thresholds: JSONBaseThresholdsSettings
  readonly zones: JSONMachineZone[]
}

interface JSONBaseReportV2 {
  readonly version: 2
  readonly dataLabels: JSONBaseDataLabels
  readonly information: JSONField[]
  readonly name: string
  readonly platform: JSONField[]
  readonly settings: JSONReportSettings
  readonly thresholds: JSONBaseThresholdsSettings
  readonly zones: JSONMachineZone[]
}

interface JSONBaseReportV1 {
  readonly version: 1
  readonly dataLabels: JSONBaseDataLabels
  readonly information: JSONField[]
  readonly name: string
  readonly platform: JSONField[]
  readonly screenshots: Documents[]
  readonly settings: JSONReportSettings
  readonly thresholds: JSONBaseThresholdsSettings
  readonly zones: JSONMachineZone[]
}

type JSONReportSettingsVAny = JSONReportSettings | JSONReportSettingsV1

interface JSONReportSettings {
  readonly version: 2
  readonly iconName: IconNameVAny
  readonly isVisible: boolean
  readonly isRefPathEditing: boolean
  readonly colorization: ReportColorization
  readonly groupBy: ReportGroupBy
}

interface JSONReportSettingsV1 {
  readonly version: 1
  readonly iconName: IconNameVAny
  readonly isVisible: boolean
  readonly colorization: ReportColorization
  readonly groupBy: ReportGroupBy
}

interface JSONBaseThresholdsSettings {
  readonly version: 1
  readonly inputs: JSONThresholdInputs
  readonly colors: JSONThresholdColorsVAny
}

type JSONThresholdColorsVAny = JSONThresholdColors | JSONThresholdColorsV1

interface JSONThresholdColors {
  readonly version: 2
  readonly low: ColorName
  readonly middle: ColorName
  readonly high: ColorName
}

interface JSONThresholdColorsV1 {
  readonly version: 1
  readonly low: ColorNameV1
  readonly middle: ColorNameV1
  readonly high: ColorNameV1
}

interface JSONThresholdInputs {
  readonly version: 1
  readonly isRequiredARange: boolean
  readonly isOptionalARange: boolean
}

type ReportColorization = 'Threshold' | 'Zone'

type ReportGroupBy = 'Number' | 'Zone'

// ---
// Object
// ---

interface BaseReport<
  Project extends BaseProject = BaseProject,
  Zone extends BaseZone = MachineZone,
  DataLabels extends BaseDataLabels = MachineDataLabels,
  Thresholds extends BaseThresholds = BaseThresholds,
> extends BaseObject<JSONBaseReport>,
    Entity<'Report'> {
  readonly id: string
  readonly album: ASS<Documents[]>
  readonly dataLabels: DataLabels
  readonly exportablePoints: Accessor<BasePoint[]>
  readonly fitOnMap: VoidFunction
  readonly information: Field[]
  readonly line: Line
  readonly name: Field
  readonly platform: Field[]
  readonly project: ASS<Project>
  readonly referencePath: ASS<ReferencePath | undefined>
  readonly settings: BaseReportSettings
  readonly sortedPoints: Accessor<BasePoint[]>
  readonly thresholds: Thresholds
  readonly zones: ASS<Zone[]>
}

interface BaseReportSettings {
  readonly iconName: ASS<IconName>
  readonly isVisible: ASS<boolean>
  readonly isRefpathEditing: ASS<boolean>
  readonly colorization: ASS<ReportColorization>
  readonly groupBy: ASS<ReportGroupBy>
}
