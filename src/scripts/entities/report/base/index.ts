import {
  createASS,
  createFieldFromJSON,
  createLine,
  flyToPoints,
  getIndexOfSelectedInSelectableList,
  upgradeColorNameFromV1ToV2,
} from '/src/scripts'
import { sortPoints } from '/src/scripts/entities/line/base'
import { createReferencePathFromJSON } from '/src/scripts/entities/referencePath'
import { getUniqueId } from '/src/scripts/utils/id'

export const createBaseReportFromJSON = <
  Zone extends MachineZone,
  DataLabels extends BaseDataLabels,
  ThresholdsGroups extends BaseThresholdsGroups,
  Thresholds extends BaseThresholds<ThresholdsGroups>,
  Project extends BaseProject,
>(
  json: JSONBaseReportVAny,
  map: mapboxgl.Map | null,
  parameters: {
    zones: Zone[]
    project: Project
    dataLabels: DataLabels
    platform: JSONField[]
    information: JSONField[]
    thresholdsGroups: ThresholdsGroups
  },
) => {
  json = upgradeJSON(json)

  const thresholds = createThresholdsFromJSON(
    json.thresholds,
    parameters.thresholdsGroups,
  ) as Thresholds

  const project = createASS(parameters.project)

  const settings = createSettingsFromJSON(json.settings)

  const zones = createASS(parameters.zones, {
    equals: false,
  })

  const sortedPoints = createMemo(() =>
    sortPoints(zones().flatMap((zone) => zone.points() as BasePoint[])),
  )

  const line = createLine(
    () => settings,
    () => project().settings,
    sortedPoints,
    map,
  )

  const report: BaseReport<Project, Zone, DataLabels, Thresholds> = {
    kind: 'Report',
    id: json.id,
    name: createFieldFromJSON(
      {
        version: 1,
        label: 'Name',
        value: json.name,
        settings: {
          version: 1,
        },
      },
      parameters.project.database().timeZoneOffsetMinutes,
    ),
    settings,
    album: createASS([], { equals: false }),
    dataLabels: parameters.dataLabels,
    thresholds,
    sortedPoints,
    zones,
    line,
    platform: parameters.platform.map((field: JSONField) =>
      createFieldFromJSON(
        field,
        parameters.project.database().timeZoneOffsetMinutes,
      ),
    ),
    information: parameters.information.map((field: JSONField) =>
      createFieldFromJSON(
        field,
        parameters.project.database().timeZoneOffsetMinutes,
      ),
    ),
    project,
    referencePath: createASS<ReferencePath | undefined>(undefined),
    fitOnMap() {
      flyToPoints(map, this.sortedPoints())
    },
    exportablePoints: createMemo(() =>
      sortedPoints().filter((point) => point.settings.isVisible()),
    ),
    toBaseJSON(): JSONBaseReport {
      return {
        version: 4,
        id: this.id,
        name: this.name.value() as string,
        dataLabels: this.dataLabels.toBaseJSON(),
        thresholds: {
          version: 1,
          colors: {
            version: 2,
            high: this.thresholds.colors.high(),
            middle: this.thresholds.colors.middle(),
            low: this.thresholds.colors.low(),
          },
          inputs: {
            version: 1,
            isRequiredARange: this.thresholds.inputs.isRequiredARange(),
            isOptionalARange: this.thresholds.inputs.isOptionalARange(),
          },
        },
        zones: this.zones().map((zone) => zone.toJSON()),
        settings: {
          version: 2,
          colorization: this.settings.colorization(),
          groupBy: this.settings.groupBy(),
          iconName: this.settings.iconName(),
          isVisible: this.settings.isVisible(),
          isRefPathEditing: this.settings.isRefpathEditing(),
        },
        platform: this.platform.map((field) => field.toJSON()),
        information: this.information.map((field) => field.toJSON()),
        referencePath: this.referencePath()?.toJSON(),
      }
    },
  }

  report.referencePath.set(
    createReferencePathFromJSON(
      json.referencePath,
      map!,
      () => report as unknown as BaseReport,
    ),
  )

  return report
}

const upgradeJSON = (json: JSONBaseReportVAny): JSONBaseReport => {
  switch (json.version) {
    case 1: {
      json = {
        version: 2,
        name: json.name,
        dataLabels: json.dataLabels,
        thresholds: json.thresholds,
        zones: json.zones,
        settings: json.settings,
        platform: json.platform,
        information: json.information,
      }
    }
    // eslint-disable-next-line no-fallthrough
    case 2: {
      json = {
        ...json,
        version: 3,
        referencePath: {
          version: 1,
          origin: 0,
          points: [],
          settings: {
            version: 1,
            isVisible: false,
          },
        },
      }
    }
    // eslint-disable-next-line no-fallthrough
    case 3: {
      json = {
        ...json,
        id: getUniqueId(),
        version: 4,
      }
    }
  }

  return json
}

const createSettingsFromJSON = (
  json: JSONReportSettingsVAny,
): BaseReportSettings => {
  json = upgradeSettingJSON(json)

  return {
    colorization: createASS(json.colorization),
    groupBy: createASS(json.groupBy),
    iconName: createASS(
      json.iconName === 'HexagonAlt' ? 'Hexagon' : json.iconName,
    ),
    isVisible: createASS(json.isVisible),
    isRefpathEditing: createASS(false),
  }
}

const upgradeSettingJSON = (
  json: JSONReportSettingsVAny,
): JSONReportSettings => {
  switch (json.version) {
    case 1: {
      const jsonV2: JSONReportSettings = {
        ...json,
        version: 2,
        isRefPathEditing: false,
      }

      json = jsonV2
    }
  }

  return json
}

const createThresholdsFromJSON = (
  json: JSONBaseThresholdsSettings,
  thresholdsGroups: BaseThresholdsGroups,
): BaseThresholds<BaseThresholdsGroups> => {
  const jsonColors = upgradeThresholdsColorsJSON(json.colors)

  return {
    groups: thresholdsGroups,
    colors: {
      high: createASS(jsonColors.high),
      middle: createASS(jsonColors.middle),
      low: createASS(jsonColors.low),
    },
    inputs: {
      isRequiredARange: createASS(json.inputs.isRequiredARange),
      isOptionalARange: createASS(json.inputs.isOptionalARange),
    },
  }
}
const upgradeThresholdsColorsJSON = (
  json: JSONThresholdColorsVAny,
): JSONThresholdColors => {
  switch (json.version) {
    case 1:
      json = {
        ...json,
        version: 2,
        high: upgradeColorNameFromV1ToV2(json.high),
        middle: upgradeColorNameFromV1ToV2(json.middle),
        low: upgradeColorNameFromV1ToV2(json.low),
      }
  }

  return json
}

export const convertThresholdsConfigurationToJSON = (
  group: ThresholdsGroup<string>,
): JSONDistinctThresholdsConfiguration => {
  const customThresholdIndex: CustomThresholdIndex = 0

  return {
    version: 1,
    selectedIndex: getIndexOfSelectedInSelectableList(group.choices) ?? 0,
    custom: (
      group.choices.list().slice()[
        customThresholdIndex
      ] as ThresoldsList[CustomThresholdIndex]
    ).toJSON(),
  }
}
