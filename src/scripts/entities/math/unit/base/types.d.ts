type JSONBaseMathUnitVAny<PossibleUnits> =
  | JSONBaseMathUnit<PossibleUnits>
  | JSONBaseMathUnitV1<PossibleUnits>

interface JSONBaseMathUnit<PossibleUnits> {
  readonly version: 2
  readonly currentUnit: PossibleUnits
  readonly currentPrecision: number
  readonly format: PossibleFormats
}

interface JSONBaseMathUnitV1<PossibleUnits> {
  readonly version: 1
  readonly currentUnit: PossibleUnits
  readonly currentPrecision: number
}

interface BaseMathUnit<PossibleUnits> extends BaseObject<JSONBaseMathUnit> {
  readonly name: UnitName
  readonly baseUnit: string
  readonly possibleSettings: [PossibleUnits, number][]
  readonly possiblePrecisions: number[]
  readonly readOnly: boolean
  readonly currentUnit: ASS<PossibleUnits>
  readonly currentPrecision: ASS<number>
  readonly format?: PossibleFormats
  readonly getAverage: (values: number[]) => number
  readonly currentToBase: (value: number) => number
  readonly baseToCurrent: (value: number) => number
  readonly checkValidity: (value: number) => boolean
  readonly valueToLocaleString: (
    value: number,
    options?: MathUnitGetLocaleStringOptions,
  ) => string
}
