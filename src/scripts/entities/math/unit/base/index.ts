import { numberToLocaleString } from '/src/locales'
import {
  convertValueFromUnitAToUnitB,
  createASS,
  defaultInvalidValueReplacement,
  defaultKo,
} from '/src/scripts'

export const createBaseMathUnit = <PossibleUnits extends string>(
  name: UnitName,
  json: JSONBaseMathUnitVAny<PossibleUnits>,
  baseUnit: string,
  possibleSettings: [PossibleUnits, number][],
  options?: {
    possiblePrecisions?: number[]
    step?: number
    averageFunction?: AverageFunction
    readOnly?: true
    invalidReplacement?: string
    checkValidity?: (value: number) => boolean
  },
): BaseMathUnit<PossibleUnits> => {
  json = upgradeJSON(json)

  const currentUnit = createASS(json.currentUnit || possibleSettings[0][0])
  const possiblePrecisions = options?.possiblePrecisions || [0, 1, 2]
  const currentPrecision = createASS(
    json.currentPrecision || possibleSettings[0][1],
  )
  const readOnly = options?.readOnly || false
  const invalidReplacement =
    options?.invalidReplacement ?? defaultInvalidValueReplacement

  const format = createASS(json.format)

  const baseMathUnits: BaseMathUnit<PossibleUnits> = {
    name,
    baseUnit,
    possibleSettings,
    currentUnit,
    possiblePrecisions,
    currentPrecision,
    readOnly,
    format: 'format' in json ? json.format : 'dec',
    getAverage(values) {
      const filteredValues: number[] = values.filter((value) =>
        this.checkValidity(value),
      )

      return filteredValues.length > 0
        ? filteredValues.reduce(
            (total, currentValue) => total + currentValue,
            0,
          ) / filteredValues.length
        : 0
    },
    currentToBase(value) {
      return convertValueFromUnitAToUnitB(
        value,
        this.currentUnit(),
        this.baseUnit,
      )
    },
    baseToCurrent(value) {
      return convertValueFromUnitAToUnitB(
        value,
        this.baseUnit,
        this.currentUnit(),
      )
    },
    checkValidity(value) {
      return !Number.isNaN(value) && (options?.checkValidity?.(value) ?? true)
    },
    valueToLocaleString(value, parseOptions = {}) {
      if (Number.isNaN(value)) return defaultKo

      let valueString

      if (this.checkValidity(value)) {
        const numberToLocaleOptions = {
          locale: parseOptions.locale,
          precision: parseOptions.precision,
        }

        numberToLocaleOptions.precision ??= this.currentPrecision()

        value = convertValueFromUnitAToUnitB(
          value,
          this.baseUnit,
          parseOptions.unit ?? this.currentUnit(),
        )

        if (parseOptions.format === 'sci') {
          valueString = value.toExponential(numberToLocaleOptions.precision)
        } else {
          valueString = `${numberToLocaleString(value, parseOptions)}`.trim()
        }
      } else {
        valueString = invalidReplacement
      }

      const localeString: string = `${valueString.replaceAll('\u202f', ' ')} ${
        parseOptions.appendUnitToString ? this.currentUnit() : ''
      }`.trim()

      return parseOptions.removeSpaces
        ? localeString.replaceAll(' ', '')
        : localeString
    },
    toBaseJSON(): JSONBaseMathUnit<PossibleUnits> {
      return {
        version: 2,
        currentUnit: this.currentUnit(),
        currentPrecision: this.currentPrecision(),
        format: format(),
      }
    },
  }

  return baseMathUnits
}

const upgradeJSON = <PossibleUnits extends string>(
  json: JSONBaseMathUnitVAny<PossibleUnits>,
): JSONBaseMathUnit<PossibleUnits> => {
  switch (json.version) {
    case 1: {
      const jsonV2: JSONBaseMathUnit<PossibleUnits> = {
        ...json,
        version: 2,
        format: 'dec',
      }
      json = jsonV2
    }
  }

  return json
}
