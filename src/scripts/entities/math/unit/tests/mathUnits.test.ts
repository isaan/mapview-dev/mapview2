import { describe, expect, test } from 'vitest'

import { createBaseMathUnit, createMathUnit } from '/src/scripts/entities'
import { loadCSV, mathUnitsCSVHeaders } from '/src/tests/'

const getUnit = (csvData: MathUnitData) => {
  let unit: HeavydynUnits = {} as HeavydynUnits

  const commonJSON = {
    currentPrecision: csvData.precision,
    currentUnit: csvData.unit,
  }

  switch (csvData.mathUnit) {
    case 'deflection':
      unit = createMathUnit(
        'Deflection',
        {
          ...commonJSON,
          max: csvData.maxValue!,
          version: 1,
        },
        'm',
        [
          ['mm', 0],
          ['1/100 mm', 0],
          ['um', 0],
        ],
        {
          checkValidity: (value) => value >= 0,
        },
      )

      break
    case 'modulus':
      unit = createMathUnit(
        'Modulus',
        {
          ...commonJSON,
          max: csvData.maxValue!,
          version: 1,
        },
        'Pa',
        [['MPa', 2]],
      )

      break
    case 'force':
      unit = createMathUnit(
        'Force',
        {
          ...commonJSON,
          max: csvData.maxValue!,
          version: 1,
        },
        'N',
        [
          ['N', 0],
          ['kN', 0],
          ['lbs', 0],
        ],
        {
          checkValidity: (value) => value >= 0,
        },
      )

      break
    case 'temperature':
      unit = createMathUnit(
        'Temperature',
        {
          ...commonJSON,
          max: csvData.maxValue!,
          version: 1,
        },
        '°C',
        [
          ['°C', 0],
          ['°F', 0],
          ['K', 0],
        ],
      )

      break
    case 'distance':
      unit = createMathUnit(
        'Distance',
        {
          ...commonJSON,
          max: csvData.maxValue!,
          version: 1,
        },
        'm',
        [
          ['m', 0],
          ['km', 0],
          ['mi', 0],
        ],
      )

      break
    case 'time':
      unit = createMathUnit(
        'Time',
        {
          ...commonJSON,
          max: csvData.maxValue!,
          version: 1,
        },
        's',
        [
          ['s', 0],
          ['ms', 0],
          ['us', 0],
        ],
        {
          step: 0.1,
          checkValidity: (value) => value >= 0,
        },
      )

      break
    case 'cumsum':
      unit = createMathUnit(
        'CumSum',
        {
          ...commonJSON,
          max: csvData.maxValue!,
          version: 1,
        },
        '',
        [['', 1]],
        {
          step: 0.1,
          readOnly: true,
        },
      )

      break
    case 'radius':
      unit = createMathUnit(
        'Radius',
        {
          ...commonJSON,
          max: csvData.maxValue!,
          version: 1,
        },
        'm',
        [
          ['m', 0],
          ['km', 0],
          ['mi', 0],
        ],
        {
          checkValidity: (value) => value >= 0,
        },
      )

      break
    case 'position':
      unit = createBaseMathUnit(
        'Position',
        {
          ...commonJSON,
          version: 1,
        },
        'm',
        [
          ['mm', 0],
          ['cm', 0],
        ],
      )

      break
    case 'sensorCoefficient':
      unit = createBaseMathUnit(
        'SensorCoefficient',
        {
          ...commonJSON,
          version: 1,
        },
        '',
        [['', 0]],
        {
          possiblePrecisions: [0, 1, 2, 3],
          checkValidity: (value) => value >= 0,
        },
      )

      break
  }

  return unit
}

describe('Tests mathUnits', async () => {
  const mathUnitTestData = (
    await loadCSV<MathUnitCSVData>(
      `${__dirname}/test_mathunits.csv`,
      mathUnitsCSVHeaders,
    )
  ).map((data) => ({ ...data, value: parseFloat(data.value) }))

  test.each(mathUnitTestData)('test ValueToLocaleString: %s', (csvData) => {
    let result = ''

    const unit = getUnit(csvData)
    result = unit.valueToLocaleString(csvData.value, { ...csvData })

    expect(result).toBe(csvData.expectedStringResult)
  })

  test.each(mathUnitTestData.filter((mathUnitTest) => mathUnitTest.capValue))(
    'test CapValue: %s',
    (csvData) => {
      let result = ''

      const unit = getUnit(csvData)
      const parsedValue = csvData.value
      result = 'capValue' in unit ? unit.capValue(parsedValue).toString() : ''

      expect(result).toBe(csvData.capValue)
    },
  )

  test.each(mathUnitTestData)('test GetAverage: %s', (csvData) => {
    let result = ''

    const unit = getUnit(csvData)
    const parsedValue = csvData.value
    result = unit.getAverage([parsedValue, parsedValue]).toString()

    expect(result).toBe(csvData.getAverage)
  })

  test.each(mathUnitTestData)('test CurrentToBase: %s', (csvData) => {
    let result = ''

    const unit = getUnit(csvData)
    const parsedValue = csvData.value
    result = unit.currentToBase(parsedValue).toString()

    expect(result).toBe(csvData.currentToBase)
  })

  test.each(mathUnitTestData)('test BaseToCurrent: %s', (csvData) => {
    let result = ''

    const unit = getUnit(csvData)
    const parsedValue = csvData.value
    result = unit.baseToCurrent(parsedValue).toString()

    expect(result).toBe(csvData.baseToCurrent)
  })

  test.each(mathUnitTestData)('test CheckValidity: %s', (csvData) => {
    let result = ''

    const unit = getUnit(csvData)
    const parsedValue = csvData.value
    result = unit.checkValidity(parsedValue).toString()

    expect(result).toBe(csvData.checkValidity)
  })
})
