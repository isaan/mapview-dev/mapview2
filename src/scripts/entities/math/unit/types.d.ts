// ---
// JSON
// ---

type JSONMathUnitVAny<PossibleUnits> =
  | JSONMathUnit<PossibleUnits>
  | JSONMathUnitV1<PossibleUnits>

interface JSONMathUnit<PossibleUnits> extends JSONBaseMathUnit<PossibleUnits> {
  readonly base: JSONBaseMathUnit
  readonly min: number
  readonly max: number
}

interface JSONMathUnitV1<PossibleUnits>
  extends JSONBaseMathUnitV1<PossibleUnits> {
  readonly max: number
  readonly min?: number
}

// ---
// Object
// ---

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyUnit = any

interface MathUnit<PossibleUnits> extends BaseMathUnit<PossibleUnits> {
  readonly min: ASS<number>
  readonly max: ASS<number>
  readonly capValue: (value: number) => number
  readonly getAverage: (values: number[]) => number
  readonly valueToLocaleString: (
    value: number,
    options?: MathUnitGetLocaleStringOptions,
  ) => string
  readonly toJSON: () => JSONMathUnit<PossibleUnits>
}

type UnitName =
  | 'Deflection'
  | 'Force'
  | 'Modulus'
  | 'Temperature'
  | 'Distance'
  | 'Time'
  | 'CumSum'
  | 'Stiffness'
  | 'Percentage'
  | 'Radius'
  | 'Position'
  | 'SensorCoefficient'

type PossibleFormats = 'sci' | 'dec'

interface MathUnitGetLocaleStringOptions {
  readonly appendUnitToString?: true
  readonly locale?: string
  readonly precision?: number
  readonly disablePreString?: true
  readonly unit?: string
  readonly removeSpaces?: true
  readonly disableMinAndMax?: true
  readonly format?: PossibleFormats
}

type AverageFunction = 'allEqual' | 'capOutliers' | 'ignoreOutliers'
