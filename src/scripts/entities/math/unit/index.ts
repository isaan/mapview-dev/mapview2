import { createUnit, unit as Unit } from 'mathjs'

import { numberToLocaleString } from '/src/locales'
import {
  createASS,
  createBaseMathUnit,
  defaultInvalidValueReplacement,
  defaultKo,
} from '/src/scripts'

export enum ConvertType {
  BaseToCurrent = 'BaseToCurrent',
  CurrentToBase = 'CurrentToBase',
}

createUnit({
  cmm: '10 um',
  dmm: '100 um',
  nlbs: '4.448221628250858 N',
})

function convertMapviewUnitToMathJSUnit(unit: undefined): undefined
function convertMapviewUnitToMathJSUnit(unit: string): string
function convertMapviewUnitToMathJSUnit(unit: AnyUnit): AnyUnit {
  switch (unit) {
    case '°C':
      return 'degC'
    case '°F':
      return 'degF'
    case '1/100 mm':
      return 'cmm'
    case '1/10 mm':
      return 'dmm'
    case 'lbs':
      return 'nlbs'
    case '%':
      return 'm'
    default:
      return unit
  }
}

const getValueForAverage = (
  min: number,
  max: number,
  value: number,
  averageFunction?: AverageFunction,
) => {
  if (averageFunction === 'capOutliers') {
    if (max && value > max) {
      return max
    }
    if (value < min) {
      return min
    }
  }
  return value
}

export const createMathUnit = <PossibleUnits extends string>(
  name: UnitName,
  json: JSONMathUnitVAny<PossibleUnits>,
  baseUnit: string,
  possibleSettings: [PossibleUnits, number][],
  options?: {
    possiblePrecisions?: number[]
    step?: number
    averageFunction?: AverageFunction
    readOnly?: true
    invalidReplacement?: string
    checkValidity?: (value: number) => boolean
  },
): MathUnit<PossibleUnits> => {
  json = upgradeJSON(json)

  const baseMathUnit = createBaseMathUnit(
    name,
    json,
    baseUnit,
    possibleSettings,
    options,
  )

  json = {
    ...json,
    base: baseMathUnit.toBaseJSON(),
  }

  const invalidReplacement =
    options?.invalidReplacement ?? defaultInvalidValueReplacement

  const max = createASS(json.max)
  const min = createASS(json.min)
  const format = createASS(json.format)

  const mathUnits: MathUnit<PossibleUnits> = {
    ...baseMathUnit,
    baseUnit,
    possibleSettings,
    min,
    max,
    capValue(value: number) {
      return Math.max(min(), Math.min(max(), value))
    },
    getAverage(values) {
      const filteredValues: number[] = values.filter(
        (value) =>
          this.checkValidity(value) &&
          (options?.averageFunction === 'ignoreOutliers'
            ? value <= max() && value >= min()
            : true),
      )

      return filteredValues.length > 0
        ? filteredValues.reduce(
            (total, currentValue) =>
              total +
              getValueForAverage(
                min(),
                max(),
                currentValue,
                options?.averageFunction,
              ),
            0,
          ) / filteredValues.length
        : 0
    },
    valueToLocaleString(value, parseOptions = {}) {
      if (Number.isNaN(value)) return defaultKo

      let valueString

      if (this.checkValidity(value)) {
        const numberToLocaleOptions = {
          locale: parseOptions.locale,
          precision: parseOptions.precision,
        }

        let preString = ''

        numberToLocaleOptions.precision ??= this.currentPrecision()

        if (!parseOptions.disableMinAndMax) {
          if (value < min()) {
            value = min()
            preString = '<'
          } else if (max && value > max()) {
            value = max()
            preString = '>'
          }
        }

        value = convertValueFromUnitAToUnitB(
          value,
          this.baseUnit,
          parseOptions.unit ?? this.currentUnit(),
        )

        valueString = `${
          parseOptions.disablePreString ? '' : preString
        } ${numberToLocaleString(value, numberToLocaleOptions)}`.trim()
      } else {
        valueString = invalidReplacement
      }

      const localeString: string = `${valueString.replaceAll('\u202f', ' ')} ${
        parseOptions.appendUnitToString ? this.currentUnit() : ''
      }`.trim()

      return parseOptions.removeSpaces
        ? localeString.replaceAll(' ', '')
        : localeString
    },
    toJSON(): JSONMathUnit<PossibleUnits> {
      return {
        version: 2,
        base: this.toBaseJSON(),
        currentUnit: this.currentUnit(),
        currentPrecision: this.currentPrecision(),
        max: max(),
        min: min(),
        format: format(),
      }
    },
  }

  return mathUnits
}

export const convertValueFromUnitAToUnitB = (
  value: number,
  unitA: string,
  unitB: string,
) =>
  unitA !== unitB && !Number.isNaN(value)
    ? Unit(value, convertMapviewUnitToMathJSUnit(unitA)).toNumber(
        convertMapviewUnitToMathJSUnit(unitB),
      )
    : value

const upgradeJSON = <PossibleUnits extends string>(
  json: JSONMathUnitVAny<PossibleUnits>,
): JSONMathUnit<PossibleUnits> => {
  switch (json.version) {
    case 1: {
      const jsonV2: JSONMathUnit<PossibleUnits> = {
        ...json,
        version: 2,
        base: undefined,
        min: json.min ?? 0,
        format: 'dec',
      }
      json = jsonV2
    }
  }

  return json
}

export * from './base'
export * from './constants'
