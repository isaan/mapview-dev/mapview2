interface HeavydynUnitsSkeleton<
  A,
  B = A,
  C = A,
  D = A,
  E = A,
  F = A,
  G = A,
  H = A,
  I = A,
  J = A,
> {
  readonly deflection: A
  readonly force: B
  readonly temperature: C
  readonly distance: D
  readonly time: E
  readonly modulus: F
  readonly cumSum: G
  readonly radius: H
  readonly position: I
  readonly sensorCoefficient: J
}

interface HeavydynUnitsSkeletonV2<
  A,
  B = A,
  C = A,
  D = A,
  E = A,
  F = A,
  G = A,
  H = A,
> {
  readonly deflection: A
  readonly force: B
  readonly temperature: C
  readonly distance: D
  readonly time: E
  readonly modulus: F
  readonly cumSum: G
  readonly radius: H
}

interface HeavydynUnitsSkeletonV1<A, B = A, C = A, D = A, E = A, F = A, G = A> {
  readonly deflection: A
  readonly force: B
  readonly temperature: C
  readonly distance: D
  readonly time: E
  readonly modulus: F
  readonly cumSum: G
}

type HeavydynUnitsNames = keyof HeavydynUnitsSkeletonV2<undefined>

type PossibleHeavydynDeflectionUnits = 'mm' | '1/100 mm' | 'um'
type PossibleHeavydynForceUnits = 'N' | 'kN' | 'lbs'
type PossibleHeavydynTemperatureUnits = '°C' | '°F' | 'K'
type PossibleHeavydynDistanceUnits = 'm' | 'km' | 'mi'
type PossibleHeavydynTimeUnits = 's' | 'ms' | 'us'
type PossibleHeavydynModulusUnits = 'MPa'
type PossibleHeavydynRadiusUnits = 'm' | 'km' | 'mi'
type PossibleHeavydynPositionUnits = 'mm' | 'cm' | 'm'

type JSONHeavydynUnitsVAny =
  | JSONHeavydynUnits
  | JSONHeavydynUnitsV2
  | JSONHeavydynUnitsV1

type JSONHeavydynUnits = { readonly version: 3 } & HeavydynUnitsSkeleton<
  JSONMathUnitVAny<PossibleHeavydynDeflectionUnits>,
  JSONMathUnitVAny<PossibleHeavydynForceUnits>,
  JSONMathUnitVAny<PossibleHeavydynTemperatureUnits>,
  JSONMathUnitVAny<PossibleHeavydynDistanceUnits>,
  JSONMathUnitVAny<PossibleHeavydynTimeUnits>,
  JSONMathUnitVAny<PossibleHeavydynModulusUnits>,
  JSONMathUnitVAny<''>,
  JSONMathUnitVAny<PossibleHeavydynRadiusUnits>,
  JSONBaseMathUnitVAny<PossibleHeavydynPositionUnits>,
  JSONBaseMathUnitVAny<''>
>

type JSONHeavydynUnitsV2 = { readonly version: 2 } & HeavydynUnitsSkeletonV2<
  JSONMathUnitVAny<PossibleHeavydynDeflectionUnits>,
  JSONMathUnitVAny<PossibleHeavydynForceUnits>,
  JSONMathUnitVAny<PossibleHeavydynTemperatureUnits>,
  JSONMathUnitVAny<PossibleHeavydynDistanceUnits>,
  JSONMathUnitVAny<PossibleHeavydynTimeUnits>,
  JSONMathUnitVAny<PossibleHeavydynModulusUnits>,
  JSONMathUnitVAny<''>,
  JSONMathUnitVAny<PossibleHeavydynRadiusUnits>
>

type JSONHeavydynUnitsV1 = { readonly version: 1 } & HeavydynUnitsSkeletonV1<
  JSONMathUnitVAny<PossibleHeavydynDeflectionUnits>,
  JSONMathUnitVAny<PossibleHeavydynForceUnits>,
  JSONMathUnitVAny<PossibleHeavydynTemperatureUnits>,
  JSONMathUnitVAny<PossibleHeavydynDistanceUnits>,
  JSONMathUnitVAny<PossibleHeavydynTimeUnits>,
  JSONMathUnitVAny<PossibleHeavydynModulusUnits>,
  JSONMathUnitVAny<''>
>

type HeavydynUnits = MathUnit<string> | BaseMathUnit<string>

type HeavydynMathUnits = HeavydynUnitsSkeleton<
  MathUnit<PossibleHeavydynDeflectionUnits>,
  MathUnit<PossibleHeavydynForceUnits>,
  MathUnit<PossibleHeavydynTemperatureUnits>,
  MathUnit<PossibleHeavydynDistanceUnits>,
  MathUnit<PossibleHeavydynTimeUnits>,
  MathUnit<PossibleHeavydynModulusUnits>,
  MathUnit<''>,
  MathUnit<PossibleHeavydynRadiusUnits>,
  BaseMathUnit<PossibleHeavydynPositionUnits>,
  BaseMathUnit<''>
> & { list: HeavydynUnits[] } & SerializableObject<JSONHeavydynUnits>
