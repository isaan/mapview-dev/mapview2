export const cleanString = (s: string) => {
  try {
    // eslint-disable-next-line sonarjs/deprecation
    return unaccent(decodeURIComponent(escape(s)))
  } catch {
    return unaccent(s)
  }
}

export const unaccent = (s: string) =>
  s.normalize('NFD').replace(/\p{Diacritic}/gu, '')
