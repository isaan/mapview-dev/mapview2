import { lineString, nearestPointOnLine, point as turfPoint } from '@turf/turf'
import type { Position } from 'geojson'

import { createDataValue } from '/src/scripts/entities/data/value'
import { convertValueFromUnitAToUnitB } from '/src/scripts/entities/math/unit'
import { store } from '/src/store'

export const refPathEventClick = (e: mapboxgl.MapMouseEvent) => {
  const report = store.selectedReport()

  const referencePath = report?.referencePath()
  referencePath?.addPoint(e.lngLat)
  referencePath?.settings.isVisible.set(true)

  if (report && referencePath!.points().length > 1) {
    recalculeChainage(referencePath!, report)
  }
}

export const recalculeChainage = (
  referencePath: ReferencePath,
  report: BaseReport,
) => {
  if (referencePath.points().length < 2) return
  const linePoints: Position[] = []
  referencePath.points().forEach((point) => {
    if ((point.coordinates()?.lng, point.coordinates()?.lat))
      linePoints.push([
        point.coordinates()?.lng as number,
        point.coordinates()?.lat as number,
      ])
  })

  const l = lineString(linePoints)
  const chainageDL = report.dataLabels.findIn('Point', 'Chainage')

  if (chainageDL) {
    report.sortedPoints().forEach((point: BasePoint) => {
      const pt = turfPoint([
        point.coordinates()?.lng as number,
        point.coordinates()?.lat as number,
      ])
      const distanceUnit = store.selectedProject()?.units.distance
      const dist = nearestPointOnLine(l, pt, {
        units: 'kilometers',
      }).properties.location

      if (dist && distanceUnit) {
        const chainageValue = convertValueFromUnitAToUnitB(
          dist,
          'km',
          distanceUnit?.currentUnit() || 'm',
        )

        point.dataset.set(
          chainageDL,
          createDataValue(chainageValue + referencePath.origin(), chainageDL),
        )
      }
    })
  }
}

export const resetReferencePath = () => {
  const map = store.map()
  const refPath = store.selectedReport()?.referencePath
  if (refPath && map) {
    store.selectedReport()?.settings.isRefpathEditing.set(false)
    map.off('click', refPathEventClick)
    map.getCanvas().style.cursor = 'grab'
  }
}
