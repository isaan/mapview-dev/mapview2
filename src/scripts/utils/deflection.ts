function sortDeflection(source: DataValue<string>[]): DataValue<string>[]
function sortDeflection(source: JSONDataValue[]): JSONDataValue[]
function sortDeflection(
  source: JSONDataValue[] | DataValue<string>[],
): DataValue<string>[] | JSONDataValue[] {
  return source.sort((a, b) => {
    return (
      Number(
        (typeof a.label === 'string' ? a.label : a.label.name).replaceAll(
          /[a-zA-Z]+/gm,
          '',
        ),
      ) -
      Number(
        (typeof b.label === 'string' ? b.label : b.label.name).replaceAll(
          /[a-zA-Z]+/gm,
          '',
        ),
      )
    )
  })
}

export { sortDeflection }
