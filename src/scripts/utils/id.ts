export const getUniqueId = (): string => {
  const buffer = new Uint32Array(2)
  crypto.getRandomValues(buffer)

  // Convert to BigInt
  // eslint-disable-next-line no-bitwise
  const randomBigInt = (BigInt(buffer[0]) << 32n) | BigInt(buffer[1])

  // Convert to hexadecimal and ensure 16 characters (64 bits)
  return randomBigInt.toString(16).padStart(16, '0')
}
