import { convertData64ImageToUint8Array } from '/src/scripts'

export const addScreenshotsToZip = async (
  zip: Zippable,
  entity: MachineProject | MachineReport,
) => {
  const screenshotsConverted: { [key: string]: Uint8Array } = {}

  let screenshotsData = []

  switch (entity.kind) {
    case 'Project':
      screenshotsData = entity.reports
        .list()
        .map((report, reportIndex) =>
          report.album().map((screenshot) => {
            return {
              reportIndex,
              screenshot,
              pathPrefix: `${report.id.toString()}/`,
            }
          }),
        )
        .flat()
      break
    case 'Report':
      screenshotsData = entity.album().map((screenshot) => {
        return {
          reportIndex: entity.project().reports.selectedIndex(),
          screenshot,
          pathPrefix: '',
        }
      })
      break
  }

  await Promise.all(
    screenshotsData.map(async (obj) => {
      screenshotsConverted[
        `${obj.pathPrefix}${obj.screenshot.name}.${obj.screenshot.extension}`
      ] = await convertData64ImageToUint8Array(obj.screenshot.file)
    }),
  )

  zip.screenshots = screenshotsConverted
}
