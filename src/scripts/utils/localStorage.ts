export const localStorageSetItem = (id?: string, value?: string) => {
  if (id) {
    if (value) localStorage.setItem(id, value)
    else localStorage.removeItem(id)
  }
}
