export enum ErrorCode {
  GENERIC = 'Generic',
  NO_POINT = 'No point',
  MISSING_DATA = 'Missing data',
  UNMANAGED_EXTENSION = 'Unmanaged extension',
  EMPTY_FILE = 'Empty file',
}
