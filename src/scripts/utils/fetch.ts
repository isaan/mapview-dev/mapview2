export const fetchFileFromURL = async (url: string, fileName?: string) => {
  const response = await fetch(url)

  const blob = await response.blob()

  return new File([blob], fileName ?? String(url.split('/').pop()))
}
