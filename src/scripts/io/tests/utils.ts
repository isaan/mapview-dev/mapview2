/* eslint-disable no-await-in-loop */
import { readdirSync, statSync } from 'fs'

import translationEN from '/src/locales/en.json?raw'
import {
  getAllPointsFromProject,
  getExtensionFromFile,
  importFile,
  removeLeading0s,
  sleep,
  unzipFile,
} from '/src/scripts'
import { store } from '/src/store'
import { getFilesFromDir } from '/src/tests/utils/files'

export const PREVIOUS_KEY = 'previousVersion'

export const getFilesForErrorTest = (path: string) => {
  const filesData = getFilesFromDir(path, {
    recursive: true,
    authorizedExtension: ['prjz', 'dynz'],
  })

  const errors = Object.keys(JSON.parse(translationEN).Error)

  return filesData.map((fileData) => {
    const errorCode = errors.find((error) => fileData.path.includes(error))

    return {
      file: fileData.file,
      name: fileData.file.name,
      errorCode,
    }
  })
}

export const getTestDataFromDir = async (path: string) => {
  const files = readdirSync(path)

  const testData: ReportTestData[] = []

  await Promise.all([
    ...files.map(async (file) => {
      const subPath = `${path}/${file}`
      const stats = statSync(subPath)

      if (stats.isDirectory()) {
        testData.push(...(await loadFiles(subPath)))
      }
    }),
  ])

  return testData
}

const acceptedExtension = [
  'pdx',
  'F25',
  'fwd',
  'mpvz',
  'mvrz',
  'prjz',
  'dynz',
  'txt',
]

export const loadFiles = async (dirPath: string) => {
  const filesData = getFilesFromDir(dirPath, {
    recursive: true,
    authorizedExtension: acceptedExtension,
  })

  const filesGrouped: { dir: string; files: File[]; hasPrevious: boolean }[] =
    []

  filesData.forEach((fileData) => {
    const pathPart = fileData.path.split('/')

    let dir = '/'

    if (pathPart.length >= 2) {
      dir = pathPart.slice(0, pathPart.length - 1).join('/')
      dir = dir.substring(dir.indexOf('/src'))
    }

    let actualGroup = filesGrouped.find((data) => data.dir === dir)

    if (!actualGroup) {
      actualGroup = { dir, files: [fileData.file], hasPrevious: false }
      filesGrouped.push(actualGroup)
    } else {
      actualGroup.files.push(fileData.file)
      if (fileData.path.toUpperCase().includes(PREVIOUS_KEY.toUpperCase())) {
        actualGroup.hasPrevious = true
      }
    }
  })

  const result: ReportTestData[] = []
  const promises: Promise<void>[] = []

  filesGrouped.forEach((fileGroup) => {
    const currentGroup: Partial<ReportTestData> = {
      directoryName: `${fileGroup.dir}/`,
      hasPrevious: fileGroup.hasPrevious,
    }

    fileGroup.files.forEach((file) => {
      const extension = getExtensionFromFile(file)

      switch (extension) {
        case 'prjz':
        case 'dynz':
        case 'mpvz': {
          if (
            (fileGroup.hasPrevious &&
              file.name.toUpperCase().includes(PREVIOUS_KEY.toUpperCase())) ||
            !fileGroup.hasPrevious
          ) {
            promises.push(
              (async () => {
                const project = await importFile(file)

                if (project) {
                  store.pushAndSelectProject(project)
                  await loadProjectData(file, project)

                  currentGroup[extension === 'mpvz' ? 'mpvz' : 'prjz'] = {
                    file,
                    project,
                  }
                }
              })(),
            )
          } else {
            currentGroup.currentMPVZ = file
          }

          break
        }
        case 'fwd':
          {
            const isSweco = file.name.toLowerCase().includes('sweco')

            if (isSweco) {
              currentGroup.fwdSweco = file
            } else {
              currentGroup.fwdDynatest = file
            }
          }
          break
        case 'F25':
          currentGroup.f25 = file
          break
        case 'pdx':
          currentGroup.pdx = file
          break
        case 'mvrz':
          currentGroup.mvrz = file
          break
        case 'txt': {
          if (file.name.toLowerCase().includes('rawData')) {
            currentGroup.txtRawData = file
          } else {
            currentGroup.txt = file
          }
        }
      }
    })

    result.push(currentGroup as ReportTestData)
  })

  await Promise.all(promises)

  return result
}

export const loadProjectData = async (
  mpvzFile: File,
  project: MachineProject,
) => {
  project.addToMap()
  // eslint-disable-next-line no-async-promise-executor
  return new Promise<void>(async (resolve) => {
    const unzipped = await unzipFile(mpvzFile)

    const raws = Object.keys(unzipped).filter((key) => /rawdata\/\w+/.exec(key))
    const overlays = Object.keys(unzipped).filter((key) =>
      /overlays\/\w+/.exec(key),
    )

    if (raws.length < 1) {
      resolve()
      return
    }

    let needInit = true

    // rawdata and screenshoot are load in async so we wetting for the data to be init
    while (needInit) {
      const points = getAllPointsFromProject(project)

      const status: boolean[] = []

      raws.forEach((key) => {
        // eslint-disable-next-line sonarjs/single-character-alternation
        const id = key.split(/(\/|\\)/)[2]
        const point = points.find(
          (p) => removeLeading0s(p.id) === removeLeading0s(id),
        )

        status.push(point?.rawDataFile()?.byteLength === undefined)
      })

      needInit =
        (status.length > 0 && status.every((value) => value)) ||
        project.state() === 'Loading' ||
        overlays.length !== project.overlays().length

      await sleep(250)
    }

    resolve()
  })
}
