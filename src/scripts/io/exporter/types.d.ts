interface Exporter {
  name: string
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  export: (...args: any[]) => Promise<File>
}

type AnyExporter =
  | MachineExporter
  | HeavydynExporter
  | MaxidynExporter
  | MinidynExporter

interface MachineExporter extends Exporter {
  readonly export: (project: MachineProject, _?: boolean) => Promise<File>
}

interface HeavydynExporter extends Exporter {
  readonly export: (project: HeavydynProject, exportRawData?: boolean) => File
}

interface MaxidynExporter extends Exporter {
  readonly export: (project: MaxidynProject, _?: boolean) => Promise<File>
}

interface MinidynExporter extends Exporter {
  readonly export: (project: MinidynProject, _?: boolean) => Promise<File>
}
