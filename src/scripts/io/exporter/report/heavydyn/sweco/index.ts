import dedent from 'dedent'

import {
  currentCategory,
  findFieldInArray,
  formatToUTCDate,
  replaceAllLFToCRLF,
} from '/src/scripts'
import { sortDeflection } from '/src/scripts/utils/deflection'

import { ddToDms } from './coordinates'

export const heavydynSwecoExporter: HeavydynExporter = {
  name: '.fwd (Sweco)',
  export: (project: HeavydynProject) => {
    return new File(
      [
        replaceAllLFToCRLF(
          '\n' + writeHeader(project) + writePoints(project) + '\n',
        ),
      ],
      `${project.reports.selected()?.name.toString() ?? ''}-sweco.fwd`,
      { type: 'text/plain' },
    )
  },
}

const writeSeparator = (): string => {
  return ''.padEnd(130, '_')
}

const padDotString = (str: string, length: number, tab = true): string => {
  const c = tab ? '\t' : ''
  return str.padEnd(length, '.') + c
}

const writeHeader = (project: HeavydynProject): string => {
  const selectedReport = project.reports.selected()
  if (!selectedReport) {
    throw new Error('cannot find selected report ')
  }

  const date = formatToUTCDate(
    findFieldInArray(selectedReport.information, 'Date')?.toString() ?? '',
    project.database().timeZoneOffsetMinutes,
    'DD/MM/YYYY',
  )

  const fwdNumber = findFieldInArray(
    project.hardware,
    'Serial number',
  )?.getValue()

  const rPlate = Math.round(project.calibrations.dPlate * 1000) / 2

  const sensors = project.calibrations.channels.slice(1)

  const lane = findFieldInArray(selectedReport.information, 'Lane')?.getValue()

  const client = findFieldInArray(project.information, 'Client')?.getValue()

  const roadReference = findFieldInArray(
    selectedReport.information,
    'Part',
  )?.getValue()

  return dedent`
    ${writeSeparator()}\n\n
    (c) ROAD SYSTEM 2016, Metric
    ${writeSeparator()}
    $1
    ${padDotString('Filename:', 23) + project.name.toString()}
    ${padDotString('Client Code:', 23)}
    ${padDotString('Road number:', 23) + (lane?.toString() ?? '')}
    ${padDotString('Name of Client:', 23) + String(client?.toString())}
    ${padDotString('Districtnumber:', 23)}
    ${padDotString('Road reference:', 23) + (roadReference?.toString() ?? '')}
    ${padDotString('Start reference:', 23)}
    ${padDotString('Date [dd/mm/yy]:', 23) + date}
    ${padDotString('FWD Number:', 23) + (fwdNumber?.toString() ?? '')}
    ${padDotString('Load plate radius [mm]', 23) + rPlate.toString()}
    ${
      ' '.repeat(23) +
      '\t' +
      sensors.map((_, index) => `R(${index + 1})`).join('\t')
    }
    ${
      padDotString('Radial offset [cm]', 23) +
      sensors
        .map((channel) => Math.round(parseFloat(channel.position) * 100))
        .join('\t')
    }
    ${padDotString('Tolerance [%]', 23) + sensors.map(() => '5').join('\t')}
    ${padDotString('Correction [%]', 23) + sensors.map(() => '0').join('\t')}
    ${padDotString('Filter OFF:', 23)} - Hz
    \n\n
  `
}

const writePoints = (project: HeavydynProject): string => {
  if (!project.reports.selected) return ''

  return (
    project.reports
      .selected()
      ?.exportablePoints()
      .map((point) => {
        let coordinates = { lng: '', lat: '' }

        const chainage = Math.round(
          Array.from<DataValue<string>>(point.dataset.values())
            .find((data) => data.label.name === 'Chainage')
            ?.rawValue() ?? 0,
        )

        coordinates = ddToDms(point.toBaseJSON().coordinates as mapboxgl.LngLat)

        const dropPosition = [
          'Position of Drop:',
          'Longitude: ' + coordinates.lng,
          'Latitude: ' + coordinates.lat,
          'Altitude: 0.0 m',
        ]

        return dedent`
          \n${writeSeparator()}
          $2
          ${padDotString('Chainage [m]', 23) + chainage.toString()}
          ${padDotString('Lane', 23)}
          ${padDotString('Pavement description', 23)}
          ${padDotString('Remarks', 23)}
          ${dropPosition.join('\t')}
          ${writeDrops(point, project.calibrations.channels)}
        `
      })
      .join('') ?? ''
  )
}

const writeDrops = (point: BasePoint, channels: JSONChannel[]): string => {
  const pointInfos = [
    'Sequence: 1/1',
    'No. of drops: ' + point.drops.length.toString(),
    'Fallheight: 0',
    'Time: ' +
      formatToUTCDate(
        point.date.toString(),
        point.zone().report().project().database().timeZoneOffsetMinutes,
        'HH:mm',
      ),
  ]
  const dropHeader = [
    'Drop',
    ...sortDeflection(
      Array.from<DataValue<string>>(point.drops[0].dataset.values()).filter(
        (data) =>
          data.label.unit ===
            point.zone().report().project().units.deflection &&
          data.label.category.name === currentCategory.name,
      ),
    ).map((_, index) => `D(${index + 1})`),
    'kPa',
    'kN',
    'Air',
    'Sur.',
    'Man.',
    'Pulse time',
  ]

  return dedent`
    ${writeSeparator()}
    $3
    ${pointInfos.join('\t')}

    ${dropHeader.join('\t')}
    ${point.drops.map((drop) => writeDrop(drop, channels)).join('\n')}
  `
}

export const writeDrop = (
  drop: MachineDrop,
  channels: JSONChannel[],
): string => {
  let str = ''

  let loadMax = Array.from<DataValue<string>>(drop.dataset.values())
    .find(
      (data) =>
        data.label.unit === drop.point.zone().report().project().units.force &&
        data.label.category.name === currentCategory.name,
    )
    ?.value.getValueAs('kN')

  if (!loadMax || loadMax < 0 || Number.isNaN(loadMax)) {
    loadMax = 0.1
  }

  const maxValues = [
    drop.index.displayedIndex,
    ...sortDeflection(
      Array.from<DataValue<string>>(drop.dataset.values()).filter(
        (data) =>
          data.label.unit ===
            drop.point.zone().report().project().units.deflection &&
          data.label.category.name === currentCategory.name,
      ),
    ).map((_drop) => {
      const def = _drop.value.getValueAs('um')

      if (def < 0 || Number.isNaN(def)) return 0.1

      return def.toFixed(1)
    }),
    0,
    loadMax.toFixed(1),
    ...Array.from<DataValue<string>>(drop.point.dataset.values())
      .slice(0, -1)
      .map((data) =>
        data.value.getLocaleString({ precision: 1, locale: 'en-US' }),
      ),
    (
      Array.from<DataValue<string>>(drop.dataset.values())
        .find(
          (data) =>
            data.label.unit === drop.point.zone().report().project().units.time,
        )
        ?.value.getValueAs('ms') ?? 0
    ).toFixed(2),
  ]

  str += `\n${maxValues.join('\t')}`

  const impactData = drop.impactData()

  if (impactData) {
    const loadPosition = Math.round(parseFloat(channels[0].position) * 100)

    const loadInfos = [
      padDotString(`LoadCell (${loadPosition})[kN]`, 23, false),
      '-',
      loadMax,
      ...impactData.load.map((value) => (value / 1000).toFixed(1)),
    ]

    str += `
      ${loadInfos.join('\t')}
      ${writeDisplacements(drop, channels)}
    `
  }

  return dedent`${str}`
}

const writeDisplacements = (
  drop: MachineDrop,
  channels: JSONChannel[],
): string => {
  const impactData = drop.impactData()

  if (!impactData) {
    throw new Error('No impact data found')
  }

  return impactData.displacement
    .map((displacement, index) => {
      const sensorName = 'Sensor' + (index + 1).toString().padStart(2, ' ')

      const sensorPosition = `(${Math.round(
        parseFloat(channels[index + 1].position) * 1000,
      )
        .toString()
        .padStart(4, ' ')})`

      const sensorData = [
        sensorName + sensorPosition + '[MPa;µm].',
        1.0,
        Array.from<DataValue<string>>(drop.dataset.values())
          [index + 2].value.getValueAs('um')
          .toFixed(1),
        ...displacement.map((val) =>
          (val * 1000000).toFixed(1).replace('-0.0', '0.0'),
        ),
      ]
      return sensorData.join('\t')
    })
    .join('\n')
}
