import dedent from 'dedent'

import { translate } from '/src/locales'
import {
  findFieldInArray,
  formatToUTCDate,
  replaceAllLFToCRLF,
} from '/src/scripts'

const localFR = 'fr-FR'

const localStringOptions: {
  locale: string
  removeSpaces: true
} = {
  locale: localFR,
  removeSpaces: true,
}

const forceUnitName = 'N'
const deflectionUnitName = 'm'

export const heavydynLegacyTXTExporter: HeavydynExporter = {
  name: '.txt',
  export: (project: HeavydynProject, exportRawData?: boolean) => {
    const selectedReport = project.reports.selected()

    return new File(
      [
        replaceAllLFToCRLF(dedent`
            ${writeTableDatabase(project)}
            ${writeTableDossier(project)}
            ${writeTablePVs(selectedReport, project)}
            ${writeTableSequences(selectedReport)}
            ${writeTablePlateformes(selectedReport, project.units.deflection)}
            ${writeTableMachines(project)}
            ${writeTableParamsAcqu(project)}
            ${writeTableCalibrations(project, selectedReport)}${writeTableVoies(project)}${writeTableSensors(project)}
            ${writeTableResultatsPV(selectedReport, project.units.deflection)}
            ${writeTableArrayResult(selectedReport, project)}${writeTableReportPointsAndDrops(project, exportRawData)}
          `),
      ],
      `${project.reports.selected()?.name.toString() ?? ''}.txt`,
      { type: 'text/plain' },
    )
  },
}

const formatDate = 'DD/MM/YYYY'
const formatDateWithHour = 'DD/MM/YYYY HH:mm:ss'
const getDate = (project: HeavydynProject, fieldDate?: Field[]) =>
  formatToUTCDate(
    findFieldInArray(fieldDate || [], 'Date')?.toString() ?? '',
    project.database().timeZoneOffsetMinutes,
    formatDate,
  )

const writeTableDatabase = (project: HeavydynProject) => {
  return dedent`
    TABLE${'\t'}Database
    Logiciel${'\t'}Heavydyn
    VersionBDD${'\t'}1
    Locale${'\t'}fr-FR
    TimeZone${'\t'}${project.database().timeZone}
    `
}

const writeTableDossier = (project: HeavydynProject) => {
  const projectDate = getDate(project, project.information)

  return dedent`
    TABLE${'\t'}Dossiers
    Nom${'\t'}${project.name.getValue()}
    Client
    ContactClient
    Dossier
    Localite
    Date${'\t'}${projectDate}
    Ouvrage
    Commentaire
    Smoothing${'\t'}True
    UnitDeplNom${'\t'}${project.units.deflection.currentUnit()}
    UnitDeplRatio${'\t'}${project.units.deflection.currentToBase(1).toExponential(0)}
    UnitForceNom${'\t'}${project.units.force.currentUnit()}
    UnitForceRatio${'\t'}${project.units.force.valueToLocaleString(project.units.force.currentToBase(1), { ...localStringOptions, unit: forceUnitName })}
    `
}

const writeTablePVs = (
  report: HeavydynReport | null,
  project: HeavydynProject,
) => {
  const reportDate = getDate(project, report?.information)

  return dedent`
    TABLE${'\t'}PVs
    Nom${'\t'}${report?.name}
    Date${'\t'}${reportDate}
    Operateur${'\t'}${findFieldInArray(report?.information ?? [], 'Operator')?.toString() ?? ''}
    Voie${'\t'}${findFieldInArray(report?.information ?? [], 'Lane')?.toString() ?? ''}
    PartieOuvrage${'\t'}${findFieldInArray(report?.information ?? [], 'Part')?.toString() ?? ''}
    Sens${'\t'}${findFieldInArray(report?.information ?? [], 'Direction')?.toString() ?? ''}
    Climat${'\t'}${findFieldInArray(report?.information ?? [], 'Climat')?.toString() ?? ''}
    Commentaire${'\t'}${findFieldInArray(report?.information ?? [], 'Comment')?.toString() ?? ''}
    `
}

const writeTableSequences = (report: HeavydynReport | null) => {
  const dropDataLabels = report?.dataLabels.groups.list()[0]

  const sequenceName = dropDataLabels?.sequenceName

  let etapes = dedent``
  dropDataLabels?.indexes.list().forEach((dropIndex) => {
    const type = translate(dropIndex.type)

    const value = dropIndex.value.unit
      ? dropIndex.value.getValueAs(dropIndex.value.unit?.currentUnit())
      : dropIndex.value.value()

    etapes += dedent`\nTABLE${'\t'}Etapes
      Numero${'\t'}${dropIndex.displayedIndex}
      TypeDrop${'\t'}${type}
      ValeurDrop${'\t'}${value.toLocaleString(localFR)}
      `
  })

  return dedent`
    TABLE${'\t'}Sequences
    Nom${'\t'}${sequenceName}${etapes}
    `
}

const writeTablePlateformes = (
  report: HeavydynReport | null,
  deflectionUnit: MathUnit<PossibleHeavydynDeflectionUnits>,
) => {
  const platform = report?.platform

  const thresholdDeflection =
    report?.thresholds.groups.deflection.choices.selected()

  return dedent`
    TABLE${'\t'}Plateformes
    Type${'\t'}${platform?.find((info) => info.label === 'Type')?.toString()}
    Couche${'\t'}${platform?.find((info) => info.label === 'Layer')?.toString()}
    Materiau${'\t'}${platform?.find((info) => info.label === 'Material')?.toString()}
    GTR${'\t'}${platform?.find((info) => info.label === 'GTR')?.toString()}
    Etat${'\t'}${platform?.find((info) => info.label === 'State')?.toString()}
    ClassDeflection${'\t'}${thresholdDeflection?.name}
    SeuilDeflection${'\t'}${deflectionUnit.valueToLocaleString(thresholdDeflection?.value() ?? 0, { ...localStringOptions })}
    `
}

const writeTableMachines = (project: HeavydynProject) => {
  const serialNumber =
    findFieldInArray(project.hardware, 'Serial number')?.toString() ?? ''
  const attribution =
    findFieldInArray(project.hardware, 'Attribution')?.toString() ?? ''
  const macAddress =
    findFieldInArray(project.hardware, 'MAC address')?.toString() ?? ''
  const licStart =
    findFieldInArray(project.hardware, 'License start')?.toString() ?? ''
  const licEnd = findFieldInArray(project.hardware, 'License end')?.toString()
  const certStart =
    findFieldInArray(project.hardware, 'Certificate start')?.toString() ?? ''
  const certEnd =
    findFieldInArray(project.hardware, 'Certificate end')?.toString() ?? ''

  return dedent`
    TABLE${'\t'}Machines
    Serial${'\t'}${serialNumber}
    Attribution${'\t'}${attribution}
    MAC${'\t'}${macAddress}
    LicStart${'\t'}${licStart}
    LicEnd${'\t'}${licEnd}
    CertStart${'\t'}${certStart}
    CertEnd${'\t'}${certEnd}
    `
}

const writeTableParamsAcqu = (project: HeavydynProject) => {
  return dedent`
    TABLE${'\t'}ParamsAcqu
    NbSample${'\t'}${project.acquisitionParameters.nbSamples()}
    FreqAcqu${'\t'}${project.acquisitionParameters.frequency()}
    `
}

const writeTableCalibrations = (
  project: HeavydynProject,
  report: HeavydynReport | null,
) => {
  const calibration = project.calibrations
  const date = formatToUTCDate(
    calibration.date.toString(),
    project.database().timeZoneOffsetMinutes,
    formatDate,
  )

  let positionCapteurs = ''
  let idxD0 = 0
  report
    ?.sortedPoints()
    .at(0)
    ?.drops.at(0)
    ?.impactData()
    ?.displacement.forEach((_, index) => {
      const sensorPosition = Math.round(
        parseFloat(project.calibrations.channels[index + 1].position) * 1000,
      )
        .toString()
        .padStart(4, ' ')

      positionCapteurs += `${'\t'}${sensorPosition},000`

      if (sensorPosition === '0') {
        idxD0 = index
      }
    })

  return dedent`
    TABLE${'\t'}Calibrations
    Infos${'\t'}Default values calibration
    Date${'\t'}${date}
    DPlaque${'\t'}${calibration.dPlate.toLocaleString(localFR)}
    PositionCapteurs${positionCapteurs}
    IndexD0${'\t'}${idxD0.toLocaleString(localFR)}
    `
}

const writeTableVoies = (project: HeavydynProject) => {
  let voies = dedent``
  project.calibrations.channels.forEach((channel) => {
    voies += dedent`\nTABLE${'\t'}Voies
      VoieAcqu${'\t'}${channel.channelNumber ?? ''}
      ChannelAdc${'\t'}${channel.adc ?? ''}
      Distance${'\t'}${Math.round(parseFloat(channel.position) * 1000)}
      Nom${'\t'}${channel.name}
      Type${'\t'}${channel.type}
      Gain${'\t'}${channel.gain.toLocaleString(localFR, { notation: 'scientific' })}
      V0${'\t'}${channel.v0.toLocaleString(localFR)}
    `
  })
  return voies
}

const writeTableSensors = (project: HeavydynProject) => {
  let sensors = dedent``

  project.calibrations.sensors.forEach((sensor) => {
    sensors += dedent`\nTABLE${'\t'}Sensors
        Nom${'\t'}${sensor.name}
        Type${'\t'}${sensor.type}
        Gain${'\t'}${sensor.gain.toLocaleString(localFR)}
        V0${'\t'}${sensor.v0.toLocaleString(localFR)}
        `
  })

  return sensors
}

const writeTableResultatsPV = (
  report: HeavydynReport | null,
  deflectionUnit: MathUnit<PossibleHeavydynDeflectionUnits>,
) => {
  if (report) {
    const group = report.dataLabels.groups.selected()
    const selected = group?.choices.selected()

    if (!group || !selected) return

    const defaultValue =
      report
        .sortedPoints()
        .at(0)
        ?.getMathNumber(
          group.from,
          selected,
          group.from === 'Drop' ? group.indexes.selected() : undefined,
        )
        ?.value() ?? 0

    let nbPoint = 0
    let nbPointOk = 0
    let maxPointOk = defaultValue
    let minPointOk = defaultValue
    let nbPointPass = 0
    let nbPointFail = 0

    let sumPointOk = 0

    const thresholdDeflectionVal = report.thresholds.groups.deflection.choices
      .selected()
      ?.value()

    report.sortedPoints().forEach((point) => {
      nbPoint += 1

      const value = point
        .getMathNumber(
          group.from,
          selected,
          group.from === 'Drop' ? group.indexes.selected() : undefined,
        )
        ?.value()

      if (typeof value === 'number') {
        nbPointOk += 1

        if (value > maxPointOk) maxPointOk = value
        else if (value < minPointOk) minPointOk = value

        sumPointOk += value

        if (thresholdDeflectionVal && value >= thresholdDeflectionVal) {
          nbPointPass += 1
        } else {
          nbPointFail += 1
        }
      }
    })

    const moyPointOk = sumPointOk / nbPointOk
    let stdPointOk = 0

    report.sortedPoints().forEach((point) => {
      const value = point
        .getMathNumber(
          group.from,
          selected,
          group.from === 'Drop' ? group.indexes.selected() : undefined,
        )
        ?.value()

      if (value) {
        stdPointOk += (value - moyPointOk) ** 2
      }
    })

    stdPointOk = Math.sqrt(stdPointOk / nbPointOk)

    return dedent`
    TABLE${'\t'}ResultatsPV
    Point_Nombre${'\t'}${nbPoint}
    PointsOK_Nombre${'\t'}${nbPointOk}
    PointOK_Max${'\t'}${deflectionUnit.valueToLocaleString(maxPointOk, { ...localStringOptions, unit: deflectionUnitName })}
    PointOK_Min${'\t'}${deflectionUnit.valueToLocaleString(minPointOk, { ...localStringOptions, unit: deflectionUnitName })}
    PointsOK_Moyenne${'\t'}${deflectionUnit.valueToLocaleString(moyPointOk, { ...localStringOptions, unit: deflectionUnitName })}
    PointsOK_St${'\t'}${deflectionUnit.valueToLocaleString(stdPointOk, { ...localStringOptions, unit: deflectionUnitName })}
    PointsPass_Nombre${'\t'}${nbPointPass}
    PointsFailNombre${'\t'}${nbPointFail}
    `
  }
}

const findLabelByName = (
  values: MapIterator<DataValue<string>>,
  label: string,
) => {
  return Array.from<DataValue<string>>(values)
    .find((pointData) => pointData.label.name === label)
    ?.rawValue()
}

const writeTableArrayResult = (
  report: HeavydynReport | null,
  project: HeavydynProject,
) => {
  if (report) {
    let latStr = ''
    let longStr = ''
    let tairStr = ''
    let tsurfStr = ''
    let tmanStr = ''

    let dataValue = ''

    const listDataValue = [
      { name: 'ForceMax', dataValue: 'Load', variableLine: '' },
      { name: 'D_0300', dataValue: 'D-300', variableLine: '' },
      { name: 'D_0200', dataValue: 'D-200', variableLine: '' },
      { name: 'D0000', dataValue: 'D0', variableLine: '' },
      { name: 'D0200', dataValue: 'D200', variableLine: '' },
      { name: 'D0300', dataValue: 'D300', variableLine: '' },
      { name: 'D0600', dataValue: 'D600', variableLine: '' },
      { name: 'D0900', dataValue: 'D900', variableLine: '' },
      { name: 'D1200', dataValue: 'D1200', variableLine: '' },
      { name: 'D1500', dataValue: 'D1500', variableLine: '' },
      { name: 'D1800', dataValue: 'D1800', variableLine: '' },
      { name: 'D2100', dataValue: 'D2100', variableLine: '' },
    ]

    report.exportablePoints().forEach((point) => {
      const { lat, lng } = point.toBaseJSON().coordinates as LngLat

      const lastDrop = point.drops.at(point.drops.length - 1)
      const tair = findLabelByName(point.dataset.values(), 'Tair')
      const tsurf = findLabelByName(point.dataset.values(), 'Tsurf')
      const tman = findLabelByName(point.dataset.values(), 'Tman')

      if (
        typeof tair === 'undefined' ||
        typeof tsurf === 'undefined' ||
        typeof tman === 'undefined'
      )
        throw new Error()

      if (lastDrop) {
        listDataValue.forEach((dataValueKey) => {
          const unit =
            dataValueKey.dataValue === 'Load'
              ? project.units.force
              : project.units.deflection

          const valString = unit.valueToLocaleString(
            findLabelByName(
              lastDrop.dataset.values(),
              dataValueKey.dataValue,
            ) ?? 0,
            { ...localStringOptions },
          )
          dataValueKey.variableLine += `${'\t'}${valString !== '--' ? valString : '0,0'}`
        })
      }

      latStr += `${'\t'}${(Math.round(lat * 1000000) / 1000000).toLocaleString(localFR, { roundingPriority: 'morePrecision' })}`
      longStr += `${'\t'}${(Math.round(lng * 1000000) / 1000000).toLocaleString(localFR, { roundingPriority: 'morePrecision' })}`
      tairStr += `${'\t'}${tair.toLocaleString(localFR, { roundingPriority: 'morePrecision' })}`
      tsurfStr += `${'\t'}${tsurf.toLocaleString(localFR)}`
      tmanStr += `${'\t'}${tman.toLocaleString(localFR)}`
    })

    listDataValue.forEach((dataValueKey) => {
      dataValue += `\n${dataValueKey.name}${dataValueKey.variableLine}`
    })

    return dedent`
      TABLE${'\t'}ArrayResult
      Latitude${latStr}
      Longitude${longStr}
      Tair${tairStr}
      Tsurf${tsurfStr}
      Tman${tmanStr}${dataValue}
      `
  }
}

const writeTableReportPointsAndDrops = (
  project: HeavydynProject,
  exportRawData?: boolean,
) => {
  const report = project.reports.selected()

  let exportReportPoint = ''

  report?.exportablePoints().forEach((point) => {
    const { lat, lng } = point.toBaseJSON().coordinates as LngLat

    const pointDate = formatToUTCDate(
      point.date.toString(),
      project.database().timeZoneOffsetMinutes,
      formatDateWithHour,
    )

    const chainage = findLabelByName(
      point.dataset.values(),
      'Chainage',
    )?.toLocaleString(localFR)
    if (typeof chainage === 'undefined') throw new Error()

    const comment =
      findFieldInArray(report.information, 'Comment')?.toString() ?? ''

    const tair = findLabelByName(
      point.dataset.values(),
      'Tair',
    )?.toLocaleString(localFR)
    const tsurf = findLabelByName(
      point.dataset.values(),
      'Tsurf',
    )?.toLocaleString(localFR)
    const tman = findLabelByName(
      point.dataset.values(),
      'Tman',
    )?.toLocaleString(localFR)
    const DOP = findLabelByName(point.dataset.values(), 'DOP')?.toLocaleString(
      localFR,
    )

    if (
      typeof tair === 'undefined' ||
      typeof tsurf === 'undefined' ||
      typeof tman === 'undefined'
    )
      throw new Error()

    const listDataValue = [
      { name: 'PulseTime', dataValue: 'Current_Time pulse', value: '' },
      { name: 'ForceMax', dataValue: 'Raw_Load' },
      { name: 'D_0300', dataValue: 'Raw_D-300' },
      { name: 'D_0200', dataValue: 'Raw_D-200' },
      { name: 'D0000', dataValue: 'Raw_D0' },
      { name: 'D0200', dataValue: 'Raw_D200' },
      { name: 'D0300', dataValue: 'Raw_D300' },
      { name: 'D0600', dataValue: 'Raw_D600' },
      { name: 'D0900', dataValue: 'Raw_D900' },
      { name: 'D1200', dataValue: 'Raw_D1200' },
      { name: 'D1500', dataValue: 'Raw_D1500' },
      { name: 'D1800', dataValue: 'Raw_D1800' },
      { name: 'D2100', dataValue: 'Raw_D2100' },
    ]

    let exportDropString = ''

    point.drops.forEach((drop) => {
      let rawData = ''

      listDataValue.forEach((dataValueKey) => {
        const valueLoad = drop.dataset
          .keys()
          .find((dataValue) => dataValue.toString() === dataValueKey.dataValue)
        if (valueLoad) {
          const val = drop.dataset.get(valueLoad)?.value.displayedString() ?? ''
          dataValueKey.value = val !== '--' ? val : '0,0'
        }
      })

      if (exportRawData) {
        const impactData = drop.impactData()
        if (!impactData) return { index: -1 }

        const frequency = 1000 / impactData.frequency

        let time = ''
        let line = ''
        impactData.load.forEach((l, index) => {
          time += `${'\t'}${(Math.round(index * frequency * 1000) / 1000).toLocaleString(localFR)}`

          const valueLoad = project.units.force.valueToLocaleString(l, {
            ...localStringOptions,
          })
          line += `${'\t'}${valueLoad !== '--' ? valueLoad : '0,0'}`
        })

        rawData += `\nTemps${time}`
        rawData += `\nForce${line}`

        impactData.displacement.forEach((dis, index) => {
          line = ''
          dis.forEach((d) => {
            const valueDis = project.units.deflection.valueToLocaleString(d, {
              ...localStringOptions,
            })
            line += `${'\t'}${valueDis !== '--' ? valueDis : '0,0'}`
          })
          rawData += `\nD${index}t${line}`
        })
      }

      const findVal = (name: string) => {
        return listDataValue.find((dataValue) => dataValue.name === name)?.value
      }

      exportDropString += dedent`\nTABLE${'\t'}Drops
        Numero${'\t'}${drop.index.displayedIndex.toString()}
        PulseTime${'\t'}${findVal('PulseTime')}
        ForceMax${'\t'}${findVal('ForceMax')}
        D_0300${'\t'}${findVal('D_0300')}
        D_0200${'\t'}${findVal('D_0200')}
        D0000${'\t'}${findVal('D0000')}
        D0200${'\t'}${findVal('D0200')}
        D0300${'\t'}${findVal('D0300')}
        D0600${'\t'}${findVal('D0600')}
        D0900${'\t'}${findVal('D0900')}
        D1200${'\t'}${findVal('D1200')}
        D1500${'\t'}${findVal('D1500')}
        D1800${'\t'}${findVal('D1800')}
        D2100${'\t'}${findVal('D2100')}${rawData}
        `
    })

    exportReportPoint += dedent`
      \nTABLE${'\t'}ReportPoints
      Numero${'\t'}${point.number()}
      Latitude${'\t'}${(Math.round(lat * 1000000) / 1000000).toLocaleString(localFR, { roundingPriority: 'morePrecision' })}
      Longitude${'\t'}${(Math.round(lng * 1000000) / 1000000).toLocaleString(localFR, { roundingPriority: 'morePrecision' })}
      DOP${'\t'}${DOP}
      Chainage${'\t'}${chainage}
      Tair${'\t'}${tair}
      Tsurf${'\t'}${tsurf}
      Tman${'\t'}${tman}
      Commentaire${'\t'}${comment}
      Date${'\t'}${pointDate}${exportDropString}
    `
  })
  return exportReportPoint
}
