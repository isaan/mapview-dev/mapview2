import * as fs from 'fs'
import { describe, expect, test } from 'vitest'

import { getFileFromPath, getFilesPathFromDir } from '/src/tests'

describe('Test legacyTxt', () => {
  const files = getFilesPathFromDir(`${__dirname}/files/`)

  test.each(
    files.map((filePath) => {
      const fileInfo = getFileFromPath(filePath)

      const fileContent = fs.readFileSync(filePath, 'utf-8')
      const lines = fileContent.split(/\r?\n/)
      return [fileInfo.name, lines]
    }),
  )('test formatting %s', (_, lines) => {
    lines.forEach((line) => {
      expect(line).toMatch(/^(?!\s).*[^,]$/)
    })
  })
})
