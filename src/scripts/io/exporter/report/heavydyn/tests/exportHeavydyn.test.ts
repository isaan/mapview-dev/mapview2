import { beforeAll, describe, expect, test } from 'vitest'

import { store } from '/src/store'

import { writeDrops as writeDropsDyna } from '../dynatest'
import { writeDrops as writeDropsF25 } from '../f25'
import { writeDrops as writeDropsPDX } from '../pdx'
import { writeDrop as writeDropSweco } from '../sweco'
import { testPoint, testProject } from './constante'

describe('export heavydyn', () => {
  beforeAll(() => {
    store.pushAndSelectProject(testProject)
  })

  test('check drop writing order PDX', () => {
    expect(writeDropsPDX(testPoint, testProject.calibrations.dPlate)).toEqual(
      'DropData_0 = 856.87,430.75,525.16,725.85,556.34,441.25,235.79,158.55,125.65,97.13,78.74,62.02',
    )
  })

  test('check drop writing order f25', () => {
    expect(writeDropsF25(testPoint, testProject.calibrations.dPlate)).toEqual(
      '\n   1,   857, 430.7, 525.2, 725.9, 556.3, 441.3, 235.8, 158.5, 125.7,  97.1,  78.7,  62.0',
    )
  })

  test('check drop writing order dynatest', () => {
    expect(writeDropsDyna(testPoint, testProject.calibrations.dPlate)).toEqual(
      '856.87430.75525.16725.85556.34441.25235.79158.55125.6597.1378.7462.02',
    )
  })

  test('check drop writing order sweco', () => {
    expect(
      writeDropSweco(testPoint.drops[0], testProject.calibrations.channels),
    ).toEqual(
      '1	430.7	525.2	725.9	556.3	441.3	235.8	158.5	125.7	97.1	78.7	62.0	0	60.6	11.4	12.9	0.0	26.13',
    )
  })
})
