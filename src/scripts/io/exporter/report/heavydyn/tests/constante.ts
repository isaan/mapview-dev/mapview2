/* eslint-disable sonarjs/no-duplicate-string */
import { createHeavydynProjectFromJSON } from '/src/scripts'

const pointJson: JSONHeavydynPoint = {
  version: 1,
  base: {
    version: 1,
    id: 'AFA51110A9BEC2FF',
    index: 0,
    settings: {
      version: 1,
      isVisible: true,
    },
    number: 1,
    date: '2022-02-07T14:05:08.000+02:00',
    coordinates: {
      lng: 2.5705123982544364,
      lat: 48.9012709766472,
    },
    data: [
      {
        version: 1,
        label: 'Tair',
        value: 11.416266826149995,
      },
      {
        version: 1,
        label: 'Tsurf',
        value: 12.934422810872396,
      },
      {
        version: 1,
        label: 'Tman',
        value: 0,
      },
      {
        version: 1,
        label: 'Chainage',
        value: 124.5,
      },
    ],
    information: [
      {
        version: 1,
        label: 'Comment',
        value: '',
        settings: {
          version: 1,
          readOnly: true,
        },
      },
    ],
    drops: [
      {
        version: 1,
        base: {
          version: 1,
          data: [
            {
              version: 1,
              label: 'PulseTime',
              value: 0.026133333333333335,
            },
            {
              version: 1,
              label: 'Load',
              value: 60568.26659695457,
            },
            {
              version: 1,
              label: 'D-300',
              value: 0.0004307461802140435,
            },
            {
              version: 1,
              label: 'D-200',
              value: 0.000525159865777031,
            },
            {
              version: 1,
              label: 'D0',
              value: 0.00072585187802953,
            },
            {
              version: 1,
              label: 'D200',
              value: 0.0005563449417392011,
            },
            {
              version: 1,
              label: 'D300',
              value: 0.0004412546542434811,
            },
            {
              version: 1,
              label: 'D600',
              value: 0.00023579299487138774,
            },
            {
              version: 1,
              label: 'D900',
              value: 0.00015854907446633348,
            },
            {
              version: 1,
              label: 'D1200',
              value: 0.0001256519726681619,
            },
            {
              version: 1,
              label: 'D1500',
              value: 0.00009712603427732983,
            },
            {
              version: 1,
              label: 'D1800',
              value: 0.00007873560768526524,
            },
            {
              version: 1,
              label: 'D2100',
              value: 0.00006201807970455643,
            },
          ],
          index: 0,
        },
        distinct: {
          version: 1,
        },
      },
    ],
  },
  distinct: {
    version: 1,
  },
}

const zoneJson: JSONHeavydynZone = {
  version: 1,
  base: {
    version: 1,
    name: 'Zone 1',
    points: [pointJson],
    settings: {
      version: 2,
      color: 'orange',
      isVisible: true,
    },
  },
  distinct: {
    version: 1,
  },
}

const reportJson: JSONHeavydynReport = {
  version: 1,
  base: {
    version: 4,
    id: '1',
    name: 'pv-01',
    referencePath: {
      version: 1,
      origin: 0,
      points: [],
      settings: {
        version: 1,
        isVisible: true,
      },
    },
    dataLabels: {
      version: 1,
      table: {
        version: 1,
        selectedIndex: 0,
        list: [
          {
            version: 1,
            from: 'Drop',
            dataLabels: [],
            index: 2,
          },
          {
            version: 1,
            from: 'Point',
            dataLabels: [],
          },
          {
            version: 1,
            from: 'Zone',
            dataLabels: [],
          },
        ],
      },
    },
    thresholds: {
      version: 1,
      colors: {
        version: 2,
        high: 'red',
        middle: 'yellow',
        low: 'green',
      },
      inputs: {
        version: 1,
        isRequiredARange: true,
        isOptionalARange: true,
      },
    },
    zones: [zoneJson],
    settings: {
      version: 2,
      colorization: 'Threshold',
      groupBy: 'Number',
      iconName: 'Circle',
      isVisible: true,
      isRefPathEditing: false,
    },
    platform: [
      {
        version: 1,
        label: 'Type',
        value: 'Voirie',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'Layer',
        value: 'Couche de roulement',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'Material',
        value: 'Béton bitumineux',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'State',
        value: 'Sèche',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'GTR',
        value: 'N.S.',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
    ],
    information: [
      {
        version: 1,
        label: 'Date',
        value: '2022-02-07T15:04:27.000+02:00',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'Operator',
        value: 'SP',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'Lane',
        value: 'Gauche',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'Sens',
        value: 'Paris -> Province',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'Part',
        value: 'BAU',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'Weather',
        value: 'Beau',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
      {
        version: 1,
        label: 'Comment',
        value: '',
        settings: {
          version: 1,
          readOnly: false,
        },
      },
    ],
  },
  distinct: {
    version: 1,
    dataLabels: {
      version: 1,
      selectedIndex: 0,
      list: [
        {
          version: 1,
          base: {
            version: 1,
            from: 'Drop',
            choices: {
              version: 1,
              selectedIndex: 16,
              list: [
                {
                  version: 1,
                  name: 'PulseTime',
                  unit: 'time',
                },
                {
                  version: 1,
                  name: 'Load',
                  unit: 'force',
                },
                {
                  version: 1,
                  name: 'D-300',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D-200',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D0',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D200',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D300',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D600',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D900',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D1200',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D1500',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D1800',
                  unit: 'deflection',
                },
                {
                  version: 1,
                  name: 'D2100',
                  unit: 'deflection',
                },
              ],
            },
          },
          distinct: {
            version: 1,
            indexes: {
              version: 1,
              selectedIndex: 2,
              list: [
                {
                  version: 1,
                  base: {
                    version: 1,
                    displayedIndex: 1,
                  },
                  distinct: {
                    version: 1,
                    type: 'Distance',
                    unit: 'Distance',
                    value: 0.1,
                  },
                },
                {
                  version: 1,
                  base: {
                    version: 1,
                    displayedIndex: 2,
                  },
                  distinct: {
                    version: 1,
                    type: 'Force',
                    unit: 'Force',
                    value: 60000,
                  },
                },
                {
                  version: 1,
                  base: {
                    version: 1,
                    displayedIndex: 3,
                  },
                  distinct: {
                    version: 1,
                    type: 'Force',
                    unit: 'Force',
                    value: 60000,
                  },
                },
              ],
            },
            sequenceName: 'FR-65KN',
          },
        },
        {
          version: 1,
          base: {
            version: 1,
            from: 'Point',
            choices: {
              version: 1,
              selectedIndex: 0,
              list: [
                {
                  version: 1,
                  name: 'Tair',
                  unit: 'temperature',
                },
                {
                  version: 1,
                  name: 'Tsurf',
                  unit: 'temperature',
                },
                {
                  version: 1,
                  name: 'Tman',
                  unit: 'temperature',
                },
                {
                  version: 1,
                  name: 'Chainage',
                  unit: 'distance',
                },
              ],
            },
          },
          distinct: {
            version: 1,
          },
        },
        {
          version: 1,
          base: {
            version: 1,
            from: 'Zone',
            choices: {
              version: 1,
              selectedIndex: 0,
              list: [],
            },
          },
          distinct: {
            version: 1,
          },
        },
      ],
    },
    thresholds: {
      version: 2,
      deflection: {
        version: 1,
        selectedIndex: 8,
        custom: {
          version: 1,
          type: 'Bicolor',
          value: 0,
          valueHigh: 0,
        },
      },
      distance: {
        version: 1,
        selectedIndex: 0,
        custom: {
          version: 1,
          type: 'Bicolor',
          value: 0,
          valueHigh: 0,
        },
      },
      force: {
        version: 1,
        selectedIndex: 0,
        custom: {
          version: 1,
          type: 'Bicolor',
          value: 0,
          valueHigh: 0,
        },
      },
      temperature: {
        version: 1,
        selectedIndex: 0,
        custom: {
          version: 1,
          type: 'Bicolor',
          value: 0,
          valueHigh: 0,
        },
      },
      time: {
        version: 1,
        selectedIndex: 0,
        custom: {
          version: 1,
          type: 'Bicolor',
          value: 0,
          valueHigh: 0,
        },
      },
      modulus: {
        version: 1,
        selectedIndex: 0,
        custom: {
          version: 1,
          type: 'Bicolor',
          value: 0,
          valueHigh: 0,
        },
      },
      cumSum: {
        version: 1,
        selectedIndex: 0,
        custom: {
          version: 1,
          type: 'Bicolor',
          value: 0,
          valueHigh: 0,
        },
      },
      radius: {
        version: 1,
        selectedIndex: 0,
        custom: {
          version: 1,
          type: 'Bicolor',
          value: 0,
          valueHigh: 0,
        },
      },
    },
  },
}

export const testProject = await createHeavydynProjectFromJSON(
  {
    version: 1,
    machine: 'Heavydyn',
    base: {
      version: 2,
      name: 'Démo Heavydyn FR',
      database: {
        version: 2,
        software: 'Anydyn',
        // eslint-disable-next-line sonarjs/no-hardcoded-ip
        softwareVersion: '3.6.45.0',
        local: 'fr-FR',
        timeZone: 'Paris, Madrid',
        timeZoneOffsetMinutes: 120,
      },
      settings: {
        version: 1,
        areOverlaysVisible: true,
        arePointsLinked: true,
        arePointsVisible: true,
        pointsState: 'number',
        map: {
          version: 1,
          styleIndex: 0,
          pitch: 15.996138524938859,
          rotation: 0,
          zoom: 12.106718115972976,
          coordinates: {
            lng: 2.594183905532873,
            lat: 48.88360655993452,
          },
        },
      },
      acquisitionParameters: {
        version: 1,
        frequency: 15000,
        nbSamples: 1000,
        preTrig: 0.007000000216066837,
        smoothing: true,
      },
      overlays: [],
      information: [
        {
          version: 1,
          label: 'Customer',
          value: 'Rincent NDT',
          settings: {
            version: 1,
            readOnly: false,
          },
        },
        {
          version: 1,
          label: 'Contact',
          value: 'Sébastien Pellevrault',
          settings: {
            version: 1,
            readOnly: false,
          },
        },
        {
          version: 1,
          label: 'Project',
          value: '23-2469',
          settings: {
            version: 1,
            readOnly: false,
          },
        },
        {
          version: 1,
          label: 'Town',
          value: 'Chelles',
          settings: {
            version: 1,
            readOnly: false,
          },
        },
        {
          version: 1,
          label: 'Date',
          value: '2022-02-07T15:04:20.000+02:00',
          settings: {
            version: 1,
            readOnly: false,
          },
        },
        {
          version: 1,
          label: 'Site',
          value: 'D224',
          settings: {
            version: 1,
            readOnly: false,
          },
        },
        {
          version: 1,
          label: 'Comments',
          value: 'Données démo non représentatives',
          settings: {
            version: 1,
            readOnly: false,
          },
        },
      ],
      hardware: [
        {
          version: 1,
          label: 'Serial number',
          value: 'HVY-106-C',
          settings: {
            version: 1,
            readOnly: true,
          },
        },
        {
          version: 1,
          label: 'Attribution',
          value: 'Rincent NDT',
          settings: {
            version: 1,
            readOnly: true,
          },
        },
        {
          version: 1,
          label: 'MAC address',
          value: '16-80-D0-EC-71-F9-C5-30',
          settings: {
            version: 1,
            readOnly: true,
          },
        },
        {
          version: 1,
          label: 'License start',
          value: '0001-01-01T00:00:00.000Z',
          settings: {
            version: 1,
            readOnly: true,
          },
        },
        {
          version: 1,
          label: 'License end',
          value: '0001-01-01T00:00:00.000Z',
          settings: {
            version: 1,
            readOnly: true,
          },
        },
        {
          version: 1,
          label: 'Certificate start',
          value: '0001-01-01T00:00:00.000Z',
          settings: {
            version: 1,
            readOnly: true,
          },
        },
        {
          version: 1,
          label: 'Certificate end',
          value: '0001-01-01T00:00:00.000Z',
          settings: {
            version: 1,
            readOnly: true,
          },
        },
      ],
      reports: {
        version: 1,
        selectedIndex: 0,
        list: [reportJson],
      },
    },
    distinct: {
      version: 1,
      calibrations: {
        version: 1,
        date: '2018-06-20T18:45:22.000+02:00',
        dPlate: 0.30000001192092896,
        channels: [
          {
            version: 3,
            name: 'FELI',
            position: '0',
            gain: 3.8879999664231946e-8,
            channelNumber: 0,
            type: 'LoadCell',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-122',
            position: '-0.30000001192092896',
            gain: 28.989999771118164,
            channelNumber: 1,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-123',
            position: '-0.20000000298023224',
            gain: 29.450000762939453,
            channelNumber: 2,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-159',
            position: '0',
            gain: 29.65999984741211,
            channelNumber: 3,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-124',
            position: '0.20000000298023224',
            gain: 29.889999389648438,
            channelNumber: 4,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-125',
            position: '0.30000001192092896',
            gain: 29.459999084472656,
            channelNumber: 5,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-126',
            position: '0.6000000238418579',
            gain: 29.510000228881836,
            channelNumber: 6,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-127',
            position: '0.8999999761581421',
            gain: 29.239999771118164,
            channelNumber: 7,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-128',
            position: '1.2000000476837158',
            gain: 29.65999984741211,
            channelNumber: 8,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-129',
            position: '1.5',
            gain: 29.709999084472656,
            channelNumber: 9,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-130',
            position: '1.7999999523162842',
            gain: 29.34000015258789,
            channelNumber: 10,
            type: 'Geophone',
            v0: 0,
          },
          {
            version: 3,
            name: 'FG-131',
            position: '2.0999999046325684',
            gain: 29.40999984741211,
            channelNumber: 11,
            type: 'Geophone',
            v0: 0,
          },
        ],
        sensors: [
          {
            version: 1,
            name: 'AirTemp',
            gain: 0.3400000035762787,
            type: 'AirTemp',
            v0: 100,
          },
          {
            version: 1,
            name: 'SurfTemp',
            gain: 0.75,
            type: 'SurfTemp',
            v0: 7,
          },
          {
            version: 1,
            name: 'DMI',
            gain: 178.5,
            type: 'Dmi',
            v0: 0,
          },
        ],
      },
      units: {
        version: 3,
        deflection: {
          version: 2,
          base: {
            version: 2,
            currentUnit: '1/100 mm',
            currentPrecision: 0,
            format: 'dec',
          },
          currentUnit: '1/100 mm',
          currentPrecision: 0,
          max: 0.003,
          min: 0,
          format: 'dec',
        },
        distance: {
          version: 2,
          base: {
            version: 2,
            currentUnit: 'm',
            currentPrecision: 0,
            format: 'dec',
          },
          currentUnit: 'm',
          currentPrecision: 0,
          max: 100000,
          min: 0,
          format: 'dec',
        },
        force: {
          version: 2,
          base: {
            version: 2,
            currentUnit: 'kN',
            currentPrecision: 0,
            format: 'dec',
          },
          currentUnit: 'kN',
          currentPrecision: 0,
          max: 500000,
          min: 0,
          format: 'dec',
        },
        temperature: {
          version: 2,
          base: {
            version: 2,
            currentUnit: '°C',
            currentPrecision: 0,
            format: 'dec',
          },
          currentUnit: '°C',
          currentPrecision: 0,
          max: 150,
          min: -50,
          format: 'dec',
        },
        time: {
          version: 2,
          base: {
            version: 2,
            currentUnit: 'ms',
            currentPrecision: 0,
            format: 'dec',
          },
          currentUnit: 'ms',
          currentPrecision: 0,
          max: 0.1,
          min: 0,
          format: 'dec',
        },
        modulus: {
          version: 2,
          base: {
            version: 2,
            currentUnit: 'MPa',
            currentPrecision: 2,
            format: 'dec',
          },
          currentUnit: 'MPa',
          currentPrecision: 2,
          max: 100000000000,
          min: 0,
          format: 'dec',
        },
        cumSum: {
          version: 2,
          base: {
            version: 2,
            currentUnit: '',
            currentPrecision: 1,
            format: 'dec',
          },
          currentUnit: '',
          currentPrecision: 1,
          max: 100,
          min: -100,
          format: 'dec',
        },
        radius: {
          version: 2,
          base: {
            version: 2,
            currentUnit: 'm',
            currentPrecision: 0,
            format: 'dec',
          },
          currentUnit: 'm',
          currentPrecision: 0,
          max: 2000,
          min: 0,
          format: 'dec',
        },
        position: {
          version: 2,
          currentUnit: 'mm',
          currentPrecision: 0,
          format: 'dec',
        },
        sensorCoefficient: {
          version: 2,
          currentUnit: '',
          currentPrecision: 3,
          format: 'sci',
        },
      },
      correctionParameters: {
        version: 1,
        load: {
          version: 2,
          active: false,
          source: 'Sequence',
          customValue: 65000,
        },
        temperature: {
          version: 3,
          country: 'fr',
          active: false,
          source: 'Tair',
          average: 'Zone',
          customValue: 0,
          reference: 15,
          structureType: 0,
        },
      },
    },
  },
  null,
)

export const testPoint = testProject.reports
  .selected()
  ?.zones()[0]
  .points()[0] as HeavydynPoint
