import {
  convertUint8ArrayToData64Image,
  getExtensionFromPath,
} from '/src/scripts'

export const screenshotFolderPathInZip = 'screenshots/'

export const getScreenshotFileNamesFromZIP = (zip: Zippable) =>
  Object.keys(zip)
    .filter((key) => key.startsWith(screenshotFolderPathInZip))
    .map((key) => key.substring(screenshotFolderPathInZip.length))
    .filter((key) => key)

export const importScreenshotsFromZIP = (
  zip: Unzipped,
  json: JSONMachineProject,
  project: MachineProject,
) => {
  const screenshots = getScreenshotFileNamesFromZIP(zip)

  json.base.reports.list.forEach((report, index) => {
    screenshots
      .filter((screenshot) => screenshot.includes(report.base.id.toString()))
      .forEach((screenshot) => {
        const array = zip[`${screenshotFolderPathInZip}${screenshot}`]

        const data64 = convertUint8ArrayToData64Image(
          array,
          String(screenshot.split('.').pop()),
        )

        project.reports.list()[index]?.album.set((l) => {
          const screen = {
            name: screenshot.split('/').pop()?.split('.')[0] ?? 'screenshot',
            file: data64,
            extension: getExtensionFromPath(screenshot),
          }
          l.push(screen)
          return l
        })
      })
  })
}
