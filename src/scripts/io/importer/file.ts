/* eslint-disable no-console */
import { env } from '/src/env'
import {
  convertJSONFromPRJZToMPVZ,
  createMachineProjectFromJSON,
  ErrorCode,
  getExtensionFromFile,
  importOverlaysFromZIP,
  importRawDataFromZIP,
  importScreenshotsFromZIP,
  unzipFile,
  waitForMap,
} from '/src/scripts'
import { store } from '/src/store'

export const unzippedToJSON = (unzipped: Unzipped) => {
  const jsonUint = unzipped['database.json']

  if (jsonUint.length === 0) {
    throw new Error(ErrorCode.EMPTY_FILE)
  }

  return JSON.parse(new TextDecoder().decode(jsonUint))
}

export const getProjectJSONFromZip = (
  unzipped: Unzipped,
  extension: string,
) => {
  const importedJSON = unzippedToJSON(unzipped)

  if (extension === 'mpvz') {
    return (importedJSON as JSONMapview).project
  }

  if (!isValidPVsFormat(importedJSON)) {
    throw new Error(ErrorCode.NO_POINT)
  }

  return convertJSONFromPRJZToMPVZ(importedJSON)
}

export const isValidPVsFormat = (json: JSONAny) => {
  return (
    json.PVs?.some((pv: JSONAny) => pv.Points && pv.Points.length > 0) || false
  )
}

export const importFile = async (file: File) => {
  let project = null as MachineProject | null
  try {
    await waitForMap()
    const extension = getExtensionFromFile(file)

    const unzipped = await unzipFile(file)

    const jsonProject = getProjectJSONFromZip(unzipped, extension || '')

    if (!env.isTest) console.log(jsonProject)

    if (jsonProject) {
      project = await createMachineProjectFromJSON(jsonProject, store.map())

      setTimeout(async () => {
        if (project) {
          importScreenshotsFromZIP(unzipped, jsonProject, project)

          importRawDataFromZIP(unzipped, project)

          await waitForMap()

          importOverlaysFromZIP(unzipped, jsonProject, project)

          setTimeout(() => {
            if (project) {
              project.state.set('Loaded')
            }
          }, 5000)
        }
      }, 100)

      return project
    }
    return null
  } catch (error) {
    console.error('Import error', error)
    store.importError.set((error as Error).message)
    return null
  }
}
