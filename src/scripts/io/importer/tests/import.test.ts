import { describe, expect, test } from 'vitest'

import { getProjectJSONFromZip, mpvzExporter, unzipFile } from '/src/scripts/'
import { getFilesForErrorTest, getTestDataFromDir } from '/src/scripts/io/tests'

describe('Tests import', async () => {
  const path = `${__dirname}/files`
  const errorPath = `${__dirname}/error`

  const testData = await getTestDataFromDir(path)
  const errorTestData = getFilesForErrorTest(errorPath)

  test.each(
    testData
      .filter((data) => data.prjz)
      .map((data) => [
        data.directoryName,
        data.prjz,
        data.currentMPVZ || data.mpvz.file,
      ]),
  )('test PRJZ: %s', async (_, prjz, expected) => {
    const actualMPVZ = await mpvzExporter.export(prjz.project as MachineProject)

    await expect(actualMPVZ).toBeSameZip(expected, true)
  })

  test.each(
    errorTestData.map((data) => [data.name, data.file, data.errorCode]),
  )('test error: %s', async (name, file, expected) => {
    const unzipped = await unzipFile(file)
    expect(() =>
      getProjectJSONFromZip(unzipped, name.split('.').pop() ?? ''),
    ).toThrowError(expected)
  })
})
