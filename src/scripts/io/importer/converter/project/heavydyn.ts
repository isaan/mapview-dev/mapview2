import { getBrowserLocale } from '/src/locales'
import { run } from '/src/scripts'
import {
  countryData,
  defaultCountry,
} from '/src/scripts/entities/project/heavydyn/correctionParameters/temperature'

import { convertPRJZToHeavydynReport } from '../report'

export const convertPRJZToHeavydynProject = (
  json: JSONAny,
  baseProject: JSONBaseProject,
): JSONHeavydynProject => {
  const project: JSONHeavydynProject = {
    version: 1,
    machine: 'Heavydyn',
    base: baseProject,
    distinct: convertPRJZToHeavydynProjectDistinct(json),
  }

  json.PVs.forEach((jsonPV: RecordAny, i: number) => {
    project.base.reports.list.push(convertPRJZToHeavydynReport(jsonPV, i, json))
  })

  project.base.reports.selectedIndex = project.base.reports.list.length
    ? 0
    : null

  return project
}

export const convertPRJZToHeavydynProjectDistinct = (
  json: JSONAny,
): JSONHeavydynProjectDistinct => {
  const units = convertPRJZToHeavydynUnits(json)

  const browserLocale = getBrowserLocale(true)

  const localCountry = countryData.find((data) => data.code === browserLocale)

  return {
    version: 1,
    units,
    correctionParameters: {
      version: 1,
      load: {
        version: 2,
        active: false,
        source: 'Sequence',
        customValue: 65000,
      },
      temperature: {
        version: 3,
        country:
          browserLocale && localCountry
            ? localCountry.code
            : defaultCountry.code,
        active: false,
        source: 'Tair',
        average: 'Zone',
        customValue: 0,
        reference: 15,
        structureType: 0,
      },
    },
    calibrations: {
      version: 1,
      date: json.Calibrations.Date,
      dPlate: json.Calibrations.Dplate,
      channels:
        json.Calibrations.Channels.map((channel: RecordAny): JSONChannel => {
          return {
            version: 3,
            name: channel.Name,
            adc: channel.ChannelAdc,
            position: channel.Position,
            gain: channel.Gain,
            channelNumber: channel.ChannelAcqu,
            type: channel.Type,
            v0: channel.V0,
          }
        }) || [],
      sensors:
        json.Calibrations.Sensors.map((sensor: RecordAny): JSONSensor => {
          return {
            version: 1,
            name: sensor.Name,
            gain: sensor.Gain,
            type: sensor.Type,
            v0: sensor.V0,
          }
        }) || [],
    },
  }
}

export const convertPRJZToHeavydynUnits = (json: JSONAny) => {
  const units: JSONHeavydynUnits = {
    version: 3,
    deflection: {
      version: 1,
      currentUnit: run((): PossibleHeavydynDeflectionUnits => {
        switch (
          (json.ExportedData.Drops as RecordAny[]).find(
            (exportedUnit) => exportedUnit.Type === 'Deflection',
          )?.Unit
        ) {
          case 'mm':
            return 'mm'
          case 'um':
            return 'um'
          default:
            return '1/100 mm'
        }
      }),
      currentPrecision: 0,
      max: 0.003,
    },
    force: {
      version: 1,
      currentUnit: run((): PossibleHeavydynForceUnits => {
        switch (
          (json.ExportedData.Drops as RecordAny[]).find(
            (exportedUnit) => exportedUnit.Type === 'Load',
          )?.Unit
        ) {
          case 'N':
            return 'N'
          default:
            return 'kN'
        }
      }),
      currentPrecision: 0,
      max: 500000,
    },
    distance: {
      version: 1,
      currentUnit: run((): PossibleHeavydynDistanceUnits => {
        switch (
          (json.ExportedData.Points as RecordAny[]).find(
            (exportedUnit) => exportedUnit.Type === 'Distance',
          )?.Unit
        ) {
          case 'km':
            return 'km'
          case 'mi':
            return 'mi'
          default:
            return 'm'
        }
      }),
      currentPrecision: 0,
      max: 100000,
    },
    time: {
      version: 1,
      currentUnit: run((): PossibleHeavydynTimeUnits => {
        switch (
          (json.ExportedData.Drops as RecordAny[]).find(
            (exportedUnit) => exportedUnit.Type === 'Time',
          )?.Unit
        ) {
          case 's':
            return 's'
          case 'us':
            return 'us'
          default:
            return 'ms'
        }
      }),
      currentPrecision: 0,
      max: 1,
    },
    temperature: {
      version: 1,
      currentUnit: run((): PossibleHeavydynTemperatureUnits => {
        switch (
          (json.ExportedData.Points as RecordAny[]).find(
            (exportedUnit) => exportedUnit.Type === 'Temperature',
          )?.Unit
        ) {
          case 'K':
            return 'K'
          case 'degF':
          case '°F':
            return '°F'
          default:
            return '°C'
        }
      }),
      currentPrecision: 0,
      min: -50,
      max: 150,
    },
    modulus: {
      version: 1,
      currentUnit: 'MPa',
      currentPrecision: 0,
      max: 1000000000,
    },
    cumSum: {
      version: 1,
      currentUnit: '',
      currentPrecision: 0,
      max: 100,
    },
    radius: {
      version: 1,
      currentUnit: 'm',
      currentPrecision: 0,
      max: 2000,
    },
    position: {
      version: 2,
      currentUnit: 'mm',
      currentPrecision: 0,
      format: 'dec',
    },
    sensorCoefficient: {
      version: 2,
      currentUnit: '',
      currentPrecision: 3,
      format: 'sci',
    },
  }

  return units
}
