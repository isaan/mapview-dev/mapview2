interface ImpactData {
  readonly load: number[]
  readonly displacement: number[][]
  readonly frequency: number
}
