import { expect, test } from '@playwright/test'

import {
  DialogConfiguration,
  DialogInformation,
  HomePage,
} from '../../../../pom'

test('Modal Informations', async ({ page, baseURL }) => {
  if (!baseURL) return

  const launchDemo = new HomePage(page, baseURL)
  await launchDemo.goto()
  await launchDemo.launch()

  const dialogInformation = new DialogInformation(page)

  const unlockedInput = page.locator('#home-input-name')
  const lockedInput = page.locator('#home-input-date')
  const numberAsStringInput = page.locator(
    '#home-input-calibration-channels-1-channelNumber',
  )

  await expect(page.locator('#desktop-menu-project')).toBeVisible()

  await dialogInformation.checkDialogInformationButton()
  await dialogInformation.launch()
  await dialogInformation.checkDialogInformation()

  await expect(unlockedInput).toBeVisible()
  await expect(unlockedInput).toBeEditable()
  await expect(unlockedInput.locator('..')).toHaveClass(/bg-transparent/)

  await expect(lockedInput).toBeVisible()
  await expect(lockedInput).toHaveAttribute('readonly')
  await expect(lockedInput.locator('..')).toHaveClass(/bg-black/)
  await expect(lockedInput.locator('..').locator('#right-icon')).toBeVisible()

  await expect(numberAsStringInput).toBeVisible()
  await expect(numberAsStringInput).toHaveValue('1')
})

test('Modal Informations reactivity', async ({ page, baseURL }) => {
  if (!baseURL) return

  const launchDemo = new HomePage(page, baseURL)
  await launchDemo.goto()
  await launchDemo.launch()

  const dialogInformation = new DialogInformation(page)
  const dialogConfigurations = new DialogConfiguration(page)

  const reactivityPositionValueInput = page.locator(
    '#home-input-calibration-channels-1-position',
  )
  const configPositionPrecisionBtn = page.locator(
    '#position-precision-select-button',
  )
  const configPositionPrecision = page.locator('#position-precision-select')
  const configPositionPrecisionOption2 = page.locator(
    '#position-precision-select-options-2',
  )

  const configPositionUnitBtn = page.locator('#position-unit-select-button')
  const configPositionUnit = page.locator('#position-unit-select')
  const configPositionUnitOptionCM = page.locator(
    '#position-unit-select-options-1',
  )

  const reactivitySensorCoefficientValueInput = page.locator(
    '#home-input-calibration-channels-0-gain',
  )
  const configSensorCoefficientPrecision = page.locator(
    '#sensorcoefficient-precision-select',
  )
  const configSensorCoefficientPrecisionBtn = page.locator(
    '#sensorcoefficient-precision-select-button',
  )
  const configSensorCoefficientPrecisionBtnOption1 = page.locator(
    '#sensorcoefficient-precision-select-options-1',
  )

  await expect(page.locator('#desktop-menu-project')).toBeVisible()

  await dialogInformation.checkDialogInformationButton()
  await dialogInformation.launch()
  await dialogInformation.checkDialogInformation()

  await dialogConfigurations.checkDialogConfigurationButton()
  await dialogConfigurations.launch()
  await dialogConfigurations.checkDialogConfiguration()

  await expect(reactivityPositionValueInput).toBeVisible()
  await expect(configPositionPrecisionBtn).toBeVisible()
  await expect(configSensorCoefficientPrecisionBtn).toBeVisible()
  await expect(configPositionUnitBtn).toBeVisible()

  await expect(reactivityPositionValueInput).toHaveValue('-300 mm')
  await configPositionUnitBtn.click()
  await expect(configPositionUnit).toBeVisible()
  await expect(configPositionUnitOptionCM).toBeVisible()
  await configPositionUnitOptionCM.click()
  await expect(reactivityPositionValueInput).toHaveValue('-30 cm')

  await configPositionPrecisionBtn.click()
  await expect(configPositionPrecision).toBeVisible()
  await expect(configPositionPrecisionOption2).toBeVisible()
  await configPositionPrecisionOption2.click()
  await page.waitForTimeout(2000)
  await expect(reactivityPositionValueInput).toHaveValue('-30.00 cm')

  await expect(reactivitySensorCoefficientValueInput).toHaveValue('3.903e-8')
  await configSensorCoefficientPrecisionBtn.click()
  await expect(configSensorCoefficientPrecision).toBeVisible()
  await expect(configSensorCoefficientPrecisionBtnOption1).toBeVisible()
  await configSensorCoefficientPrecisionBtnOption1.click()
  await expect(reactivitySensorCoefficientValueInput).toHaveValue('3.9e-8')
})
