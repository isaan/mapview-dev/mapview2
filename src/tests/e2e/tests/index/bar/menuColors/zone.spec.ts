import path from 'path'
import { fileURLToPath } from 'url'
import test, { expect } from '@playwright/test'

import { MenuColors } from '/src/tests/e2e/pom/bar/menuColors'
import { DataBar } from '/src/tests/e2e/pom/bar/menuData'
import { HomePage } from '/src/tests/e2e/pom/homePage'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

test.describe('Zone actions', () => {
  test('deleting zone effect group by zone', async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    const dataBar = new DataBar(page)
    const menuColors = new MenuColors(page)

    await homePage.goto()
    await homePage.uploadFile(`${_dirname}/../../../files/generic.mpvz`, 0)

    await dataBar.setGroupBy(1)

    const nbLines = await dataBar.getDataLines().count()
    const nbTables = await dataBar.getAllTable().count()

    await menuColors.deleteZone(1)

    expect(await dataBar.getDataLines().count()).toBe(nbLines)
    expect(await dataBar.getAllTable().count()).toBe(nbTables - 1)
  })

  test('deleting zone effect group by number', async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    const dataBar = new DataBar(page)
    const menuColors = new MenuColors(page)

    await homePage.goto()
    await homePage.uploadFile(`${_dirname}/../../../files/generic.mpvz`, 0)

    await dataBar.setGroupBy(0)

    const nbLines = await dataBar.getDataLines().count()
    const nbTables = await dataBar.getAllTable().count()

    await menuColors.deleteZone(1)

    expect(await dataBar.getDataLines().count()).toBe(nbLines)
    expect(await dataBar.getAllTable().count()).toBe(nbTables)
  })
})
