import path from 'path'
import { fileURLToPath } from 'url'
import test, { expect } from '@playwright/test'

import { DataBar } from '/src/tests/e2e/pom/bar'
import { HomePage } from '/src/tests/e2e/pom/homePage'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

test.describe('test table points', () => {
  test('test sorting', async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    const dataBar = new DataBar(page)

    await homePage.goto()
    await homePage.uploadFile(`${_dirname}/../../../files/generic.mpvz`, 0)
    await homePage.checkMenus()

    await dataBar.setGroupBy(0)

    const initialLine = dataBar.getLine(0, 0)
    const destinationLine = dataBar.getLine(0, 5)

    const lignes = await page
      .locator('[data-testid="table-points"] tbody tr')
      .all()

    const initialValues = await Promise.all(
      lignes.map((loc) => loc.locator('td').nth(3).innerText()),
    )

    await initialLine.getByRole('button').first().hover()
    await page.waitForTimeout(1000)
    await page.mouse.down()
    await page.waitForTimeout(1000)

    await destinationLine.scrollIntoViewIfNeeded()
    await page.mouse.move(
      (await destinationLine.boundingBox())?.x ?? 0,
      (await destinationLine.boundingBox())?.y ?? 0,
    )

    await page.waitForTimeout(1000)
    await page.mouse.up()

    expect(initialValues).not.toEqual(
      await Promise.all(
        lignes.map((loc) => loc.locator('td').nth(3).innerText()),
      ),
    )
  })
})
