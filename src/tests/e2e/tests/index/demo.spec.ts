import { test } from '@playwright/test'

import { HomePage } from '../../pom/homePage'

test('demo launch', async ({ page, baseURL }) => {
  if (!baseURL) return

  const launchDemo = new HomePage(page, baseURL)
  await launchDemo.goto()
  await launchDemo.launch()
  await launchDemo.checkMenus()
})
