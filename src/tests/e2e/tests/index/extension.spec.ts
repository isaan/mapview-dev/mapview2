/* eslint-disable no-restricted-syntax */
import path from 'path'
import { fileURLToPath } from 'url'
import test from '@playwright/test'

import {
  getExtensionFromPath,
  getFileNameFromPath,
} from '/src/scripts/utils/file'
import { getFilesPathFromDir } from '/src/tests/utils/files'

import { HomePage } from '../../pom/homePage'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

const acceptedFiles = getFilesPathFromDir(
  `${_dirname}/files/extension/accepted`,
  {
    authorizedExtension: ['prjz', 'dynz', 'mpvz'],
  },
)

for (const filePath of acceptedFiles) {
  test(`check authorized extension ${getExtensionFromPath(filePath)} (${getFileNameFromPath(filePath)})`, async ({
    page,
    baseURL,
  }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    await homePage.goto()

    await homePage.dragAndDropFile(filePath, '#drop-zone', 10000)

    await homePage.checkMenus()
  })
}

const rejectedFiles = getFilesPathFromDir(
  `${_dirname}/files/extension/rejected`,
  {
    authorizedExtension: '*',
  },
)

for (const filePath of rejectedFiles) {
  test(`check unauthorized extension ${getExtensionFromPath(filePath)}`, async ({
    page,
    baseURL,
  }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    await homePage.goto()

    await homePage.dragAndDropFile(filePath, '#drop-zone')

    await homePage.checkErrorModal()
  })
}
