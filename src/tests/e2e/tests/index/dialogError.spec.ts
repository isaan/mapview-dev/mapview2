import path from 'path'
import { fileURLToPath } from 'url'
import { test } from '@playwright/test'

import { getFilesPathFromDir } from '/src/tests/utils/files'

import { HomePage } from '../../pom'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

const files = getFilesPathFromDir(`${_dirname}/files/errors`, {
  authorizedExtension: ['prjz', 'dynz', 'mpvz'],
})

test.describe('dialog error', () => {
  // eslint-disable-next-line no-restricted-syntax
  for (const fileData of files) {
    test(`upload file ${fileData}`, async ({ page, baseURL }) => {
      if (!baseURL) return

      const homePage = new HomePage(page, baseURL)
      await homePage.goto()
      await homePage.uploadFile(fileData)

      await homePage.checkErrorModal()
    })
  }

  test('empty file error', async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    await homePage.goto()

    await homePage.uploadFile(
      `${_dirname}/files/errors/Minidyn_json_empty_error.dynz`,
    )

    await homePage.checkErrorModal(
      'The file is empty, please regenerate the project using the acquisition software',
    )
  })
})
