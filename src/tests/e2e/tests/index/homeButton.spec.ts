import { expect, test } from '@playwright/test'

import { HomePage } from '../../pom/homePage'

test('home button', async ({ page, baseURL }) => {
  if (!baseURL) return

  const launchDemo = new HomePage(page, baseURL)
  await launchDemo.goto()
  await launchDemo.launch()

  const logo = page.locator('#logo')

  await expect(logo).toBeVisible()

  await logo.click()

  await launchDemo.checkHomePage()
})
