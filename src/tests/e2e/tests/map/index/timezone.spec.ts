import path from 'path'
import { fileURLToPath } from 'url'
import { expect, test } from '@playwright/test'

import { HomePage } from '/src/tests/e2e/pom/homePage'
import { getFilesPathFromDir } from '/src/tests/utils/files'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

const files = getFilesPathFromDir(`${_dirname}/files/timezone`, {
  authorizedExtension: ['prjz', 'dynz', 'mpvz'],
})

// eslint-disable-next-line no-restricted-syntax
for (const file of files) {
  test(`Timezone ${file}`, async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    await homePage.goto()

    await page.waitForTimeout(2000)

    await homePage.uploadFile(file)

    await homePage.zoomAndClickOnPoint(0)

    const dateField = page.locator('#point-0-Date')
    await expect(dateField).not.toBeEmpty()
    await expect(dateField).toContainText(/\(UTC [+-]\d{2}:\d{2}\)/)
  })
}
