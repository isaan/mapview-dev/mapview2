import path from 'path'
import { fileURLToPath } from 'url'
import test, { expect } from '@playwright/test'

import { HomePage } from '/src/tests/e2e/pom'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

test.describe('Point', () => {
  test('Check number points', async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    await homePage.goto()

    await homePage.uploadFile(`${_dirname}/files/test_points.mpvz`, 0)

    const points = page.locator('[data-testid^="icon-point-"]')

    await page.waitForTimeout(5000)

    await expect(points).toHaveCount(23)
  })
})
