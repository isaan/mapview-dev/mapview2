/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
import path from 'path'
import { fileURLToPath } from 'url'
import test, { expect } from '@playwright/test'

import { gray } from '/src/scripts/entities/color'
import { HomePage } from '/src/tests/e2e/pom/homePage'
import { getFilesPathFromDir } from '/src/tests/utils/files'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

const filesPath = getFilesPathFromDir(`${_dirname}/files/KO`, {
  authorizedExtension: ['prjz', 'dynz', 'mpvz'],
})

for (const filePath of filesPath) {
  test('Check Ko Point color', async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    await homePage.goto()

    await page.waitForTimeout(2000)
    await homePage.uploadFile(filePath)

    const tableData = page.locator('#data-table')
    await expect(tableData).toBeVisible()
    const tbody = tableData.locator('tbody')
    await expect(tbody).toBeVisible()

    const ko = tbody.getByText('KO')
    const count = await ko.count()

    expect(count).toBeGreaterThan(0)

    for (let i = 0; i < count; i++) {
      const tr = ko.nth(i).locator('..')
      await tr.scrollIntoViewIfNeeded()
      await expect(tr).toHaveAttribute('color', gray)
      const tdNumber = tr.locator('#point-number')
      await expect(tdNumber).toBeVisible()
      const number = Number(await tdNumber.innerText())

      const zoomButton = tr.locator(`#btn-zoom-point-${number - 1}`)
      await zoomButton.click()
      await page.waitForTimeout(2000)

      const point = page.locator(`#icon-point-${number - 1}`)
      await expect(point).toBeVisible()

      const svg = point.locator('svg')

      await expect(svg).toHaveAttribute('fill', '#9da3ae')
    }
  })
}
