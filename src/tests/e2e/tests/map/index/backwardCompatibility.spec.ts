import path from 'path'
import { fileURLToPath } from 'url'
import { expect, test } from '@playwright/test'

import { HomePage } from '/src/tests/e2e/pom/homePage'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

test("Backward Compatibility ’comments'", async ({ page, baseURL }) => {
  if (!baseURL) return

  const homePage = new HomePage(page, baseURL)
  await homePage.goto()

  await page.waitForTimeout(2000)

  await homePage.uploadFile(
    `${_dirname}/files/backwardCompatibility/PORNIC_-_AGGLO__1_.prjz`,
  )

  await homePage.zoomAndClickOnPoint(0)

  const commentField = page.locator('#point-0-Comment')

  await expect(commentField).not.toBeEmpty()
})
