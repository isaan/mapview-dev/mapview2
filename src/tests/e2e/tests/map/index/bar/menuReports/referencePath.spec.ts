/* eslint-disable sonarjs/no-duplicate-string */
import path from 'path'
import { fileURLToPath } from 'url'
import { expect, test, type Page } from '@playwright/test'

import {
  HomePage,
  ProjectBar,
  ReferencePath,
  ReportBar,
} from '../../../../../pom'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

test.describe('Reference Path interaction', () => {
  let page: Page

  // launch project
  let homePage: HomePage
  let reportBar: ReportBar
  let projectBar: ProjectBar
  let refPath: ReferencePath

  test.beforeAll(async ({ browser }) => {
    page = await browser.newPage()

    homePage = new HomePage(page, 'http://localhost:4173')
    reportBar = new ReportBar(page)
    projectBar = new ProjectBar(page)
    refPath = new ReferencePath(page)

    await homePage.goto()

    await homePage.uploadFile(
      `${_dirname}/files/monoReport/fromScratch.mpvz`,
      0,
    )

    await page.waitForTimeout(5000)

    await homePage.zoomOnCurrentReport()
  })

  test.afterEach(async () => {
    if (
      (await refPath.buttonEdit.isEnabled()) &&
      (await refPath.buttonEdit.getAttribute('class'))?.includes(
        'text-green-900',
      )
    ) {
      await refPath.clickEdit()
    }

    if (await refPath.buttonRemove.isEnabled()) {
      await refPath.clickRemove()
    }
  })

  test('initial number of points', async () => {
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    expect(await refPath.getDistanceNumber()).toBe(0)
    await expect(points).toHaveCount(0)
  })

  test("can't create point at init", async () => {
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    await page.mouse.click(600, 300)

    expect(await refPath.getDistanceNumber()).toBe(0)
    expect(await refPath.getLignesNumber()).toBe(0)
    await expect(points).toHaveCount(0)
  })

  test('creating 1 point and deleting it', async () => {
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    await refPath.clickEdit()
    await page.mouse.click(600, 300)

    expect(await refPath.getDistanceNumber()).toBe(0)
    await expect(points).toHaveCount(1)
    expect(await refPath.getLignesNumber()).toBe(0)

    await page.mouse.click(600, 300)

    expect(await refPath.getDistanceNumber()).toBe(0)
    await expect(points).toHaveCount(0)
    expect(await refPath.getLignesNumber()).toBe(0)
  })

  test('creating 1 point and moving it', async () => {
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    await refPath.clickEdit()
    await page.mouse.click(600, 300)

    expect(await refPath.getDistanceNumber()).toBe(0)
    await expect(points).toHaveCount(1)
    expect(await refPath.getLignesNumber()).toBe(0)

    const initialPosition = await points.first().boundingBox()
    const initialCoordinates = await refPath.getPointCoordinates()

    await points.first().hover()
    await page.mouse.down()
    await page.mouse.move(700, 200)
    await page.mouse.up()
    await page.waitForTimeout(1000)

    const newPosition = await points.first().boundingBox()
    expect(initialPosition).not.toEqual(newPosition)
    expect(initialCoordinates).not.toEqual(await refPath.getPointCoordinates())

    expect(await refPath.getDistanceNumber()).toBe(0)
    await expect(points).toHaveCount(1)
    expect(await refPath.getLignesNumber()).toBe(0)

    // disable edit mode
    await refPath.clickEdit()

    const coordinates = await refPath.getPointCoordinates()

    await points.first().hover()
    await page.mouse.down()
    await page.mouse.move(700, 300)
    await page.mouse.up()

    expect(coordinates).toEqual(await refPath.getPointCoordinates())
  })

  test('creating 2 points and moving one', async () => {
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    await refPath.clickEdit()
    await page.mouse.click(600, 300)
    await page.mouse.click(600, 400)

    const initialPosition = await points.first().boundingBox()
    const initialCoordinates = await refPath.getPointCoordinates()

    await points.first().hover()
    await page.mouse.down()
    await page.mouse.move(700, 200)
    await page.mouse.up()

    await page.waitForTimeout(1000)

    const newPosition = await points.first().boundingBox()
    expect(initialPosition).not.toEqual(newPosition)
    expect(initialCoordinates).not.toEqual(await refPath.getPointCoordinates())

    expect(await refPath.getDistanceNumber()).toBe(1)
    await expect(points).toHaveCount(2)
    expect(await refPath.getLignesNumber()).toBe(1)
  })

  test('creating 2 points and deleting it with remove button', async () => {
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    await refPath.clickEdit()
    await page.mouse.click(600, 300)
    await page.mouse.click(600, 400)

    await page.waitForTimeout(1000)

    expect(await refPath.getDistanceNumber()).toBe(1)
    await expect(points).toHaveCount(2)
    expect(await refPath.getLignesNumber()).toBe(1)

    await refPath.clickRemove(1000)

    expect(await refPath.getDistanceNumber()).toBe(0)
    await expect(points).toHaveCount(0)
    expect(await refPath.getLignesNumber()).toBe(0)
  })

  test('creating 2 points and deleting it by clicking on it', async () => {
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    await refPath.clickEdit()
    await page.mouse.click(600, 300)
    await page.mouse.click(600, 400)

    await page.waitForTimeout(1000)

    expect(await refPath.getDistanceNumber()).toBe(1)
    await expect(points).toHaveCount(2)
    expect(await refPath.getLignesNumber()).toBe(1)

    await page.mouse.click(600, 300)
    await page.waitForTimeout(1000)

    expect(await refPath.getDistanceNumber()).toBe(0)
    await expect(points).toHaveCount(1)
    expect(await refPath.getLignesNumber()).toBe(0)

    await page.mouse.click(600, 400)
    await page.waitForTimeout(1000)

    expect(await refPath.getDistanceNumber()).toBe(0)
    await expect(points).toHaveCount(0)
    expect(await refPath.getLignesNumber()).toBe(0)
  })

  test('creating 2 points and check visibility', async () => {
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    await refPath.clickEdit()
    await page.mouse.click(600, 300)
    await page.mouse.click(600, 400)

    await page.waitForTimeout(1000)

    expect(await refPath.getDistanceNumber()).toBe(1)
    await expect(points).toHaveCount(2)
    expect(await refPath.getLignesNumber()).toBe(1)

    await test.step('ref Path visibility', async () => {
      await refPath.clickVisibility()

      expect(await refPath.getDistanceNumber()).toBe(0)
      await expect(points).toHaveCount(0)
      expect(await refPath.getLignesNumber()).toBe(0)

      await refPath.clickVisibility()

      expect(await refPath.getDistanceNumber()).toBe(1)
      await expect(points).toHaveCount(2)
      expect(await refPath.getLignesNumber()).toBe(1)
    })

    await test.step('ref Path visibility with report', async () => {
      await reportBar.switchReportVisibility(0)

      expect(await refPath.getDistanceNumber()).toBe(0)
      await expect(points).toHaveCount(0)
      expect(await refPath.getLignesNumber()).toBe(0)

      await reportBar.switchReportVisibility(0)

      expect(await refPath.getDistanceNumber()).toBe(1)
      await expect(points).toHaveCount(2)
      expect(await refPath.getLignesNumber()).toBe(1)
    })

    await test.step('ref Path visibility with project', async () => {
      await projectBar.addProject(
        `${_dirname}/files/monoReport/fromScratch2.mpvz`,
      )

      expect(await refPath.getDistanceNumber()).toBe(0)
      await expect(points).toHaveCount(0)

      await projectBar.changeProject(0, 5000)

      expect(await refPath.getDistanceNumber()).toBe(1)
      await expect(points).toHaveCount(2)
      expect(await refPath.getLignesNumber()).toBe(1)
    })
  })
})

test.describe('ReferencePath global test', () => {
  test('Check interface', async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    await homePage.goto()

    await homePage.uploadFile(
      `${_dirname}/files/monoReport/fromScratch.mpvz`,
      0,
    )
    await homePage.zoomOnCurrentReport(10000)
    await homePage.checkMenus()

    const refPath = new ReferencePath(page)

    await expect(refPath.buttonEdit).toBeVisible()
    await expect(refPath.buttonVisibility).toBeVisible()
    await expect(refPath.buttonVisibility).toBeDisabled()
    await expect(refPath.buttonRemove).toBeVisible()
    await expect(refPath.buttonRemove).toBeDisabled()

    await refPath.clickEdit()
    await page.waitForTimeout(1000)

    await expect(refPath.buttonEdit).toHaveClass(/text-green-900/)
    await expect(refPath.buttonEdit).toHaveClass(/bg-green-200/)

    await page.mouse.click(600, 300)
    await page.waitForTimeout(1000)

    await expect(refPath.buttonRemove).toBeEnabled()
    await expect(refPath.buttonVisibility).toBeEnabled()

    await page.mouse.click(600, 300)

    await refPath.clickEdit()
    await expect(refPath.buttonEdit).toHaveClass(/text-black/)
    await expect(refPath.buttonEdit).toHaveClass(/bg-black/)

    await refPath.clickEdit()
    await page.mouse.click(600, 300)

    await refPath.clickRemove()

    await expect(refPath.buttonRemove).toBeDisabled()
    await expect(refPath.buttonVisibility).toBeDisabled()
    expect(await refPath.getDistanceNumber()).toBe(0)
  })

  test('check hidden saved point', async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    await homePage.goto()

    await homePage.uploadFile(`${_dirname}/files/monoReport/hiddenPath.mpvz`, 0)

    const refPath = new ReferencePath(page)
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    expect(await refPath.getDistanceNumber()).toBe(0)
    expect(await refPath.getLignesNumber()).toBe(0)
    await expect(points).toHaveCount(0)
  })

  test.skip('check visibility mono report from file', async ({
    page,
    baseURL,
  }) => {
    if (!baseURL) return

    // launch project
    const homePage = new HomePage(page, baseURL)
    const reportBar = new ReportBar(page)
    const refPath = new ReferencePath(page)
    const projectBar = new ProjectBar(page)

    await homePage.goto()

    await homePage.uploadFile(
      `${_dirname}/files/monoReport/fromPath.mpvz`,
      10000,
    )

    // check that the point is created
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    await expect(points).toHaveCount(7)
    expect(await refPath.getLignesNumber()).toBe(6)
    expect(await refPath.getDistanceNumber()).toBe(1)

    // hide the ref path
    await refPath.clickVisibility()

    // check that the point is hidden
    await expect(points).toHaveCount(0)
    expect(await refPath.getLignesNumber()).toBe(0)
    expect(await refPath.getDistanceNumber()).toBe(0)

    // show the ref path again
    await refPath.clickVisibility()
    await page.waitForTimeout(1000)

    // check that the point is visible
    await expect(points).toHaveCount(7)
    expect(await refPath.getLignesNumber()).toBe(6)
    expect(await refPath.getDistanceNumber()).toBe(1)

    // change the report visibility
    // the modal button give the testid to the svg in it so we need to be more precise
    await reportBar.switchReportVisibility(0)

    // check that the point is visible
    await expect(points).toHaveCount(0)
    expect(await refPath.getLignesNumber()).toBe(0)
    expect(await refPath.getDistanceNumber()).toBe(0)
    await page.waitForTimeout(1000)

    await reportBar.switchReportVisibility(0)

    // check that the point is visible
    await expect(points).toHaveCount(7)
    expect(await refPath.getLignesNumber()).toBe(6)
    expect(await refPath.getDistanceNumber()).toBe(1)

    // change the project

    await projectBar.addProject(
      `${_dirname}/files/monoReport/fromScratch2.mpvz`,
    )

    // check that the point is hidden
    await expect(points).toHaveCount(0)
    expect(await refPath.getLignesNumber()).toBe(0)
    expect(await refPath.getDistanceNumber()).toBe(0)

    // restore the project
    await projectBar.changeProject(0)

    // check that the point is visible
    await expect(points).toHaveCount(7)
    expect(await refPath.getLignesNumber()).toBe(6)
    expect(await refPath.getDistanceNumber()).toBe(1)
  })

  test.skip('check visibility multi report from file', async ({
    page,
    baseURL,
  }) => {
    if (!baseURL) return

    // launch project
    const homePage = new HomePage(page, baseURL)
    const reportBar = new ReportBar(page)
    const refPath = new ReferencePath(page)
    await homePage.goto()

    await page.waitForTimeout(2000)

    await homePage.uploadFile(`${_dirname}/files/multiReport/fromPath.mpvz`)

    await page.waitForTimeout(10000)

    // check that the point is created
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    await expect(points).toHaveCount(10)
    expect(await refPath.getLignesNumber()).toBe(8)
    expect(await refPath.getDistanceNumber()).toBe(2)

    await reportBar.switchReportVisibility(0)

    // check that the point is hidden
    await expect(points).toHaveCount(5)
    expect(await refPath.getLignesNumber()).toBe(4)
    expect(await refPath.getDistanceNumber()).toBe(1)

    await reportBar.switchReportVisibility(1)

    // check that the point is hidden
    await expect(points).toHaveCount(0)
    expect(await refPath.getLignesNumber()).toBe(0)
    expect(await refPath.getDistanceNumber()).toBe(0)

    await reportBar.switchReportVisibility(0)

    // check that the point is visible
    await expect(points).toHaveCount(5)
    expect(await refPath.getLignesNumber()).toBe(4)
    expect(await refPath.getDistanceNumber()).toBe(1)
  })

  test('check line init', async ({ page, baseURL }) => {
    if (!baseURL) return

    const homePage = new HomePage(page, baseURL)
    await homePage.goto()

    await homePage.uploadFile(
      `${_dirname}/files/multiReport/Figari_2024__1_-3.mpvz`,
      5000,
    )

    const refPath = new ReferencePath(page)
    const points = page.locator('[data-testid^="ref-path-icon-"]')

    expect(await refPath.getLignesNumber()).toBe(3)
    expect(await refPath.getDistanceNumber()).toBe(1)
    await expect(points).toHaveCount(4)

    await page.waitForTimeout(1000)
  })
})
