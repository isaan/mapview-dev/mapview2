import { readFileSync } from 'fs'
import { expect } from '@playwright/test'

export class HomePage {
  readonly page: Page

  readonly baseUrl: string

  readonly loader: Locator

  readonly demoButton: Locator

  readonly dragAndDrop: Locator

  readonly inputButton: Locator

  constructor(page: Page, baseUrl: string) {
    this.page = page
    this.baseUrl = baseUrl
    this.loader = page.locator('#loader')
    this.demoButton = page.getByRole('button', { name: 'Try demo' })
    this.dragAndDrop = page.locator('#drag-drop')
    this.inputButton = page.locator('#file-input')
  }

  async goto() {
    await this.page.goto(this.baseUrl)
    await this.checkHomePage()
    await this.page.waitForTimeout(2000)
  }

  async launch(timeout = 2000) {
    await this.demoButton.click()
    await expect(this.loader).toBeVisible()
    await expect(this.loader).toHaveCount(0, { timeout: 40000 })
    await this.page.waitForTimeout(timeout)
  }

  async uploadFile(path: string, timeout = 2000) {
    await this.inputButton.setInputFiles(path)
    await expect(this.loader).toBeVisible()
    await expect(this.loader).toHaveCount(0, { timeout: 40000 })
    await this.page.waitForTimeout(timeout)
  }

  async dragAndDropFile(filePath: string, selector: string, timeout = 2000) {
    const buffer = readFileSync(filePath).toString('base64')

    const dataTransfer = this.page.evaluateHandle(
      async ({ localBuffer, localFileName }) => {
        const dt = new DataTransfer()

        const blobData = await fetch(localBuffer).then((res) => res.blob())

        dt.items.add(new File([blobData], localFileName))

        return dt
      },
      {
        localBuffer: `data:application/octet-stream;base64,${buffer}`,
        localFileName: filePath.split('/').pop() as string,
      },
    )

    await this.page
      .locator(selector)
      .dispatchEvent('drop', { dataTransfer: await dataTransfer })

    await this.page.waitForTimeout(timeout)
  }

  async checkHomePage() {
    await expect(this.demoButton, {
      message: 'check demo button visibility',
    }).toBeVisible()

    await expect(this.dragAndDrop, {
      message: 'check drag and drop visibility',
    }).toBeVisible()
  }

  async checkMenus() {
    await expect(this.page.locator('#desktop-menu-project')).toBeVisible({
      visible: true,
    })

    await expect(this.page.locator('#desktop-menu-map')).toBeVisible({
      visible: true,
    })

    await expect(this.page.locator('#desktop-menu-reports')).toBeVisible({
      visible: true,
    })

    await expect(this.page.locator('#desktop-menu-colors')).toBeVisible({
      visible: true,
    })

    await expect(this.page.locator('#desktop-menu-data')).toBeVisible({
      visible: true,
    })
  }

  async checkErrorModal(text?: string) {
    await expect(this.page.locator('#import-error-dialog')).toBeVisible()

    if (text) {
      await expect(
        this.page.locator('#import-error-dialog [data-testid="error-message"]'),
      ).toHaveText(text)
    }
  }

  async zoomAndClickOnPoint(index: number) {
    const zoomButton = this.page.locator(`#btn-zoom-point-${index}`)

    await expect(zoomButton).toBeVisible()
    await zoomButton.click()

    await this.page.waitForTimeout(3000)

    const mapPoint = this.page.locator(`#icon-point-${index}`).first()
    await expect(mapPoint).toBeVisible()

    await mapPoint.click()
  }

  async zoomOnCurrentReport(timeout = 2000) {
    await this.page.locator('#fly-to-report').click()
    await this.page.waitForTimeout(timeout)
  }
}
