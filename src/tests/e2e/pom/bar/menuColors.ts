export class MenuColors {
  readonly page: Page

  zoneDialogBtn: Locator

  zoneDialog: Locator

  constructor(page: Page) {
    this.page = page
    this.zoneDialogBtn = page
      .locator('[data-testid="zone-configuration-btn"]')
      .and(page.locator('button'))
    this.zoneDialog = page.locator('[data-testid="zone-configuration-dialog"]')
  }

  async setZoneColor(zoneIndex: number, colorIndex: number, timeout = 10) {
    await this.zoneDialogBtn.click()
    await this.zoneDialog.locator(`[data-testid="color-${colorIndex}"]`).click()
    await this.page.waitForTimeout(timeout)
  }

  async deleteZone(zoneIndex: number, timeout = 10) {
    if (zoneIndex === 0) return

    await this.zoneDialogBtn.click()
    await this.page.waitForTimeout(1000)
    await this.zoneDialog
      .locator(`[data-testid="delete-zone-${zoneIndex}-btn"]`)
      .and(this.page.locator('button'))
      .click()
    await this.page.waitForTimeout(timeout)
  }
}
