export class DataBar {
  readonly page: Page

  readonly selectGroupBy: Locator

  readonly tablePoints: Locator

  constructor(page: Page) {
    this.page = page
    this.selectGroupBy = page
      .locator('[data-testid="select-group-by-btn"]')
      .and(page.locator('button'))

    this.tablePoints = page.locator('[data-testid="table-points"]')
  }

  async setGroupBy(index: number, timeout = 2000) {
    await this.selectGroupBy.click()
    await this.page.locator(`#select-group-by-options-${index}`).click()
    await this.page.waitForTimeout(timeout)
  }

  getLine(zoneIndex: number, lineIndex: number) {
    return this.page.locator(
      `[data-testid="zone-${zoneIndex}-tr-${lineIndex}"]`,
    )
  }

  getDataLines() {
    return this.tablePoints.locator('tbody tr')
  }

  getAllTable() {
    return this.tablePoints.locator('table')
  }
}
