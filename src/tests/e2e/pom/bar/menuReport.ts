export class ReportBar {
  readonly page: Page

  readonly selectReport: Locator

  constructor(page: Page) {
    this.page = page
    this.selectReport = page
      .locator('[data-testid="selected-report-btn"]')
      .first()
  }

  async changeReport(index: number, timeout = 1000) {
    await this.selectReport.click()
    await this.page.waitForTimeout(timeout)
    const buttonReport = this.page
      .locator(`[data-testid^="button-report-${index}"]`)
      .first()
    await buttonReport.click()
    await this.selectReport.click()
  }

  async switchReportVisibility(index: number, timeout = 1000) {
    await this.selectReport.click()
    await this.page.waitForTimeout(timeout)
    const buttonReportVisibility = this.page
      .locator(`[data-testid^="button-report-visibility-${index}"]`)
      .first()
    await buttonReportVisibility.click()
    await this.selectReport.click()
    await this.page.waitForTimeout(timeout)
  }
}
