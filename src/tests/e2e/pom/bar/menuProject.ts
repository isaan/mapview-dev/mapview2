export class ProjectBar {
  readonly page: Page

  readonly fileInput: Locator

  constructor(page: Page) {
    this.page = page
    this.fileInput = page
      .locator('[data-testid="project-file-input"]')
      .and(this.page.locator('input'))
  }

  async addProject(path: string, timeout = 5000) {
    await this.fileInput.setInputFiles(path)
    await this.page.waitForTimeout(timeout)
  }

  async changeProject(index: number, timeout = 5000) {
    await this.page
      .locator('[data-testid="select-project-btn"]')
      .and(this.page.locator('button'))
      .click()
    await this.page.waitForTimeout(2000)
    await this.page
      .locator(`[data-testid="select-project-options-${index}"]`)
      .and(this.page.locator('button'))
      .click()
    await this.page.waitForTimeout(timeout)
  }
}
