import { expect } from '@playwright/test'

export class DialogConfiguration {
  readonly page: Page

  readonly dialogConfigurationButton: Locator

  readonly dialogConfiguration: Locator

  constructor(page: Page) {
    this.page = page
    this.dialogConfigurationButton = page.locator(
      '#dialog-configurations-button',
    )
    this.dialogConfiguration = page.locator('#dialog-configurations')
  }

  async launch() {
    await this.dialogConfigurationButton.click()
  }

  async checkDialogConfigurationButton() {
    await expect(this.dialogConfigurationButton, {
      message: 'check dialogConfigurations button visibility',
    }).toBeVisible()
  }

  async checkDialogConfiguration() {
    await expect(this.dialogConfiguration, {
      message: 'check dialogConfigurations visibility',
    }).toBeVisible()
  }
}
