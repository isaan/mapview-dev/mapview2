import { expect } from '@playwright/test'

export class DialogInformation {
  readonly page: Page

  readonly dialogInformationButton: Locator

  readonly modalInformations: Locator

  constructor(page: Page) {
    this.page = page
    this.dialogInformationButton = page.locator(
      '#dialog-project-informations-button',
    )
    this.modalInformations = page.locator('#dialog-project-informations')
  }

  async launch() {
    await this.dialogInformationButton.click()
  }

  async checkDialogInformationButton() {
    await expect(this.dialogInformationButton, {
      message: 'check dialogInformation button visibility',
    }).toBeVisible()
  }

  async checkDialogInformation() {
    await expect(this.modalInformations, {
      message: 'check dialogInformation visibility',
    }).toBeVisible()
  }
}
