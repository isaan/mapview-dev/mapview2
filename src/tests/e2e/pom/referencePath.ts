export class ReferencePath {
  readonly page: Page

  readonly buttonEdit: Locator

  readonly buttonVisibility: Locator

  readonly buttonRemove: Locator

  constructor(page: Page) {
    this.page = page
    this.buttonEdit = page.locator('#button-reference-path-edit')
    this.buttonVisibility = page.locator('#button-reference-path-visibility')
    this.buttonRemove = page.locator('#reference-path-remove-button')
  }

  async clickEdit(timeout = 1000) {
    await this.buttonEdit.click()
    await this.page.waitForTimeout(timeout)
  }

  async clickVisibility() {
    await this.buttonVisibility.click()
    await this.page.waitForTimeout(1000)
  }

  async clickRemove(timeout = 1000) {
    await this.buttonRemove.click()
    await this.page.waitForTimeout(1000)
    await this.page.locator('#delete-button').click()
    await this.page.waitForTimeout(timeout)
  }

  async getLignesNumber() {
    return this.page.evaluate(() => {
      // @ts-expect-error mapboxgl is not defined in the global scope
      return window.getUniqueFeatures(
        // @ts-expect-error mapboxgl is not defined in the global scope
        window.map?.queryRenderedFeatures({ layers: ['reference-path'] }),
        'id',
      ).length
    })
  }

  async getDistanceNumber() {
    return this.page.evaluate(() => {
      // @ts-expect-error mapboxgl is not defined in the global scope
      return window.getUniqueFeatures(
        // @ts-expect-error mapboxgl is not defined in the global scope
        window.map?.queryRenderedFeatures({
          layers: ['reference-path-distance'],
        }),
        'id',
      ).length
    })
  }

  async getPointCoordinates(index = 0) {
    return this.page.evaluate((i) => {
      // @ts-expect-error points are not defined in the global scope
      return window.getPoints()[i].coordinates()
    }, index)
  }
}
