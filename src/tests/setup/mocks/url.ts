import { vi } from 'vitest'

const urlMock = {
  createObjectURL: vi.fn(),
}

vi.stubGlobal('URL', urlMock)
vi.stubGlobal('navigator', { language: 'en-US' })
