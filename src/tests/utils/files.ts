import { readdirSync, readFileSync, statSync } from 'fs'

import { getExtensionFromPath } from '/src/scripts/utils/file'

interface CompareFilesOptions {
  filter?: string
  parser?: (elm: string) => string
}

const getContentOfDir = (path: string) => {
  const dirStats = statSync(path)
  if (dirStats.isDirectory()) {
    const subContent = readdirSync(path)
    return subContent.map((s) => `${path}/${s}`)
  }

  return []
}

export const getFilesPathFromDir = (
  path: string,
  options: {
    recursive?: boolean
    authorizedExtension?: string[] | '*'
  } = { authorizedExtension: '*' },
) => {
  const paths: string[] = getContentOfDir(path)
  const filesPath: string[] = []

  while (paths.length > 0) {
    const currentPath = paths[0]
    const stats = statSync(currentPath)

    if (stats.isDirectory() && options?.recursive) {
      paths.push(...getContentOfDir(currentPath))
    } else if (stats.isFile()) {
      const extension = getExtensionFromPath(currentPath)

      if (
        !currentPath.split('/').pop()?.startsWith('.') &&
        (options?.authorizedExtension === '*' ||
          options?.authorizedExtension?.includes(extension))
      ) {
        filesPath.push(currentPath)
      }
    }
    paths.shift()
  }
  return filesPath
}

export const getFilesFromDir = (
  path: string,
  options?: {
    recursive?: boolean
    authorizedExtension?: string[]
  },
) => {
  const paths = getFilesPathFromDir(path, options)

  return paths.map((currentPath) => {
    const file = getFileFromPath(currentPath)

    return {
      file,
      path: currentPath,
    }
  })
}

export const getFileFromPath = (path: string) => {
  const buffer = readFileSync(path)

  return new File([buffer], path.split('/').pop() as string)
}

const filesToIgnore = ['.DS_Store']

export const compareFiles = (
  actual: Unzipped,
  expected: Unzipped,
  options?: CompareFilesOptions,
) => {
  let actualKeys = Object.keys(actual)
    .filter((key) => !filesToIgnore.some((file) => key.endsWith(file)))
    .sort((a, b) => a.localeCompare(b))
  let expectedKeys = Object.keys(expected)
    .filter((key) => !filesToIgnore.some((file) => key.endsWith(file)))
    .sort((a, b) => a.localeCompare(b))

  if (options?.filter) {
    const optionsFilter = options.filter
    actualKeys = actualKeys.filter((key) => key.includes(optionsFilter))
    expectedKeys = expectedKeys.filter((key) => key.includes(optionsFilter))
  }

  if (actualKeys.length !== expectedKeys.length) {
    return {
      isSameLength: false,
      actualKeys,
      expectedKeys,
      actualLength: actualKeys.length,
      expectedLength: expectedKeys.length,
    }
  }

  let haveSameFile = true
  let haveSameContent = true
  let lastKey = ''

  for (let i = 0; i < actualKeys.length && haveSameContent; i++) {
    const parser = options?.parser
    const parsedActualKeys = parser ? actualKeys.map(parser) : actualKeys

    const parsedExpectedKeys = parser ? expectedKeys.map(parser) : expectedKeys

    const parsedKey = parsedActualKeys[i]

    lastKey = parsedKey

    haveSameFile = parsedExpectedKeys.includes(parsedKey)

    haveSameContent =
      haveSameFile &&
      actual[actualKeys[i]].toString() === expected[expectedKeys[i]].toString()
  }

  return {
    haveSameFile,
    haveSameContent,
    lastKey,
    actualKeys,
    expectedKeys,
    isSameLength: true,
  }
}
