#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -lt 1 ]; then
  echo "Usage: $0 <directory>"
  exit 1
fi

import=false

while getopts ":i" opt; do
  case ${opt} in
    i)
      echo "switch to import mode"
      import=true
      shift
      ;;
  esac
done

# Assign arguments to variables
directory=$1
BASEDIR=$(dirname "$0")

# Function to recursively search and execute command on each file
process_files() {
  local dir=$1
  for file in "$dir"/*; 
  do
    local onlyFile=true

    if [ -d "$file" ]; then
      onlyFile=false
      process_files "$file"  # Recursively process subdirectories
      if [ $? -ne 0 ]; then
        return 1  # Stop if an error occurred in a subdirectory
      fi
    fi

    if $onlyFile; then
      local importArg=''
      if $import; then
        importArg='--import'
      fi

      echo "run for $dir with \"$importArg\" argument"
      tsx ${BASEDIR}/updater.ts "$dir" $importArg  # Execute the command on the file

      if [ $? -ne 0 ]; then
        echo "Error executing command on $file"
        return 1  # Stop if an error occurred
      fi
    fi
  done
}

# Start processing from the given directory
process_files "$directory"
if [ $? -ne 0 ]; then
  echo "Processing stopped due to an error."
  exit 1
fi