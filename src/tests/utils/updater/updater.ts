/* eslint-disable no-console */
import { readdirSync, readFileSync, statSync, writeFileSync } from 'fs'
import path from 'path'
import { fileURLToPath } from 'url'
import { GlobalRegistrator } from '@happy-dom/global-registrator'
import chalk from 'chalk'
import { program } from 'commander'
import {
  heavydynDynatestExporter,
  heavydynF25Exporter,
  heavydynLegacyTXTExporter,
  heavydynPDXExporter,
  heavydynSwecoExporter,
  importFile,
  loadProjectData,
  mpvzExporter,
  mvrzExporter,
  PREVIOUS_KEY,
  sleep,
  store,
} from 'lib-dist/main'

const _filename = fileURLToPath(import.meta.url)
const _dirname = path.dirname(_filename)

const io = `${_dirname}/../../../scripts/io`

const defaultExportFolder = `${io}/exporter/tests/files`
const defaultImportFolder = `${io}/importer/tests/files`

const acceptedExtension = [
  'pdx',
  'f25',
  'fwd',
  'mpvz',
  'mvrz',
  'prjz',
  'dynz',
  'txt',
]
const importProjectExtension = ['prjz', 'dynz']
const exportProjectExtension = ['mpvz']

program
  .name('updater')
  .description('CLI to update reference file of the CI')
  .version('0.0.1')
  .arguments('[path]')
  .option('-f --file', 'update only the file give as path')
  .option('-i --import', 'update reference mpvz of the default import folder')
  .option(
    '--ignore <name>',
    'folder to ignore',
    (value: string, previous: string[]) => previous.concat([value]),
  )
  .parse()

GlobalRegistrator.register()

const opts = program.opts()

const projects: Map<string, HeavydynProject> = new Map()

const loadProject = async (subPath: string, name: string) => {
  const buffer = readFileSync(subPath)

  const projectFile = new File([buffer], name)

  const project = (await importFile(projectFile)) as HeavydynProject

  await loadProjectData(projectFile, project)

  projects.set(subPath, project)

  return project
}

const getExtension = (currentPath: string) =>
  String(path.extname(currentPath)).toLowerCase().replace('.', '')

 
const updateRefFile = async (fileToUpdatePath: string) => {
  const name = path.basename(fileToUpdatePath)
  const extension = getExtension(fileToUpdatePath)

  if (
    extension === 'mpvz' &&
    [opts.ignore]
      .concat([PREVIOUS_KEY])
      .some(
        (folder: string) =>
          folder && name.toUpperCase().includes(folder.toUpperCase()),
      )
  ) {
    console.log(chalk.blue(`Ignore mpvz: ${chalk.yellow(fileToUpdatePath)}`))
    return
  }

  const dirPath = path.dirname(fileToUpdatePath)

  const directoryContent = readdirSync(dirPath)

  const hasPreviousVersion = directoryContent.find((fileName) =>
    fileName.toLocaleLowerCase().includes(PREVIOUS_KEY.toLocaleLowerCase()),
  )

  const projectFileName = directoryContent.find((fileName) => {
    const lowerExt = path.extname(fileName).toLowerCase()

    return opts.import
      ? lowerExt.includes('prjz') || lowerExt.includes('dynz')
      : lowerExt.includes('mpvz') &&
          (!hasPreviousVersion ||
            (hasPreviousVersion &&
              fileName
                .toLocaleLowerCase()
                .includes(PREVIOUS_KEY.toLocaleLowerCase())))
  })

  const projectPath = `${dirPath}/${projectFileName}`
  const projectStats = statSync(projectPath)

  if (!projectFileName || !projectStats.isFile()) {
    console.error(
      chalk.red(
        `no ${opts.import ? 'prjz' : 'mpvz'} file found can't build a project`,
      ),
    )
    return
  }

  console.groupCollapsed()
  const stylizedPath = chalk.yellow(`\`${fileToUpdatePath}\``)
  console.log(`preparing to update ${stylizedPath} file`)

  let project = projects.get(projectPath)

  if (!project) {
    console.log('Loading reference project')
    project = await loadProject(projectPath, projectFileName)
  } else {
    console.log('reference project loaded')
  }

  let exportedFile

  console.log('set store')
  store.pushAndSelectProject(project)
  console.log('store set')

  await sleep(5000)

  console.log(`exporting \`${chalk.yellow(extension)}\` file`)
  switch (extension) {
    case 'f25':
      exportedFile = heavydynF25Exporter.export(project)
      break
    case 'fwd':
      if (name.includes('sweco')) {
        exportedFile = heavydynSwecoExporter.export(project)
      } else if (name.includes('dynatest')) {
        exportedFile = heavydynDynatestExporter.export(project)
      }
      break
    case 'pdx':
      exportedFile = heavydynPDXExporter.export(project)
      break
    case 'mvrz':
      exportedFile = await mvrzExporter.export(project)
      break
    case 'mpvz':
      exportedFile = await mpvzExporter.export(project)
      break
    case 'txt':
      if (name.includes('rawdata')) {
        exportedFile = heavydynLegacyTXTExporter.export(project, true)
      } else {
        exportedFile = heavydynLegacyTXTExporter.export(project)
      }
      break
  }

  if (!exportedFile) {
    console.error(chalk.red(`File extension not managed ${extension}`))
    return
  }

  writeFileSync(
    fileToUpdatePath,
    new DataView(await exportedFile.arrayBuffer()),
  )

  console.log(`Update of ${stylizedPath}  done`)
  console.groupEnd()
}

const updateRefFolder = async (
  directories: string[] = [defaultExportFolder],
) => {
  if (
    !directories.every((dir) => {
      const stats = statSync(dir)

      return stats.isDirectory()
    })
  ) {
    console.error(chalk.red("Some path aren't folder"))
    return
  }

  console.group()
  console.log('Loading files to update...')

  const dirs: string[] = [...directories]
  const files: string[] = []
  const projectFiles: string[] = []
  let index = 0

  while (index < dirs.length) {
    const dir = dirs[index]
    const subElement = readdirSync(dir)

    subElement.forEach((elt) => {
      const eltPath = `${dir}/${elt}`
      const stats = statSync(eltPath)
      const ext = getExtension(eltPath)

      const isImportProjectExt = importProjectExtension.includes(ext)
      const isExportProjectExt = exportProjectExtension.includes(ext)

      if (stats.isDirectory()) {
        dirs.push(eltPath)
      } else if (
        (stats.isFile() && acceptedExtension.includes(ext)) ||
        isImportProjectExt ||
        isExportProjectExt
      ) {
        if (ext !== 'prjz' && ext !== 'dynz') files.push(eltPath) // prjz and dynz can't be updated

        if (
          (opts.import && isImportProjectExt) ||
          (!opts.import && isExportProjectExt)
        ) {
          projectFiles.push(eltPath)
        }
      }
    })

    index += 1
  }

  console.log(`${files.length} files found`)

  const promises: Promise<HeavydynProject>[] = []

  projectFiles.forEach((projectFile) => {
    promises.push(loadProject(projectFile, path.basename(projectFile)))
  })

  console.log('Loading references projects...')

  await Promise.all(promises)

  console.log('references projects loaded')
  console.groupEnd()

  // eslint-disable-next-line no-restricted-syntax
  for (const element of files) {
    console.log('\n')
    // eslint-disable-next-line no-await-in-loop
    await updateRefFile(element)
  }
}

const args = program.args

if (opts.file) {
  void updateRefFile(args[0])
} else {
  let folder = args[0]

  if (!folder) {
    folder = opts.import ? defaultImportFolder : defaultExportFolder
  }

  await updateRefFolder([folder])
}
