import { createReadStream } from 'fs'
import { finished } from 'stream/promises'
import { parse } from 'csv-parse'

export const mathUnitsCSVHeaders = [
  'value',
  'mathUnit',
  'unitValue',
  'minValue',
  'maxValue',
  'locale',
  'precision',
  'unit',
  'format',
  'appendUnitToString',
  'disablePreString',
  'removeSpaces',
  'disableMinAndMax',
  'expectedStringResult',
  'capValue',
  'getAverage',
  'currentToBase',
  'baseToCurrent',
  'checkValidity',
]

export const loadCSV = async <T>(path: string, header: string[]) => {
  const csvData: T[] = []

  const parser = createReadStream(path, { encoding: 'utf-8' }).pipe(
    parse({
      delimiter: ';',
      columns: header,
    }),
  )
  parser.on('readable', () => {
    let rec: T
    let firstLine = true
    // eslint-disable-next-line no-cond-assign
    while ((rec = parser.read())) {
      if (!firstLine) csvData.push(rec)
      firstLine = false
    }
  })
  await finished(parser)
  return csvData
}
