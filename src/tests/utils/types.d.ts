interface KeyValueData {
  key: string
  value: string
}

interface FileTransformerOpt {
  removeBlankLine?: boolean
  dataType?: transformerDataType
}

interface ParsedDate {
  date: Date
  origin: string
}

interface BaseTestFileData {
  file: File
  path: string
}

interface MathUnitCSVData {
  value: string
  mathUnit: string
  unitValue: string
  minValue?: number
  maxValue?: number
  locale: string
  precision: number
  unit: string
  format: PossibleFormats
  appendUnitToString?: true
  disablePreString?: true
  removeSpaces?: true
  disableMinAndMax?: true
  expectedStringResult: string
  capValue?: string
  getAverage: string
  currentToBase: string
  baseToCurrent: string
  checkValidity: string
}

interface MathUnitData extends Omit<MathUnitCSVData, 'value'> {
  value: number
}

type CheckDataConformityMessage =
  | 'dataType differ'
  | 'invalid Number'
  | 'invalid Date'
  | 'invalid date format'
  | 'array data invalid'
  | 'valid data'
  | 'object data differ'
  | 'object data invalid'
  | 'invalid data'
