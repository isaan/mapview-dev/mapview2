/* @refresh reload */
import { flatten, translator, type Translator } from '@solid-primitives/i18n'
import { MetaProvider } from '@solidjs/meta'
// eslint-disable-next-line import-x/no-unresolved
import routes from '~solid-pages'
import { render } from 'solid-js/web'

import 'solid-devtools'
import './styles/main.css'
import 'mapbox-gl/dist/mapbox-gl.css'

import { dictionaries, getBrowserLocale } from './locales'

const root = document.getElementById('root')

interface AppState {
  t: Translator<RecordAny>
}

const AppContext = createContext<AppState>({} as AppState)

export const useAppState = () => useContext(AppContext)

if (root) {
  render(() => {
    const t = translator(() => {
      let lang = getBrowserLocale(true) ?? ''
      if (lang !== 'fr' && lang !== 'en') lang = 'en'
      return (
        flatten((dictionaries as RecordAny)[lang]) || flatten(dictionaries.en)
      )
    })

    const state: AppState = {
      t,
    }

    return (
      <AppContext.Provider value={state}>
        <MetaProvider>
          <Router>{routes}</Router>
        </MetaProvider>
      </AppContext.Provider>
    )
  }, root)
}
