export const env = {
  isDev: import.meta.env.DEV,
  isProd:
    import.meta.env.VITE_CONTEXT === 'production' &&
    import.meta.env.VITE_BRANCH === 'prod',
  isTest: import.meta.env.MODE === 'test',
  isHTTPS:
    typeof window === 'undefined'
      ? false
      : window.location.protocol === 'https:',
  context: import.meta.env.VITE_CONTEXT as string,
}
