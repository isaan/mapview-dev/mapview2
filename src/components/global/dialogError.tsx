import { Button, DialogCore } from '/src/components'
import { useAppState } from '/src/index'
import { createASS } from '/src/scripts'
import { store } from '/src/store'

interface Props {
  id?: string
}

export const DialogError = (props: Props) => {
  const { t } = useAppState()

  let toggleDialog: DialogToggleFunction | undefined

  const errorCode: ASS<string> = createASS('Generic')

  createEffect(
    on(
      () => store.importError(),
      (current, previous) => {
        if (!previous && current) {
          errorCode.set(
            store.availableErrorCode().includes(current) ? current : 'Generic',
          )
          toggleDialog?.(true)
        }
      },
    ),
  )

  return (
    <DialogCore
      closeable
      id={props.id}
      title={t('Error.Title')}
      onToggleCreated={(toggle) => {
        toggleDialog = toggle
      }}
      onClose={() => {
        store.importError.set(undefined)
      }}
      footer={
        <div class="flex justify-between">
          <Button color="green">{t('Ok')}</Button>
        </div>
      }
    >
      <div class="space-y-6" data-testid="error-message">
        {t(`Error.${errorCode()}`)}
      </div>
    </DialogCore>
  )
}
