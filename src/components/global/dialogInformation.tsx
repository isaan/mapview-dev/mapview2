import {
  Button,
  classPropToString,
  Dialog,
  Input,
  InputDataList,
  Label,
} from '/src/components'
import { useAppState } from '/src/index'

interface Props {
  readonly id?: string
  readonly bulks: {
    readonly title: string | Element
    readonly fields: Field[]
  }[]
  readonly calibrations?: HeavydynCalibrations | null
  readonly units?: HeavydynMathUnits | null
}

export const DialogInformation = (props: Props) => {
  const { t } = useAppState()

  const [state, setState] = createStore({
    open: false,
    channelsDatas: <></>,
    sensorsDatas: <></>,
  })

  createEffect(
    on(
      [
        () => props.units?.position.currentPrecision(),
        () => props.units?.position.currentUnit(),
        () => props.units?.sensorCoefficient.currentPrecision(),
      ],
      () => {
        const calibration = props.calibrations
        if (calibration) {
          setState(
            'channelsDatas',
            createDataInputs('Channels', calibration.channels),
          )
          createDataInputs('Sensors', calibration.sensors)
        }
      },
    ),
  )

  const pendingChanges = new Map<string, VoidFunction>()

  const attributeToFormat = ['position', 'gain', 'v0']

  const formatAttribut = (name: string, value: number | string) => {
    let valueString
    if (typeof value === 'string') value = parseFloat(value)
    switch (name) {
      case 'position':
        valueString = props.units?.position.valueToLocaleString(value, {
          appendUnitToString: true,
          precision: props.units?.position?.currentPrecision(),
          unit: props.units?.position.currentUnit(),
          format: props.units?.position.format,
        })
        break
      case 'gain':
      case 'v0':
        valueString = props.units?.sensorCoefficient.valueToLocaleString(
          value,
          {
            precision: props.units?.sensorCoefficient?.currentPrecision(),
            format: props.units?.sensorCoefficient.format,
          },
        )
        break
    }

    return valueString
  }

  const createDataInputs = (
    name: string,
    datas: JSONChannel[] | JSONSensor[],
  ) => {
    return (
      <Label label={t(name)} size="base">
        <For each={datas}>
          {(data, index) => (
            <>
              <For each={Object.keys(data)}>
                {(attr) => {
                  if (attr === 'version') {
                    return
                  }
                  const id = createMemo(
                    () =>
                      `input-calibration-${name.toLowerCase()}-${index()}-${attr}`,
                  )
                  return attributeToFormat.includes(attr) ? (
                    <Input
                      full
                      label={t(attr)}
                      readOnly={true}
                      type="string"
                      long={false}
                      value={formatAttribut(attr, data[attr])}
                      id={id()}
                    />
                  ) : (
                    <Input
                      full
                      label={t(attr)}
                      readOnly={true}
                      type="string"
                      long={false}
                      value={data[attr]}
                      id={id()}
                    />
                  )
                }}
              </For>

              <hr
                class={classPropToString([
                  index() + 1 !== datas.length ? '' : 'invisible',
                ])}
              />
            </>
          )}
        </For>
      </Label>
    )
  }

  return (
    <Dialog
      id={props.id}
      title={t('Information')}
      moveable
      button={{
        leftIcon: IconTablerInfoCircle,
        text: t('View information'),
        full: true,
      }}
      footer={
        <div class="flex justify-between">
          <Button color="red" leftIcon={IconTablerEraser}>
            {t('Cancel')}
          </Button>
          <Button
            color="green"
            rightIcon={IconTablerDeviceFloppy}
            onClick={() => {
              ;[...pendingChanges.values()].forEach((setter) => setter())
            }}
          >
            {t('Validate')}
          </Button>
        </div>
      }
      onOpen={() => {
        setState('open', true)
        pendingChanges.clear()
      }}
      onClose={() => setState('open', false)}
    >
      <div class="space-y-8">
        <For each={props.bulks}>
          {(bulk) => (
            <Label label={bulk.title}>
              <For each={bulk.fields}>
                {(field) => {
                  let input = undefined as HTMLInputElement | undefined
                  let value: string | number | boolean
                  let type: string | undefined

                  const fieldValue = field.value()

                  switch (typeof fieldValue) {
                    case 'object':
                      switch (fieldValue.kind) {
                        case 'dateValue':
                          value = fieldValue.value().split('T')[0]
                          type = 'date'
                          break
                        default:
                          value = fieldValue.value()
                          break
                      }

                      break

                    default:
                      value = fieldValue
                      break
                  }

                  const isLongString =
                    typeof fieldValue === 'object' &&
                    fieldValue.kind === 'longString'

                  const isInput =
                    typeof fieldValue === 'object'
                      ? fieldValue.kind === 'dateValue' || isLongString
                      : typeof value === 'string' || typeof value === 'number'

                  const id = createMemo(
                    () => `input-${field.label.toLowerCase()}`,
                  )

                  const readOnly = createMemo(
                    () => type === 'date' || field.settings.readOnly(),
                  )

                  createEffect(() => {
                    if (state.open && input && !readOnly()) {
                      input.value = String(value)
                    }
                  })

                  const onInput = (newValue: string | undefined) => {
                    pendingChanges.set(id(), () => {
                      field.setValue(newValue ?? '')
                      value = newValue ?? ''
                    })
                  }

                  const label =
                    field.label === 'Project' ? 'Project Number' : field.label

                  return (
                    <Switch>
                      <Match when={isInput}>
                        <Input
                          ref={(ref) => {
                            input = ref
                          }}
                          full
                          label={t(label) || label}
                          readOnly={readOnly()}
                          long={isLongString}
                          value={value as string}
                          type="string"
                          id={id()}
                          onInput={onInput}
                        />
                      </Match>
                      <Match
                        when={
                          typeof fieldValue === 'object' &&
                          fieldValue.kind === 'selectableString' &&
                          fieldValue
                        }
                      >
                        {(_fieldValue) => (
                          <InputDataList
                            ref={(ref) => {
                              input = ref
                            }}
                            full
                            id={id()}
                            label={t(field.label)}
                            value={_fieldValue().value()}
                            list={_fieldValue().possibleValues.map(
                              (_value) => t(_value) || _value,
                            )}
                            onInput={onInput}
                          />
                        )}
                      </Match>
                    </Switch>
                  )
                }}
              </For>
            </Label>
          )}
        </For>
        <Show when={props.calibrations}>
          <Label label={t('Calibration')}>
            <Input
              full
              label={t('Date')}
              readOnly={true}
              long={false}
              value={props.calibrations?.date.toISOString().split('T')[0]}
              type="date"
              id="input-calibration-date"
            />
            <Input
              full
              label={t('DPlate')}
              readOnly={true}
              long={false}
              value={`${props.units?.position.valueToLocaleString(
                props.calibrations?.dPlate ?? 0,
              )} ${props.units?.position.currentUnit()}`}
              id="input-calibration-dplate"
            />
            <Show when={props.calibrations?.channels}>
              {state.channelsDatas}
            </Show>

            <Show when={props.calibrations?.sensors}>{state.sensorsDatas}</Show>
          </Label>
        </Show>
      </div>
    </Dialog>
  )
}
