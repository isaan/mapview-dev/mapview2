import { Button, classPropToString } from '/src/components'
import { createASS, ErrorCode, getExtensionFromFile } from '/src/scripts'
import { store } from '/src/store'

interface Props extends ParentProps {
  accept: string
  buttonText: string
  onInput: (value: File[] | null) => void
}

export const DragAndDrop = (props: Props) => {
  let fileInp: HTMLInputElement | undefined

  const state = {
    dragging: createASS(false),
  }

  const drop = (dataTransfer: DataTransfer | null) => {
    state.dragging.set(false)

    const extensions = props.accept.split(', ')
    if (dataTransfer?.files) {
      const result = Array.from(dataTransfer.files).filter((file) =>
        extensions.includes(`.${getExtensionFromFile(file)}`),
      )

      if (result.length <= 0) {
        store.importError.set(ErrorCode.UNMANAGED_EXTENSION)
        return
      }

      props.onInput(result)
    }
  }

  return (
    <div id="drag-drop">
      <div
        class={classPropToString([
          state.dragging() ? 'bg-gray-200' : 'bg-gray-100',
          'group relative hidden h-[55vh] w-full cursor-pointer items-center justify-center rounded-lg p-1.5 transition-colors duration-200 hover:bg-gray-200 lg:block',
        ])}
      >
        <div
          class={classPropToString([
            state.dragging() ? 'border-gray-400' : 'border-gray-300',
            'font-gray-900 flex h-full w-full items-center justify-center rounded-lg border-[3px] border-dashed text-sm font-medium transition-colors duration-200 group-hover:border-gray-400',
          ])}
        >
          <div class="space-y-4">
            <IconTablerFilePlus class="mx-auto size-7 text-gray-400 transition-colors duration-200 group-hover:text-gray-500" />
            <p>{props.children}</p>
          </div>
        </div>

        <div
          class="absolute inset-0 size-full"
          onClick={() => fileInp?.click()}
          id="drop-zone"
          onDragEnter={() => {
            state.dragging.set(true)
          }}
          onDragLeave={() => {
            state.dragging.set(false)
          }}
          onDragOver={(event) => {
            event.preventDefault()
            event.stopPropagation()
          }}
          onDrop={(event) => {
            event.preventDefault()
            event.stopPropagation()
            drop(event.dataTransfer)
          }}
        />

        <input
          id="file-input"
          class="hidden"
          onClick={() => {
            if (fileInp) fileInp.value = ''
          }}
          onChange={(event) => {
            const files = event.target?.files
            props.onInput(files ? Array.from(files) : null)
          }}
          accept={props.accept}
          type="file"
          ref={fileInp}
        />
      </div>
      <Button
        class="lg:hidden"
        onClick={() => fileInp?.click()}
        full
        leftIcon={IconTablerFilePlus}
      >
        {props.buttonText}
      </Button>
    </div>
  )
}
