/* eslint-disable @typescript-eslint/unbound-method */
import { render } from '@solidjs/testing-library'
import userEvent from '@testing-library/user-event'
import { expect, test, vi } from 'vitest'

import { Button } from './component'

const user = userEvent.setup()

const callback = vi.fn()

test('Button', async () => {
  const { getByRole } = render(() => (
    <Button onClick={callback}>Click me</Button>
  ))
  const button = getByRole('button')
  expect(button).toHaveTextContent('Click me')
  await user.click(button)
  expect(callback).toHaveBeenCalled()
})
