interface Props extends ParentProps {
  close: Accessor<DialogCloseFunction | undefined>
}

export const DialogForm = (props: Props) => {
  return (
    <form
      class="size-full"
      method="dialog"
      onSubmit={(event) => {
        event.preventDefault()
        props.close()?.(event.submitter ?? undefined)
      }}
    >
      {props.children}
    </form>
  )
}
