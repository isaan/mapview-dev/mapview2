import { classPropToString } from '/src/components'

export const DialogDivider = (props: BaseProps) => {
  return (
    <Show when={props.color !== 'transparent'}>
      <hr
        class={classPropToString([
          'flex-none border-t-2 border-black/5',

          props.class,
        ])}
      />
    </Show>
  )
}
