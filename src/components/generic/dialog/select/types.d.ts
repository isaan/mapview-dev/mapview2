type DialogSelectProps = DialogSelectPropsOnly & DialogClassicProps

interface DialogSelectPropsOnly extends SaveableProps, Identified {
  readonly search?: Omit<InputPropsWithHTMLAttributes, 'onInput'>

  readonly values: DialogValuesProps
}
