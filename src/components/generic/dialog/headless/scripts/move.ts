import { makeEventListener } from '@solid-primitives/event-listener'
import {
  useMousePosition,
  type MousePositionInside,
} from '@solid-primitives/mouse'

import { clamp } from '/src/scripts'
import { store } from '/src/store'

import { activateUserSelectNone, deactivateUserSelectNone } from './userSelect'

export const createMovingEffect = (
  dialog: Accessor<HTMLDialogElement | undefined>,
  handle: Accessor<HTMLElement | undefined>,
  isMoveable: Accessor<boolean>,
  moving: ASS<boolean>,
  defaultLeft: Accessor<number>,
  defaultTop: Accessor<number>,
  position: DialogPosition,
) =>
  createEffect(
    on(handle, (_handle) => {
      if (!_handle || !isMoveable()) return

      const mousePosition = useMousePosition()

      makeEventListener(_handle, 'mousedown', () => {
        activateUserSelectNone(dialog())
        moving.set(true)
      })

      makeEventListener(window, 'mouseup', () => {
        deactivateUserSelectNone(dialog())
        moving.set(false)
      })

      createEffect(
        on(
          () => isMoveable() && moving(),
          (_moving) =>
            _moving &&
            moveDialog(
              dialog(),
              defaultLeft(),
              defaultTop(),
              mousePosition,
              position,
            ),
        ),
      )
    }),
  )

const moveDialog = (
  dialog: HTMLDialogElement | undefined,
  defaultLeft: number,
  defaultTop: number,
  mousePosition: MousePositionInside,
  position: DialogPosition,
) => {
  if (!dialog) return

  const { x: originMouseX, y: originMouseY } = mousePosition
  const {
    offsetTop: dialogOffsetTop,
    offsetLeft: dialogOffsetLeft,
    offsetHeight: dialogHeight,
    offsetWidth: dialogWidth,
  } = dialog
  const { innerHeight: windowHeight, innerWidth: windowWidth } = window

  createEffect(() => {
    const { x: currentMouseX, y: currentMouseY } = mousePosition

    const left = dialogOffsetLeft + currentMouseX - originMouseX
    const top = dialogOffsetTop + currentMouseY - originMouseY

    const maxX = windowWidth - dialogWidth
    const minX = 0

    const maxY = windowHeight - dialogHeight
    const minY = 0

    const menuRef = store.menuRef()

    position.left.set(
      snap(
        clamp(left, minX, maxX),
        (menuRef?.offsetLeft ?? 0) + (menuRef?.offsetWidth ?? 0),
      ),
    )
    position.top.set(clamp(top, minY, maxY))
  })
}

const snap = (currentPosition: number, snapPosition: number) => {
  const snapRange = 10

  return Math.abs(currentPosition - snapPosition) <= snapRange
    ? snapPosition
    : currentPosition
}
