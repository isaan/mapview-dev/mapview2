import { openForcedClosedChildDialogs } from './children'
import { moveToFront } from './front'

export const createDialogOpenFunction =
  ({
    dialog,
    open,
    showSetter,
    forcedClosedChildDialogsOpenButtons,
    dialogsDiv,
    zIndex,
    onOpen,
  }: {
    dialog: Accessor<HTMLDialogElement | undefined>
    open: ASS<boolean>
    showSetter: Setter<boolean>
    onOpen: VoidFunction | undefined
    forcedClosedChildDialogsOpenButtons: HTMLButtonElement[]
    dialogsDiv: Accessor<HTMLElement | undefined>
    zIndex: ASS<number>
  }): DialogOpenFunction =>
  (isUserEvent) => {
    if (open()) return

    const noChildrenWerePreviouslyClosed =
      !forcedClosedChildDialogsOpenButtons.length

    if (isUserEvent && noChildrenWerePreviouslyClosed) {
      moveToFront(dialogsDiv, zIndex.set, zIndex)
    }

    dialog()?.show()

    showSetter(true)

    onOpen?.()

    setTimeout(() => open.set(true), 50)

    openForcedClosedChildDialogs(forcedClosedChildDialogsOpenButtons)
  }
