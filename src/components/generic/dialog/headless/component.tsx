/* eslint-disable sonarjs/no-duplicate-string */
import { createMediaQuery } from '@solid-primitives/media'
import {
  createElementSize,
  useWindowSize,
} from '@solid-primitives/resize-observer'
import defaultTailwindCSSTheme from 'tailwindcss/defaultTheme'

import {
  classPropToString,
  headlessDialogBooleanPropsKeysObject,
  removeProps,
} from '/src/components'
import { createASS } from '/src/scripts'
import { store } from '/src/store'

import { DialogBackdrop, DialogResizers } from './components'
import {
  createDialogCloseFunction,
  createDialogOpenFunction,
  createMovingEffect,
  createOnCloseEffect,
  createRelativePositionEffect,
  createScrolledOutEffect,
  HIDDEN_CLOSE_BUTTON_CLASS,
  makeClickOutsideEventListener,
  moveToFront,
} from './scripts'

type Props = HeadlessDialogPropsWithHTMLAttributes

// eslint-disable-next-line sonarjs/cognitive-complexity
export const HeadlessDialog = (props: Props) => {
  const state = {
    show: createASS(false),
    open: createASS(false),
    maximized: createASS(false),
    value: createASS(''),
    position: {
      left: createASS(undefined),
      top: createASS(undefined),
    } as DialogPosition,
    dimensions: {
      width: createASS(undefined),
      height: createASS(undefined),
    } as DialogDimensions,
    zIndex: createASS(0),
    moving: createASS(false),
    dialog: createASS<HTMLDialogElement | undefined>(undefined),
    dialogsDiv: createASS<HTMLElement | undefined>(undefined),
    attached: {
      left: createASS(undefined),
      top: createASS(undefined),
      width: createASS(undefined),
    } as DialogAttached,
  }

  const dialogProps = removeProps(props, headlessDialogBooleanPropsKeysObject)

  const forcedClosedChildDialogsOpenButtons: HTMLButtonElement[] = []

  const isWindowLarge = createMediaQuery(
    `(min-width: ${defaultTailwindCSSTheme.screens.md})`,
  )
  const isAbsolute = createMemo(() => !!props.attach?.())
  const isFixed = createMemo(() => !isAbsolute())
  const isMaximized = createMemo(
    () => !isAbsolute() && (props.maximized || state.maximized()),
  )
  const isMoveable = createMemo(
    () => !isAbsolute() && !isMaximized() && !!props.moveable && !!props.handle,
  )
  const hasBackdrop = createMemo(() => props.backdrop && !isAbsolute())
  const isWindowed = createMemo(() => !isAbsolute() && !isMaximized())
  const isInteractive = createMemo(() => isWindowLarge() && isWindowed())
  const id = createMemo(() => {
    return props.id && props.id.length > 0
      ? props.id
      : `dialog-${props.title?.toLowerCase().replaceAll(' ', '-')}-${Math.floor(
          Math.random() * 100000000,
        )}`
  })
  const defaultLeft = createMemo(() => {
    if (!state.show()) return 0

    if (typeof props.defaultPosition === 'object') {
      return props.defaultPosition.left ?? 0
    }

    switch (props.defaultPosition) {
      case 'center':
      case 'top-center':
      case 'bottom-center':
        return (useWindowSize().width - dialogWidth()) / 2
      case 'top-full':
      case 'bottom-full':
        return 0
      case 'top-minus-menu':
      case 'bottom-minus-menu':
        return store.menuRef()?.offsetWidth ?? 0
      default:
        return (useWindowSize().width - dialogWidth()) / 2
    }
  })
  const defaultTop = createMemo(() => {
    if (typeof props.defaultPosition === 'object') {
      return props.defaultPosition.top ?? 0
    }

    switch (props.defaultPosition) {
      case 'top-full':
      case 'top-center':
      case 'top-minus-menu':
        return 0
      case 'center':
        return Math.round(useWindowSize().height / 10)
      case 'bottom-full':
      case 'bottom-center':
      case 'bottom-minus-menu':
        return useWindowSize().height - dialogHeight()
      default:
        return Math.round(useWindowSize().height / 10)
    }
  })

  const dialogDimensions = createElementSize(state.dialog)
  const dialogWidth = createMemo(
    () => (dialogDimensions.width && state.dialog()?.offsetWidth) ?? 0,
  )

  const dialogHeight = createMemo(
    () => (dialogDimensions.height && state.dialog()?.offsetHeight) ?? 0,
  )

  const open: DialogOpenFunction = createDialogOpenFunction({
    dialog: state.dialog,
    dialogsDiv: state.dialogsDiv,
    forcedClosedChildDialogsOpenButtons,
    onOpen: props.onOpen,
    open: state.open,
    showSetter: state.show.set,
    zIndex: state.zIndex,
  })

  const close: DialogCloseFunction = createDialogCloseFunction({
    dialog: state.dialog,
    setOpen: state.open.set,
    setValue: state.value.set,
    setShow: state.show.set,
    onCloseEnd: () => props.onCloseEnd,
    forcedClosedChildDialogsOpenButtons,
  })

  const toggle: DialogToggleFunction = (isUserEvent) => {
    if (!state.open()) {
      open(isUserEvent)
    } else {
      close()
    }
  }

  const toggleMaximize: DialogToggleMaximizedFunction = () => {
    moveToFront(state.dialogsDiv, state.zIndex.set, state.zIndex)
    state.maximized.set((b) => !b)
  }

  createEffect(() => state.dimensions.height.set(props.dimensions?.height))
  createEffect(() => state.dimensions.width.set(props.dimensions?.width))
  createEffect(() => state.position.left.set(props.position?.left))
  createEffect(() => state.position.top.set(props.position?.top))
  createEffect(() => props.onOpenCreated?.(open))
  createEffect(() => props.onCloseCreated?.(close))
  createEffect(() => props.onToggleCreated?.(toggle))
  createEffect(() => props.onToggleMaximizeCreated?.(toggleMaximize))
  createEffect(() => props.onIdCreated?.(id()))
  createEffect(() => props.onAbsolute?.(isAbsolute()))
  createEffect(() => props.onMaximized?.(isMaximized()))

  onMount(() => {
    state.dialogsDiv.set(document.getElementById('dialogs') || undefined)

    createEffect(() => {
      if (props.onClose) {
        createOnCloseEffect({
          onClose: props.onClose,
          open: state.open,
          value: state.value,
        })
      }
    })

    createRelativePositionEffect({
      dialog: state.dialog,
      attach: props.attach,
      setters: state.attached,
      options: props.attachOptions,
    })

    createScrolledOutEffect({
      attach: props.attach,
      open: state.open,
      close,
    })

    createEffect(() => {
      const attach = props.attach

      if (attach) {
        let clearClickEvent: VoidFunction | undefined
        createEffect(() => {
          clearClickEvent?.()
          if (isAbsolute() && state.open()) {
            clearClickEvent = makeClickOutsideEventListener(
              state.dialog(),
              props.attach?.(),
              close,
            )
          }
        })
        onCleanup(() => {
          clearClickEvent?.()
        })
      }
    })

    createMovingEffect(
      state.dialog,
      () => props.handle?.(),
      isMoveable,
      state.moving,
      defaultLeft,
      defaultTop,
      state.position,
    )
  })

  const styleWidth = createMemo(() => {
    if (
      isMaximized() ||
      props.defaultPosition === 'top-full' ||
      props.defaultPosition === 'bottom-full'
    )
      return '100%'

    if (
      props.defaultPosition === 'top-minus-menu' ||
      props.defaultPosition === 'bottom-minus-menu'
    )
      return `max(calc(100% - ${defaultLeft()}px), ${state.dimensions.width()}px)`

    return undefined
  })

  return (
    <Portal mount={state.dialogsDiv()}>
      <DialogBackdrop
        show={() => state.show() && hasBackdrop()}
        open={state.open}
        zIndex={state.zIndex}
      />

      <Dynamic
        component={props.component ?? 'dialog'}
        {...dialogProps}
        ref={state.dialog.set}
        id={id()}
        data-testid={props.testId}
        onMouseDown={(event: MouseEvent) => {
          event.stopPropagation()
          moveToFront(state.dialogsDiv, state.zIndex.set, state.zIndex)
        }}
        style={{
          margin: 0,
          position: isAbsolute() ? 'absolute' : 'fixed',
          height: props.full?.() || isMaximized() ? '100%' : undefined,
          width: styleWidth(),
          'max-height': isMaximized() ? '100%' : undefined,
          top: isMaximized() ? '0px' : undefined,
          display: state.show() ? 'flex' : undefined,
          'z-index': state.zIndex(),
          padding: 0,

          ...(isAbsolute()
            ? {
                top: `${state.attached.top()}px`,
                left: `${state.attached.left()}px`,
                width: `${state.attached.width()}px`,
              }
            : {}),

          ...(isInteractive()
            ? {
                top: `${Math.min(
                  state.position.top() ?? defaultTop(),
                  Math.max(useWindowSize().height - dialogHeight(), 0),
                )}px`,
                left: `${Math.min(
                  state.position.left() ?? defaultLeft(),
                  Math.max(useWindowSize().width - dialogWidth(), 0),
                )}px`,
                ...(state.dimensions.width()
                  ? {
                      'max-width': `${Math.min(state.dimensions.width() ?? useWindowSize().width, useWindowSize().width)}px`,
                    }
                  : {}),
                ...(state.dimensions.height()
                  ? {
                      height: '100%',
                      'max-height': `${Math.min(state.dimensions.height() ?? useWindowSize().height, useWindowSize().height)}px`,
                    }
                  : {}),
              }
            : {}),
        }}
        class={classPropToString([
          props.classes,
          state.open() && props.classesOpen,
          isMoveable() && props.classesMoveable,
          isAbsolute() && props.classesAbsolute,
          isFixed() && props.classesFixed,
          isWindowed() && props.classesWindowed,
          isMaximized() && props.classesMaximized,
          (props.defaultPosition === 'center' ||
            props.defaultPosition === 'top-center' ||
            props.defaultPosition === 'bottom-center') &&
            'md:max-w-2xl',
        ])}
      >
        <div
          style={{
            position: 'relative',
            display: 'flex',
            width: '100%',
          }}
        >
          <Show when={state.show()}>
            <div style={{ overflow: 'auto', width: '100%' }}>
              {props.children}
            </div>
          </Show>

          <button
            hidden
            class={HIDDEN_CLOSE_BUTTON_CLASS}
            onClick={() => close()}
          />

          <Show when={props.resizable && !state.maximized()}>
            <DialogResizers
              dialog={state.dialog}
              dimensions={state.dimensions}
              position={state.position}
            />
          </Show>
        </div>
      </Dynamic>
    </Portal>
  )
}
