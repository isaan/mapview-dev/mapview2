import { Button, Dialog } from '/src/components'
import { useAppState } from '/src/index'

type Props = DialogRemoveProps

export const DialogRemove = (props: Props) => {
  const { t } = useAppState()

  return (
    <Dialog
      id={props.id}
      closeable
      size="sm"
      title={props.title}
      button={{
        icon: IconTablerTrash,
        color: 'red',
        disabled: props.disabled,
      }}
      onClose={(value) => props.onClose(value)}
      form={
        <div class="space-y-2">
          <p>{props.textContent}</p>
          <Button full color="red" value="delete" id="delete-button">
            <span class="flex-1">{t("Yes, I'm sure")}</span>
          </Button>
        </div>
      }
    />
  )
}
