interface DialogRemoveProps {
  readonly id?: string
  readonly title: string
  readonly textContent: JSX.Element
  readonly onClose: (value: string | undefined) => void
  readonly disabled: boolean
}
