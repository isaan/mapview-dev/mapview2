export const dialogClassicBooleanPropsKeysObject: BooleanPropsKeysObject<DialogClassicPropsOnly> =
  {
    id: true,
    attached: true,
    button: true,
  }

export const dialogButtonBooleanPropsKeysObject: BooleanPropsKeysObject<InternalButtonPropsOnly> =
  {
    text: true,
    'data-testid': false,
  }
