type DialogClassicProps = DialogClassicPropsOnly & DialogPropsWithHTMLAttributes

interface DialogClassicPropsOnly {
  readonly id?: string
  readonly attached?: true
  readonly button?: InternalButtonProps
}

type InternalButtonProps = InternalButtonPropsOnly &
  ButtonPropsWithHTMLAttributes

interface InternalButtonPropsOnly {
  readonly text?: string | (() => JSXElement)
  readonly 'data-testid'?: string
}
