interface Props extends ParentProps {
  id?: string
}

export const Table = (props: Props) => {
  return (
    <table id={props.id} class="relative w-full table-auto text-sm font-medium">
      {props.children}
    </table>
  )
}
